# Turist-app
Repositorio de la aplicacion de proyecto de grado incluye, frontend, backend y app movil

Actualmente la configuracion del backend se encuentra configurada para estar desplegada en la nube

Si deseas probar de forma local el backend debes seguir los siguientes pasos.

Dentro de la carpeta backend -> 

$npm i
$nodemon server/server.js

El frontend web se encuentra desplegado en heroku, si deseas probar de forma local y realizar cambios debes seguir los siguientes pasos.

Dentro de la carpte frontend ->
$npm i
$ng serve -o

La aplicacion movil para ser iniciada debes realizar los siguientes pasos.

Dentro de la carpeta app ->
$flutter packages get

Posterior puede correr la aplicacion usando un emulador de android 4.4+