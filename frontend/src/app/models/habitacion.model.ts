export interface HabitacionInterface {
  _id:string,
  id_hotel: string;
  nombre_hotel:string,
  tipo: string;
  imagenes: [string];
  precio: number;
  personas: number;
  descripcion: string;
  estado: boolean;
}
