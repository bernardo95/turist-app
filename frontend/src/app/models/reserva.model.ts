import { PlanesModel } from './plan.model';
import { UsuarioInterface } from './usuario.model';
import { HotelInterface } from './hotel.model';
import { HabitacionInterface } from './habitacion.model';
export interface ReservaPlanInterface {
  _id: string;
  id_usuario: string;
  id_plan: string;
  nombre_usuario:string,
  apellido_usuario:string,
  email:string,
  nombre_plan:string,
  estado: string;
  precio: number;
  personas: number;
  fecha: string;
  plan: [PlanesModel];
  usuario:[UsuarioInterface];
}

export interface ReservaHabitacionInterface {
  _id: string;
  id_usuario: string;
  id_habitacion: string;
  id_hotel: string;
  estado: string;
  precio: number;
  personas: number;
  habitaciones: number;
  fechaInicio: string;
  fechaFin: string;
  hotel:[HotelInterface];
  habitacion:[HabitacionInterface];
  usuario:[UsuarioInterface];
}
