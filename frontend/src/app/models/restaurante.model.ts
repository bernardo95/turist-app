export interface InterfaceRestaurante {
  _id: string;
  nombre: string;
  imagenes: [string];
  informacion: string;
  horario: string;
  ubicacion: string;
  coordenadas: {
    latitud: number;
    longitud: number;
  };
  calificacion: number;
  estado: boolean;
}
