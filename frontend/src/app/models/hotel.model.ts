export interface HotelInterface{
    _id: string,
    nombre:string,
    descripcion:string,
    categoria:number,
    checkIn:{
        desde: number,
        hasta:number
    },
    checkOut:{
        desde: number,
        hasta:number
    },
    ubicacion:string,
    coordenadas:{
        latitud:number,
        longitud:number,
    },
    calificacion:number,
    imagenes:Array<string>,
    estado:boolean
}