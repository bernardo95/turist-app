export class Usuario {

    constructor(
        public _id: string,
        public nombre: string,
        public apellido:string,
        public email: string,
        public role:string,
        public google:boolean,
        public facebook:boolean,
        public estado:boolean
    ){}

}

export interface UsuarioInterface {
         _id: string,
         nombre: string,
         apellido:string,
         email: string,
         role:string,
         google:boolean,
         facebook:boolean,
         estado:boolean,
         createdAt:string,
         updatedAt:string
}
