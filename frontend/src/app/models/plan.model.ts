export interface PlanInterface{
    _id:string,
    sitio:string,
    nombre:string,
    imagenes:[string],
    descripcion:string,
    ubicacion:string,
    duracion:string,
    personas:number,
    precio:number,
    servicios:[string],
    servicios_extra:[string],
    punto_encuentro:string,
    coordenadas:{
        latitud: number,
        longitud: number
    },
    recomendaciones:[string],
    categoria:string,
    observaciones:[string],
    itinerario:[string],
    hora_inicio:string,
    hora_fin:string,
    calificacion:number,
    estado:boolean,
    createdAt:string,
    updatedAt:string
}

export class PlanesModel {
    constructor(
    public _id:string,
    public sitio:string,
    public nombre:string,
    public imagenes:[string],
    public descripcion:string,
    public ubicacion:string,
    public duracion:string,
    public personas:number,
    public precio:number,
    public servicios:[string],
    public servicios_extra:[string],
    public punto_encuentro:string,
    public coordenadas:{
        latitud: number,
        longitud: number
    },
    public recomendaciones:[string],
    public categoria:string,
    public observaciones:[string],
    public itinerario:[string],
    public hora_inicio:string,
    public hora_fin:string,
    public calificacion:number,
    public estado:boolean,
    public createdAt:string,
    public updatedAt:string
    ) {
        
    }

}


