import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { unSetUser } from '../../auth/auth.actions';
import { Router } from '@angular/router';
import { unSetPlanes } from '../../components/planes.actions';
import { unSetHoteles } from '../../components/hoteles.actions';
import { unSetHabitaciones } from '../../components/habitaciones.actions';
import { unSetRestaurantes } from '../../components/restaurantes.actions';
import { unSetReservasPlanes } from '../../components/reservas_planes.actions';
import { unSetReservasHabitaciones } from '../../components/reservas_habitaciones.actions';
import { unSetUsuarios } from '../../components/reportes/usuarios.actions';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private store:Store<AppState>,private router:Router) { }

  ngOnInit(): void {
  }
  logout(){
    this.store.dispatch(unSetUser());
    this.store.dispatch(unSetPlanes());
    this.store.dispatch(unSetHoteles());
    this.store.dispatch(unSetHabitaciones());
    this.store.dispatch(unSetRestaurantes());
    this.store.dispatch(unSetReservasPlanes());
    this.store.dispatch(unSetReservasHabitaciones());
    this.store.dispatch(unSetUsuarios());
    localStorage.clear();
    this.router.navigate(['login']);
  }

}
