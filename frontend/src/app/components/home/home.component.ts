import { Component, OnInit, OnDestroy } from "@angular/core";
import { PlanService } from "../../services/plan.service";
import { PlanesModel } from "../../models/plan.model";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducer";
import { setPlanes, unSetPlanes } from "../planes.actions";
import { Subscription } from "rxjs";
import { HotelService } from '../../services/hotel.service';
import { HotelInterface } from '../../models/hotel.model';
import { setHoteles, unSetHoteles } from '../hoteles.actions';
import { HabitacionService } from '../../services/habitacion.service';
import { HabitacionInterface } from '../../models/habitacion.model';
import { setHabitaciones, unSetHabitaciones } from '../habitaciones.actions';
import { filtrosValidos, setFiltro } from '../filtro.actions';
import Swal from 'sweetalert2';
import { InterfaceRestaurante } from '../../models/restaurante.model';
import { RestauranteService } from '../../services/restaurante.service';
import { unSetRestaurantes, setRestaurantes } from '../restaurantes.actions';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit, OnDestroy {
  
  planes: PlanesModel[] = [];
  hoteles: HotelInterface[] = [];
  habitaciones: HabitacionInterface[] = [];
  restaurantes: InterfaceRestaurante[] = [];
  filtroLista: filtrosValidos[] = ['ninguno','planes','hoteles','habitaciones','restaurantes'];
  filtroActual:filtrosValidos = 'ninguno';
  planesSubscription: Subscription;
  hotelesSubscription: Subscription;
  habitacionesSubscription:Subscription;
  restaurantesSubscription:Subscription;
  filtroSubscription:Subscription;


  constructor(
    private store: Store<AppState>,
    private planService: PlanService,
    private hotelService:HotelService,
    private habitacionService:HabitacionService,
    private restauranteService:RestauranteService
  ) {
  }

  ngOnInit(): void {
    this.subscriptionFiltro();
    this.subscriptionPlanes();
    this.subscriptionHoteles();
    this.subscriptionHabitaciones();
    this.subscribeRestaurantes();
  }

  subscriptionPlanes() {
    this.planesSubscription = this.store
      .select("planes")
      .subscribe(({ planes }) => {
        this.planes = [];
        Object.keys(planes).forEach((key) => {
          let planesTemp: PlanesModel = planes[key];
          this.planes.push(planesTemp);
        });
      });
  }

  subscriptionHoteles() {
    this.hotelesSubscription = this.store
      .select("hoteles")
      .subscribe(({ hoteles }) => {
        this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
      });
  }

  subscriptionHabitaciones(){
    this.habitacionesSubscription = this.store
      .select("habitaciones")
      .subscribe(({ habitaciones }) => {  
        this.habitaciones = [];
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          let hotelFind = this.hoteles.find((hotel)=>hotel._id === habitacionesTemp.id_hotel);
          const habitacionAdd: HabitacionInterface = Object.assign({}, habitacionesTemp, { nombre_hotel: hotelFind.nombre });
          this.habitaciones.push(habitacionAdd);
        });
      });
  }
  subscribeRestaurantes(){
    this.restaurantesSubscription = this.store.select('restaurantes').subscribe(({restaurantes})=>{
      this.restaurantes = [];
      Object.keys(restaurantes).forEach((key) => {
        let restaurantesTemp: InterfaceRestaurante = restaurantes[key];
        this.restaurantes.push(restaurantesTemp);
      });
    });
  }

  subscriptionFiltro(){
    this.filtroSubscription = this.store.select('filtro').subscribe(({filtro})=>{
      this.filtroActual = filtro;
    });
  }


  ngOnDestroy(): void {
    this.planesSubscription?.unsubscribe();
    this.hotelesSubscription?.unsubscribe();
    this.habitacionesSubscription?.unsubscribe();
    this.restaurantesSubscription?.unsubscribe();
  }

  async cambiarEstadoPlanes(plan: PlanesModel) {
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
      let estado = plan.estado;
    estado = !estado;
    const updatePlan: PlanesModel = Object.assign({}, plan, { estado: estado });
    console.log(updatePlan);
    this.planService.actualizarPlan(updatePlan).subscribe((resp) => {
      console.log(resp);
      const planResponse: PlanesModel = resp["planDB"];
      let newArrayPlanes: PlanesModel[] = this.planes.map((plan) => {
        if (plan._id === planResponse._id) {
          plan = planResponse;
          return plan;
        } else {
          return plan;
        }
      });
      Swal.fire({
        icon: "success",
        title: "Estado actualizado",
        showConfirmButton: false,
        timer: 1000,
      });
      this.planes = [];
      this.store.dispatch(unSetPlanes());
      this.store.dispatch(setPlanes({ planes: newArrayPlanes }));
    });
    }
    
  }
  async cambiarEstadoHoteles(hotel:HotelInterface){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
      let estado = hotel.estado;
    estado = !estado;
    const updateHotel: HotelInterface = Object.assign({}, hotel, { estado: estado });
    console.log(updateHotel);
    this.hotelService.actualizarHotel(updateHotel).subscribe((resp) => {
      const hotelResponse: HotelInterface = resp["hotelDB"];
      let newArrayHoteles: HotelInterface[] = this.hoteles.map((hotel) => {
        if (hotel._id === hotelResponse._id) {
          hotel = hotelResponse;
          return hotel;
        } else {
          return hotel;
        }
      });
      this.hoteles = [];
      this.store.dispatch(unSetHoteles());
      this.store.dispatch(setHoteles({ hoteles: newArrayHoteles }));
      Swal.fire({
        icon: "success",
        title: "Estado actualizado",
        showConfirmButton: false,
        timer: 1000,
      });
    });
    }
  }
  async cambiarEstadoHabitacion(habitacion:HabitacionInterface){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
      delete habitacion.nombre_hotel;
    let estado = habitacion.estado;
    estado = !estado;
    const updateHabitacion: HabitacionInterface = Object.assign({}, habitacion, { estado: estado });
    this.habitacionService.actualizarHabitacion(updateHabitacion).subscribe((resp) => {
      const habitacionResponse: HabitacionInterface = resp["habitacionDB"];
      let newArrayHabitaciones: HabitacionInterface[] = this.habitaciones.map((habitacion) => {
        if (habitacion._id === habitacionResponse._id) {
          habitacion = habitacionResponse;
          return habitacion;
        } else {
          return habitacion;
        }
      });
      Swal.fire({
        icon: "success",
        title: "Estado actualizado",
        showConfirmButton: false,
        timer: 1000,
      });
      this.habitaciones = [];
      this.store.dispatch(unSetHabitaciones());
      this.store.dispatch(setHabitaciones({ habitaciones: newArrayHabitaciones }));
    });
    }
    
  }
  async cambiarEstadoRestaurante(restaurante:InterfaceRestaurante){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
    let estado = restaurante.estado;
    estado = !estado;
    const updateRestaurante: InterfaceRestaurante = Object.assign({}, restaurante, { estado: estado });
    this.restauranteService.actualizarRestaurante(updateRestaurante).subscribe((resp) => {
      const restauranteResponse: InterfaceRestaurante = resp["restauranteDB"];
      let newArrayRestaurante: InterfaceRestaurante[] = this.restaurantes.map((restaurante) => {
        if (restaurante._id === restauranteResponse._id) {
          restaurante = restauranteResponse;
          return restaurante;
        } else {
          return restaurante;
        }
      });
      Swal.fire({
        icon: "success",
        title: "Estado actualizado",
        showConfirmButton: false,
        timer: 1000,
      });
      this.restaurantes = [];
      this.store.dispatch(unSetRestaurantes());
      this.store.dispatch(setRestaurantes({ restaurantes: newArrayRestaurante }));
    });
    }
    
  }
  aplicarFiltro(filtro:filtrosValidos){
    this.store.dispatch(setFiltro({filtro:filtro}));
  }
}
