import { createReducer, on } from "@ngrx/store";
import { setHoteles,unSetHoteles } from "./hoteles.actions";
import { HotelInterface } from "../models/hotel.model";

export interface State {
  hoteles: HotelInterface[];
}

export const initialState: State = {
  hoteles: [],
};

const _hotelReducer = createReducer(
  initialState,

  on(setHoteles, (state, { hoteles }) => ({ ...state, hoteles: {...hoteles}})),
  on(unSetHoteles, state => ({ ...state, hoteles: []})),

);

export function hotelReducer(state, action) {
  return _hotelReducer(state, action);
}
