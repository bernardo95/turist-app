import { createAction, props } from '@ngrx/store';
import { ReservaPlanInterface } from '../models/reserva.model';

export const setReservasPlanes = createAction('[Reserva Planes Component] set Reservas Planes',props<{reservas_planes:ReservaPlanInterface[]}>());
export const unSetReservasPlanes = createAction('[Reserva Planes Component] unSet Reservas Planes');
