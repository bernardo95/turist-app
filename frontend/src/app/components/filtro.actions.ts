import { createAction, props } from '@ngrx/store';
export type filtrosValidos = 'planes' | 'hoteles' | 'habitaciones'| 'restaurantes' | 'ninguno';

export const setFiltro = createAction('[Filtro Component] Set Filtro',props<{filtro:filtrosValidos}>());