import { createAction, props } from '@ngrx/store';
import { HotelInterface } from '../models/hotel.model';

export const setHoteles = createAction('[Hotel Component] Set Hoteles',props<{hoteles:HotelInterface[]}>());
export const unSetHoteles = createAction('[Hotel Component] UnSet Hoteles');