import { Component, OnInit, OnDestroy } from '@angular/core';
import { InterfaceRestaurante } from 'src/app/models/restaurante.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { RestauranteService } from 'src/app/services/restaurante.service';
import { ActivatedRoute, Router } from '@angular/router';
import { isLoading, stopLoading } from 'src/app/shared/ui.actions';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-restaurante',
  templateUrl: './editar-restaurante.component.html',
  styleUrls: ['./editar-restaurante.component.css']
})
export class EditarRestauranteComponent implements OnInit , OnDestroy{
  id: string;
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  initialImages: Array<string> = [];
  restaurante:InterfaceRestaurante;
  restaurantes: InterfaceRestaurante[] = [];
  seleccionandoImagenes: boolean = false;
  forma: FormGroup;
  cargando: boolean = false;
  restaurantesSubscription:Subscription;
  cargandoSubscription: Subscription;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private restauranteService:RestauranteService
  ) { 
    this.activatedRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.subscribeRestaurantes();
  }
  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.restaurantesSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());

    this.crearFormulario();
    this.cargarImagenes();
  }
  subscribeRestaurantes(){
    this.restaurantesSubscription = this.store.select('restaurantes').subscribe(({restaurantes})=>{
      this.restaurantes = [];
      Object.keys(restaurantes).forEach((key) => {
        let restaurantesTemp: InterfaceRestaurante = restaurantes[key];
        this.restaurantes.push(restaurantesTemp);
      });
      this.restaurante = this.restaurantes.find((restaurante)=>{
        if (restaurante._id==this.id) {
          return restaurante;
        }
      });
    });
  }
  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get calificacionValido(){
    return (this.forma.controls.calificacion.invalid && this.forma.controls.calificacion.touched);
  }
  get horarioValido(){
    return (this.forma.controls.horario.invalid && this.forma.controls.horario.touched);
  }
  get informacionValido(){
    return (this.forma.controls.informacion.invalid && this.forma.controls.informacion.touched);
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  crearFormulario() {
    this.forma = this.fb.group({
      nombre: [this.restaurante?.nombre, Validators.required],
      imagenes: [""],
      informacion: [this.restaurante?.informacion, Validators.required],
      horario: [this.restaurante?.horario, Validators.required],
      ubicacion: [this.restaurante?.ubicacion, Validators.required],
      coordenadas: this.fb.group({
        latitud: [this.restaurante?.coordenadas.latitud, Validators.required],
        longitud: [this.restaurante?.coordenadas.longitud, Validators.required],
      }),
      calificacion:[this.restaurante?.calificacion,Validators.required],
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  cargarImagenes() {
    this.restaurante.imagenes.map((imagen) => {
      this.initialImages.push(imagen);
    });
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    this.seleccionandoImagenes = true;
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      this.previewUrls = [];
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }
  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      if (this.uploadFiles.length === 0) {
        this.forma.value.imagenes = this.initialImages;
      }
      this.restauranteService
        .editarRestaurante(this.id, this.forma.value)
        .subscribe(async (resp) => {
          if (resp["ok"] == false) {
            this.store.dispatch(stopLoading());

            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["restauranteDB"]._id;
            if (this.uploadFiles.length != 0) {
              this.restauranteService
                ._cargarImagenesRestaurante(formData, id)
                .subscribe(async (resp) => {
                  this.store.dispatch(stopLoading());
                  Swal.fire({
                    icon: "success",
                    title: "Restaurante actualizado",
                    showConfirmButton: false,
                    timer: 2500,
                  });
                  this.store.dispatch(stopLoading());
                  this.forma.reset();
                  setTimeout(async () => {
                    await this.router.navigate(["/editar"]);
                    window.location.reload();
                  }, 2500);
                });
            } else {
              Swal.fire({
                icon: "success",
                title: "Restaurante actualizado",
                showConfirmButton: false,
                timer: 1000,
              });
              this.store.dispatch(stopLoading());
              this.forma.reset();
              await this.router.navigate(["/editar"]);
              window.location.reload();
            }
          }
        });
    }
  }
}
