import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { PlanesModel } from "../../../models/plan.model";
import { Store } from "@ngrx/store";
import { AppState } from "../../../app.reducer";
import { FormGroup, FormBuilder, FormArray, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { PlanService } from "src/app/services/plan.service";
import { stopLoading, isLoading } from "src/app/shared/ui.actions";
import Swal from "sweetalert2";

@Component({
  selector: "app-editar-plan",
  templateUrl: "./editar-plan.component.html",
  styleUrls: ["./editar-plan.component.css"],
})
export class EditarPlanComponent implements OnInit, OnDestroy {
  id: string;
  plan: PlanesModel;
  planes: PlanesModel[] = [];
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  initialImages: Array<string> = [];
  seleccionandoImagenes: boolean = false;
  forma: FormGroup;
  cargando: boolean = false;
  cargandoSubscription: Subscription;
  planesSubscription: Subscription;

  constructor(
    private activateRoute: ActivatedRoute,
    private store: Store<AppState>,
    private fb: FormBuilder,
    private planService: PlanService,
    private router: Router
  ) {
    this.activateRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.obtenerPlan();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());


    this.crearFormulario();
    this.cargarServicios();
    this.cargarServiciosExtra();
    this.cargarRecomendaciones();
    this.cargarItinerarios();
    this.cargarObservaciones();
    this.cargarImagenes();
  }

  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.planesSubscription?.unsubscribe();
  }

  obtenerPlan() {
    this.planesSubscription = this.store
      .select("planes")
      .subscribe(({ planes }) => {
        Object.keys(planes).forEach((key) => {
          let planesTemp: PlanesModel = planes[key];
          this.planes.push(planesTemp);
        });
        this.plan = this.planes.find((plan) => {
          if (plan._id === this.id) {
            return plan;
          }
        });
      });
  }

  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  get duracionNoValido() {
    return (
      this.forma.controls.duracion.invalid &&
      this.forma.controls.duracion.touched
    );
  }
  get personasNoValido() {
    return (
      this.forma.controls.personas.invalid &&
      this.forma.controls.personas.touched
    );
  }
  get precioNoValido() {
    return (
      this.forma.controls.precio.invalid && this.forma.controls.precio.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get serviciosNoValido() {
    return (
      this.forma.controls.servicios.invalid &&
      this.forma.controls.servicios.touched
    );
  }
  get serviciosExtraNoValido() {
    return (
      this.forma.controls.servicios_extra.invalid &&
      this.forma.controls.servicios_extra.touched
    );
  }
  get recomendacionesNoValido() {
    return (
      this.forma.controls.recomendaciones.invalid &&
      this.forma.controls.recomendaciones.touched
    );
  }
  get itinerarioNoValido() {
    return (
      this.forma.controls.itinerario.invalid &&
      this.forma.controls.itinerario.touched
    );
  }
  get observacionesNoValido() {
    return (
      this.forma.controls.observaciones.invalid &&
      this.forma.controls.observaciones.touched
    );
  }
  get horaInicioNoValido() {
    return (
      this.forma.controls.hora_inicio.invalid &&
      this.forma.controls.hora_inicio.touched
    );
  }
  get horaFinNoValido() {
    return (
      this.forma.controls.hora_fin.invalid &&
      this.forma.controls.hora_fin.touched
    );
  }
  get puntoEncuentroNoValido() {
    return (
      this.forma.controls.punto_encuentro.invalid &&
      this.forma.controls.punto_encuentro.touched
    );
  }
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get sitioNoValido() {
    return (
      this.forma.controls.sitio.invalid && this.forma.controls.sitio.touched
    );
  }
  get categoriaNoValido() {
    return (
      this.forma.controls.categoria.invalid &&
      this.forma.controls.categoria.touched
    );
  }
  get servicios() {
    return this.forma.get("servicios") as FormArray;
  }
  get serviciosExtra() {
    return this.forma.get("servicios_extra") as FormArray;
  }
  get recomendaciones() {
    return this.forma.get("recomendaciones") as FormArray;
  }
  get itinerario() {
    return this.forma.get("itinerario") as FormArray;
  }
  get observaciones() {
    return this.forma.get("observaciones") as FormArray;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      sitio: [this.plan?.sitio, Validators.required],
      nombre: [this.plan?.nombre, Validators.required],
      descripcion: [this.plan?.descripcion, Validators.required],
      ubicacion: [this.plan?.ubicacion, Validators.required],
      duracion: [this.plan?.duracion, Validators.required],
      personas: [this.plan?.personas, Validators.required],
      precio: [this.plan?.precio, Validators.required],
      servicios: this.fb.array([]),
      servicios_extra: this.fb.array([]),
      hora_inicio: [this.plan?.hora_inicio, Validators.required],
      hora_fin: [this.plan?.hora_fin, Validators.required],
      punto_encuentro: [this.plan?.punto_encuentro, Validators.required],
      coordenadas: this.fb.group({
        latitud: [this.plan?.coordenadas.latitud, Validators.required],
        longitud: [this.plan?.coordenadas.longitud, Validators.required],
      }),
      recomendaciones: this.fb.array([]),
      observaciones: this.fb.array([]),
      itinerario: this.fb.array([]),
      categoria: [this.plan?.categoria, Validators.required],
      imagenes: [""],
    });
  }
  agregarServicio() {
    this.servicios.push(this.fb.control("", Validators.required));
  }
  cargarServicios() {
    this.plan.servicios.map((servicio) => {
      // console.log(servicio);
      this.servicios.push(this.fb.control(servicio, Validators.required));
    });
  }
  borrarServicio(i: number) {
    this.servicios.removeAt(i);
  }
  agregarServicioExtra() {
    this.serviciosExtra.push(this.fb.control("", Validators.required));
  }
  cargarServiciosExtra() {
    this.plan.servicios_extra.map((servicio_extra) => {
      this.serviciosExtra.push(
        this.fb.control(servicio_extra, Validators.required)
      );
    });
  }
  borrarServicioExtra(i: number) {
    this.serviciosExtra.removeAt(i);
  }
  agregarRecomendacion() {
    this.recomendaciones.push(this.fb.control("", Validators.required));
  }
  cargarRecomendaciones() {
    this.plan.recomendaciones.map((recomendacion) => {
      this.recomendaciones.push(
        this.fb.control(recomendacion, Validators.required)
      );
    });
  }
  borrarRecomendacion(i: number) {
    this.recomendaciones.removeAt(i);
  }
  agregarItinerario() {
    this.itinerario.push(this.fb.control(""));
  }
  cargarItinerarios() {
    this.plan.itinerario.map((itinerario) => {
      this.itinerario.push(this.fb.control(itinerario));
    });
  }
  borrarItinerario(i: number) {
    this.itinerario.removeAt(i);
  }
  agregarObservacion() {
    this.observaciones.push(this.fb.control("", Validators.required));
  }
  cargarObservaciones() {
    this.plan.observaciones.map((observacion) => {
      this.observaciones.push(
        this.fb.control(observacion, Validators.required)
      );
    });
  }
  borrarObservacion(i: number) {
    this.observaciones.removeAt(i);
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  cargarImagenes() {
    this.plan.imagenes.map((imagen) => {
      this.initialImages.push(imagen);
    });
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    this.seleccionandoImagenes = true;
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      this.previewUrls = [];
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }

  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.store.dispatch(isLoading());
      if (this.uploadFiles.length === 0) {
        this.forma.value.imagenes = this.initialImages;
      }
      this.planService
        .editarPlan(this.plan._id, this.forma.value)
        .subscribe(async (resp) => {
          if (resp["ok"] == false) {
            this.store.dispatch(stopLoading());
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["planDB"]._id;
            if (this.uploadFiles.length != 0) {
              this.planService
                .actualizarImagenes(formData, id)
                .subscribe(async (resp) => {
                  Swal.fire({
                    icon: "success",
                    title: "Plan actualizado",
                    showConfirmButton: false,
                    timer: 1500,
                  });
                  this.store.dispatch(stopLoading());
                  this.forma.reset();
                  await this.router.navigate(["/editar"]);
                  window.location.reload();
                });
            } else {
              Swal.fire({
                icon: "success",
                title: "Plan actualizado",
                showConfirmButton: false,
                timer: 1000,
              });
              this.store.dispatch(stopLoading());
              this.forma.reset();
              await this.router.navigate(["/editar"]);
              window.location.reload();
            }
          }
        });
    }
  }
}
