import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppState } from "src/app/app.reducer";
import { Store } from "@ngrx/store";
import { FormBuilder, FormGroup, FormArray, Validators } from "@angular/forms";
import { HotelService } from "../../../services/hotel.service";
import { HotelInterface } from "../../../models/hotel.model";
import { Subscription } from "rxjs";
import { isLoading, stopLoading } from "src/app/shared/ui.actions";
import Swal from "sweetalert2";

@Component({
  selector: "app-editar-hotel",
  templateUrl: "./editar-hotel.component.html",
  styleUrls: ["./editar-hotel.component.css"],
})
export class EditarHotelComponent implements OnInit, OnDestroy {
  id: string;
  hotel: HotelInterface;
  hoteles: HotelInterface[] = [];
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  initialImages: Array<string> = [];
  seleccionandoImagenes: boolean = false;
  forma: FormGroup;
  cargando: boolean = false;
  cargandoSubscription: Subscription;
  hotelesSubscription: Subscription;
  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<AppState>,
    private fb: FormBuilder,
    private hotelService: HotelService,
    private router: Router
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.obtenerHotel();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());

    this.crearFormulario();
    this.cargarImagenes();
  }

  ngOnDestroy(): void {
    this.hotelesSubscription?.unsubscribe();
    this.cargandoSubscription?.unsubscribe();
  }

  obtenerHotel() {
    this.hotelesSubscription = this.store
      .select("hoteles")
      .subscribe(({ hoteles }) => {
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
        this.hotel = this.hoteles.find((hotel) => {
          if (hotel._id === this.id) {
            return hotel;
          }
        });
      });
  }
  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get checkInValido() {
    return (
      this.forma.controls.checkIn.invalid && this.forma.controls.checkIn.touched
    );
  }
  get checkOutValido() {
    return (
      this.forma.controls.checkOut.invalid &&
      this.forma.controls.checkOut.touched
    );
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get calificacionValido(){
    return (this.forma.controls.calificacion.invalid && this.forma.controls.calificacion.touched);
  }
  get categoriaNoValido() {
    return (
      this.forma.controls.categoria.invalid &&
      this.forma.controls.categoria.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      nombre: [this.hotel?.nombre, Validators.required],
      imagenes: [""],
      ubicacion: [this.hotel?.ubicacion, Validators.required],
      coordenadas: this.fb.group({
        latitud: [this.hotel?.coordenadas.latitud, Validators.required],
        longitud: [this.hotel?.coordenadas.longitud, Validators.required],
      }),
      calificacion:[this.hotel?.calificacion,Validators.required],
      checkIn: this.fb.group({
        desde: [this.hotel?.checkIn.desde, Validators.required],
        hasta: [this.hotel?.checkIn.hasta, Validators.required],
      }),
      checkOut: this.fb.group({
        desde: [this.hotel?.checkOut.desde, Validators.required],
        hasta: [this.hotel?.checkOut.hasta, Validators.required],
      }),
      categoria: [this.hotel?.categoria, Validators.required],
      descripcion: [this.hotel?.descripcion, Validators.required],
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  cargarImagenes() {
    this.hotel.imagenes.map((imagen) => {
      this.initialImages.push(imagen);
    });
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    this.seleccionandoImagenes = true;
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      this.previewUrls = [];
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }
  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      if (this.uploadFiles.length === 0) {
        this.forma.value.imagenes = this.initialImages;
      }
      this.hotelService
        .editarHotel(this.id, this.forma.value)
        .subscribe(async (resp) => {
          if (resp["ok"] == false) {
            this.store.dispatch(stopLoading());

            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["hotelDB"]._id;
            if (this.uploadFiles.length != 0) {
              this.hotelService
                ._cargarImagenesHotel(formData, id)
                .subscribe(async (resp) => {
                  this.store.dispatch(stopLoading());
                  Swal.fire({
                    icon: "success",
                    title: "Hotel actualizado",
                    showConfirmButton: false,
                    timer: 2500,
                  });
                  this.store.dispatch(stopLoading());
                  this.forma.reset();
                  setTimeout(async () => {
                    await this.router.navigate(["/editar"]);
                    window.location.reload();
                  }, 2500);
                });
            } else {
              Swal.fire({
                icon: "success",
                title: "Hotel actualizado",
                showConfirmButton: false,
                timer: 1000,
              });
              this.store.dispatch(stopLoading());
              this.forma.reset();
              await this.router.navigate(["/editar"]);
              window.location.reload();
            }
          }
        });
    }
  }
}
