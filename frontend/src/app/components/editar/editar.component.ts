import { Component, OnInit, OnDestroy } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducer";
import { filtrosValidos, setFiltro } from "../filtro.actions";
import { Subscription } from "rxjs";
import { PlanesModel } from "src/app/models/plan.model";
import { HotelInterface } from "src/app/models/hotel.model";
import { HabitacionInterface } from "src/app/models/habitacion.model";
import { Router } from '@angular/router';
import { InterfaceRestaurante } from 'src/app/models/restaurante.model';

@Component({
  selector: "app-editar",
  templateUrl: "./editar.component.html",
  styleUrls: ["./editar.component.css"],
})
export class EditarComponent implements OnInit, OnDestroy {
  filtroLista: filtrosValidos[] = [
    "ninguno",
    "planes",
    "hoteles",
    "habitaciones",
    "restaurantes"
  ];
  filtroActual: filtrosValidos = "ninguno";
  filtroSubscription: Subscription;
  planesSubscription: Subscription;
  hotelesSubscription: Subscription;
  habitacionesSubscription: Subscription;
  restaurantesSubscription:Subscription;
  planes: PlanesModel[] = [];
  hoteles: HotelInterface[] = [];
  habitaciones: HabitacionInterface[] = [];
  restaurantes:InterfaceRestaurante[] = [];


  constructor(
    private store: Store<AppState>,
    private router:Router
  ) {
  }

  ngOnInit(): void {
    this.subscriptionFiltro();
    this.subscriptionPlanes();
    this.subscriptionHoteles();
    this.subscriptionHabitaciones();
    this.subscriptionRestaurantes();

  }

  ngOnDestroy(): void {
    this.filtroSubscription?.unsubscribe();
    this.planesSubscription?.unsubscribe();
    this.hotelesSubscription?.unsubscribe();
    this.habitacionesSubscription?.unsubscribe();
    this.restaurantesSubscription?.unsubscribe();
  }

  subscriptionFiltro() {
    this.filtroSubscription = this.store
      .select("filtro")
      .subscribe(({ filtro }) => {
        this.filtroActual = filtro;
      });
  }
  subscriptionPlanes() {
    this.planesSubscription = this.store
      .select("planes")
      .subscribe(({ planes }) => {
        this.planes = [];
        Object.keys(planes).forEach((key) => {
          let planesTemp: PlanesModel = planes[key];
          this.planes.push(planesTemp);
        });
      });
  }
  subscriptionHoteles() {
    this.hotelesSubscription = this.store
      .select("hoteles")
      .subscribe(({ hoteles }) => {
        this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
      });
  }
  subscriptionHabitaciones() {
    this.habitacionesSubscription = this.store
      .select("habitaciones")
      .subscribe(({ habitaciones }) => {
        this.habitaciones = [];
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          this.habitaciones.push(habitacionesTemp);
        });
      });
  }
  subscriptionRestaurantes(){
    this.restaurantesSubscription = this.store.select('restaurantes').subscribe(({restaurantes})=>{
      this.restaurantes = [];
      Object.keys(restaurantes).forEach((key) => {
        let restaurantesTemp: InterfaceRestaurante = restaurantes[key];
        this.restaurantes.push(restaurantesTemp);
      });
    });
  }

  aplicarFiltro(filtro: filtrosValidos) {
    this.store.dispatch(setFiltro({ filtro: filtro }));
  }

  editarPlan(plan: PlanesModel) {
    this.router.navigate(['/editarPlan',plan._id]);
  }
  editarHotel(hotel: HotelInterface) {
    this.router.navigate(['/editarHotel',hotel._id]);
  }
  editarHabitacion(habitacion: HabitacionInterface) {
    this.router.navigate(['/editarHabitacion',habitacion._id]);
  }
  editarRestaurante(restaurante:InterfaceRestaurante){
    this.router.navigate(['/editarRestaurante',restaurante._id]);

  }
}
