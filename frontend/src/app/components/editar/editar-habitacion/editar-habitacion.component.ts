import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HabitacionInterface } from '../../../models/habitacion.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { HabitacionService } from '../../../services/habitacion.service';
import { HotelInterface } from 'src/app/models/hotel.model';
import { isLoading } from 'src/app/shared/ui.actions';
import Swal from 'sweetalert2';
import { stopLoading } from '../../../shared/ui.actions';

@Component({
  selector: 'app-editar-habitacion',
  templateUrl: './editar-habitacion.component.html',
  styleUrls: ['./editar-habitacion.component.css']
})
export class EditarHabitacionComponent implements OnInit, OnDestroy {
  id: string;
  habitacion: HabitacionInterface;
  habitaciones: HabitacionInterface[] = [];
  hoteles: HotelInterface[] = [];
  hotel: HotelInterface;
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  initialImages: Array<string> = [];
  seleccionandoImagenes: boolean = false;
  forma: FormGroup;
  cargando: boolean = false;
  cargandoSubscription: Subscription;
  habitacionSubscription: Subscription;
  hotelSubscription: Subscription;
  constructor(
    private activateRoute: ActivatedRoute,
    private store: Store<AppState>,
    private fb: FormBuilder,
    private habitacionService: HabitacionService,
    private router: Router
    ) { 
      this.activateRoute.params.subscribe((params) => {
        this.id = params["id"];
      });
    this.store.dispatch(stopLoading());

      this.obtenerHabitacion();
      this.obtenerHotel();      
      
    }
  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.habitacionSubscription?.unsubscribe();
    this.hotelSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.crearFormulario();
    this.cargarImagenes();
  }

  obtenerHabitacion() {
    this.habitacionSubscription = this.store
      .select("habitaciones")
      .subscribe(({ habitaciones }) => {
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          this.habitaciones.push(habitacionesTemp);
        });
        this.habitacion = this.habitaciones.find((habitacion) => {
          if (habitacion._id === this.id) {
            return habitacion;
          }
        });
      });
  }
  obtenerHotel(){
    this.hotelSubscription = this.store.select('hoteles').subscribe(({hoteles})=>{
      Object.keys(hoteles).forEach((key) => {
        let hotelesTemp: HotelInterface = hoteles[key];
        this.hoteles.push(hotelesTemp);
      });
      this.hotel = this.hoteles.find((hotel)=>{
        if (hotel._id === this.habitacion.id_hotel) {
          return hotel;
        }
      });
    });
  }
  get id_hotelNoValido() {
    return (
      this.forma.controls.id_hotel.invalid &&
      this.forma.controls.id_hotel.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get tipoNoValido() {
    return this.forma.controls.tipo.invalid && this.forma.controls.tipo.touched;
  }
  get precioNoValido() {
    return (
      this.forma.controls.precio.invalid && this.forma.controls.precio.touched
    );
  }
  get personasNoValido() {
    return (
      this.forma.controls.personas.invalid &&
      this.forma.controls.personas.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  crearFormulario() {
    this.forma = this.fb.group({
      id_hotel: [this.habitacion?.id_hotel, Validators.required],
      imagenes: [""],
      tipo: [this.habitacion?.tipo, Validators.required],
      precio: [this.habitacion?.precio, Validators.required],
      personas: [this.habitacion?.personas, Validators.required],
      descripcion: [this.habitacion?.descripcion, Validators.required],
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  cargarImagenes() {
    this.habitacion.imagenes.map((imagen) => {
      this.initialImages.push(imagen);
    });
  }
  onFileChange(event) {
    this.seleccionandoImagenes = true;
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      this.previewUrls = [];
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }
  guardar(){
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.store.dispatch(isLoading());
      if (this.uploadFiles.length === 0) {
        this.forma.value.imagenes = this.initialImages;
      }
      this.habitacionService
        .editarHabitacion(this.id, this.forma.value)
        .subscribe(async (resp) => {
          if (resp["ok"] == false) {
            this.store.dispatch(stopLoading());
            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["habitacionDB"]._id;
            if (this.uploadFiles.length != 0) {
              this.habitacionService
                ._cargarImagenesHabitacion(formData, id)
                .subscribe(async (resp) => {
                  Swal.fire({
                    icon: "success",
                    title: "Habitacion actualizada",
                    showConfirmButton: false,
                    timer: 1500,
                  });
                  this.store.dispatch(stopLoading());
                  this.forma.reset();
                  await this.router.navigate(["/editar"]);
                  window.location.reload();
                });
            } else {
              Swal.fire({
                icon: "success",
                title: "Habitacion actualizada",
                showConfirmButton: false,
                timer: 1000,
              });
              this.store.dispatch(stopLoading());
              this.forma.reset();
              await this.router.navigate(["/editar"]);
              window.location.reload();
            }
          }
        });
    }
  }
}
