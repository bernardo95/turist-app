import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlanesModel } from 'src/app/models/plan.model';
import { HotelInterface } from 'src/app/models/hotel.model';
import { HabitacionInterface } from 'src/app/models/habitacion.model';
import { filtrosValidos, setFiltro } from '../filtro.actions';
import { Subscription } from 'rxjs';
import { PlanService } from 'src/app/services/plan.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { HotelService } from 'src/app/services/hotel.service';
import { HabitacionService } from 'src/app/services/habitacion.service';
import Swal from 'sweetalert2';
import { unSetPlanes, setPlanes } from '../planes.actions';
import { unSetHoteles, setHoteles } from '../hoteles.actions';
import { unSetHabitaciones, setHabitaciones } from '../habitaciones.actions';
import { InterfaceRestaurante } from '../../models/restaurante.model';
import { RestauranteService } from '../../services/restaurante.service';
import { unSetRestaurantes, setRestaurantes } from '../restaurantes.actions';

@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit, OnDestroy {
  planes: PlanesModel[] = [];
  hoteles: HotelInterface[] = [];
  habitaciones: HabitacionInterface[] = [];
  restaurantes:InterfaceRestaurante[] = [];
  filtroLista: filtrosValidos[] = ['ninguno','planes','hoteles','habitaciones','restaurantes'];
  filtroActual:filtrosValidos = 'ninguno';
  planesSubscription: Subscription;
  hotelesSubscription: Subscription;
  habitacionesSubscription:Subscription;
  restaurantesSubscription:Subscription;
  filtroSubscription:Subscription;
  constructor(
    private planService: PlanService,
    private store: Store<AppState>,
    private hotelService:HotelService,
    private habitacionService:HabitacionService,
    private restaurantesService:RestauranteService
  ) { }
  ngOnDestroy(): void {
    this.planesSubscription?.unsubscribe();
    this.hotelesSubscription?.unsubscribe();
    this.habitacionesSubscription?.unsubscribe();
    this.restaurantesSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptionFiltro();
    this.subscriptionPlanes();
    this.subscriptionHoteles();
    this.subscriptionHabitaciones();
    this.subscriptionRestaurantes();
  }

  subscriptionPlanes() {
    this.planesSubscription = this.store
      .select("planes")
      .subscribe(({ planes }) => {
        this.planes = [];
        Object.keys(planes).forEach((key) => {
          let planesTemp: PlanesModel = planes[key];
          this.planes.push(planesTemp);
        });
      });
  }

  subscriptionHoteles() {
    this.hotelesSubscription = this.store
      .select("hoteles")
      .subscribe(({ hoteles }) => {
        this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
      });
  }

  subscriptionHabitaciones(){
    this.habitacionesSubscription = this.store
      .select("habitaciones")
      .subscribe(({ habitaciones }) => {  
        this.habitaciones = [];
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          this.habitaciones.push(habitacionesTemp);
        });
      });
  }
  subscriptionRestaurantes(){
    this.restaurantesSubscription = this.store.select('restaurantes').subscribe(({restaurantes})=>{
      this.restaurantes = [];
      Object.keys(restaurantes).forEach((key) => {
        let restaurantesTemp: InterfaceRestaurante = restaurantes[key];
        this.restaurantes.push(restaurantesTemp);
      });
    });
  }

  subscriptionFiltro(){
    this.filtroSubscription = this.store.select('filtro').subscribe(({filtro})=>{
      this.filtroActual = filtro;
    });
  }

  async eliminarPlan(removePlan: PlanesModel) {
    console.log(removePlan._id);
    // return ;
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
    this.planService.eliminarPlan(removePlan._id).subscribe((resp) => {
      console.log(resp);
      // const planResponse: PlanesModel = resp["planDB"];
      let newArrayPlanes: PlanesModel[] = this.planes.filter((plan)=>{
        if (plan._id != removePlan._id) {
          return plan
        }
      });
      Swal.fire({
        icon: "success",
        title: "Plan Eliminado",
        showConfirmButton: false,
        timer: 1000,
      });
      this.planes = [];
      this.store.dispatch(unSetPlanes());
      this.store.dispatch(setPlanes({ planes: newArrayPlanes }));
    });
    }
    
  }
  async eliminarHotel(removeHotel:HotelInterface){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
    this.hotelService.eliminarHotel(removeHotel._id).subscribe((resp) => {
      // const hotelResponse: HotelInterface = resp["hotelDB"];
      let newArrayHoteles: HotelInterface[] = this.hoteles.filter((hotel)=>{
        if (hotel._id != removeHotel._id) {
          return hotel
        }
      });
      this.hoteles = [];
      this.store.dispatch(unSetHoteles());
      this.store.dispatch(setHoteles({ hoteles: newArrayHoteles }));
      Swal.fire({
        icon: "success",
        title: "Hotel Eliminado",
        showConfirmButton: false,
        timer: 1000,
      });
    });
    }
  }
  async eliminarHabitacion(removeHabitacion:HabitacionInterface){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
    this.habitacionService.eliminarHabitacion(removeHabitacion._id).subscribe((resp) => {
      let newArrayHabitaciones: HabitacionInterface[] = this.habitaciones.filter((habitacion)=>{
        if (habitacion._id != removeHabitacion._id) {
          return habitacion
        }
      });
      Swal.fire({
        icon: "success",
        title: "Habitacion eliminada",
        showConfirmButton: false,
        timer: 1000,
      });
      this.habitaciones = [];
      this.store.dispatch(unSetHabitaciones());
      this.store.dispatch(setHabitaciones({ habitaciones: newArrayHabitaciones }));
    });
    }
    
  }
  async eliminarRestaurante(removeRestaurante:InterfaceRestaurante){
    const _swal = await Swal.fire({
      title: '¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    });
    if (_swal.value) {
    this.restaurantesService.eliminarRestaurante(removeRestaurante._id).subscribe((resp) => {
      let newArrayRestaurante: InterfaceRestaurante[] = this.restaurantes.filter((restaurante)=>{
        if (restaurante._id != removeRestaurante._id) {
          return restaurante
        }
      });
      Swal.fire({
        icon: "success",
        title: "Restaurante eliminado",
        showConfirmButton: false,
        timer: 1000,
      });
      this.restaurantes = [];
      this.store.dispatch(unSetRestaurantes());
      this.store.dispatch(setRestaurantes({ restaurantes: newArrayRestaurante }));
    });
    }
    
  }
  aplicarFiltro(filtro:filtrosValidos){
    this.store.dispatch(setFiltro({filtro:filtro}));
  }

}
