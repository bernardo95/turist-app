import { createAction, props } from '@ngrx/store';
import { HabitacionInterface } from '../models/habitacion.model';

export const setHabitaciones = createAction('[Habitaciones Component] Set Habitaciones',props<{habitaciones:HabitacionInterface[]}>());
export const unSetHabitaciones = createAction('[Habitaciones Component] unSet Habitaciones');