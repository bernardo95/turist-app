import { createReducer, on } from '@ngrx/store';
import { setPlanes,unSetPlanes,editarPlan } from './planes.actions';
import { PlanesModel } from '../models/plan.model';

export interface State {
    planes: PlanesModel[]; 
}

export const initialState: State = {
   planes: [],
}

const _planesReducer = createReducer(initialState,

    on(setPlanes, (state,{planes}) => ({ ...state, planes: {...planes}})),
    on(unSetPlanes, state => ({ ...state, planes: []})),
);

export function planesReducer(state, action) {
    return _planesReducer(state, action);
}