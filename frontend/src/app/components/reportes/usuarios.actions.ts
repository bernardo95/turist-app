import { createAction, props } from '@ngrx/store';
import { UsuarioInterface } from '../../models/usuario.model';

export const setUsuarios = createAction('[Usuarios Reportes] setUsuarios',props<{usuarios:UsuarioInterface[]}>());
export const unSetUsuarios = createAction('[Usuarios Reportes] unSetusuarios');
