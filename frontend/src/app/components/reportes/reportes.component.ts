import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.reducer';
import { Subscription } from 'rxjs';
import { ReservaPlanInterface, ReservaHabitacionInterface } from '../../models/reserva.model';
import { filtroReservaValidos } from '../reservas_habitaciones.actions';
import { setFiltroSegundario } from '../reservas/filtro_segundario.actions';
import { PlanInterface } from '../../models/plan.model';
import { HotelInterface } from '../../models/hotel.model';
import { HabitacionInterface } from '../../models/habitacion.model';


@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit , OnDestroy{
  planes:PlanInterface[]=[];
  plan:PlanInterface;
  hoteles:HotelInterface[]=[];
  hotel:HotelInterface;
  habitaciones:HabitacionInterface[]=[];
  habitacionesFiltradas:HabitacionInterface[]=[];
  habitacion:HabitacionInterface;
  habitacionSeleccionadas:ReservaHabitacionInterface[];
  reserva:ReservaHabitacionInterface;
  habitacionTop:HabitacionInterface;
  hotelTop:HotelInterface;
  habitacionTopCount:number;
  planTop:PlanInterface;
  planTopCount:number;
  reservasPlanes:ReservaPlanInterface[]= [];
  reservaHabitaciones:ReservaHabitacionInterface[]=[];
  filtrosPrimario:filtroReservaValidos[]=["HABITACIONES","PLANES","NINGUNO"];
  filtroActualPrimario = "PLANES";
  reservasFiltradasPlan:ReservaPlanInterface[];
  reservasFiltradasHabitaciones:ReservaHabitacionInterface[];
  reservaLength:number;
  reservasActivas:number;
  reservasCanceladas:number;
  reservasFinalizadas:number;
  reservaHabitacionesLength:number;
  reservasHabitacionesActivas:number;
  reservasHabitacionesCanceladas:number;
  reservasHabitacionesFinalizadas:number;
  subscriptionReservasPlanes: Subscription;
  subscriptionPlanes:Subscription;
  subscriptionReservaHabitaciones: Subscription;
  subscriptionHoteles:Subscription;
  subscriptionHabitaciones:Subscription;
  filtroPrimarioSubscription: Subscription;

  
  constructor(
    private store:Store<AppState>,
    ) {
      this.subcriptionPlanes();
      this.subcriptionReservaPlanes();
      this.subscriptionHotelesMethod();
      this.subscriptionHabitacionesMethod();
      this.subcriptionReservaHabitaciones();
      this.subscriptionFiltro();
   }
  ngOnDestroy(): void {
    this.subscriptionPlanes?.unsubscribe();
    this.subscriptionReservasPlanes?.unsubscribe();
    this.subscriptionReservaHabitaciones?.unsubscribe();
    this.subscriptionHoteles?.unsubscribe();
    this.subscriptionHabitaciones?.unsubscribe();
    this.filtroPrimarioSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }
  subcriptionPlanes(){
    this.subscriptionPlanes = this.store.select('planes').subscribe(({planes})=>{
      this.planes = [];
        Object.keys(planes).forEach((key) => {
          let planTemp: PlanInterface = planes[key];
          this.planes.push(planTemp);
        });
    });
  }
  subscriptionHotelesMethod(){
    this.subscriptionHoteles = this.store.select('hoteles').subscribe(({hoteles})=>{
      this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
          if (this.habitacionTop!=null) {
            this.hotelTopMethod();
          }
        });
    });
  }

  subscriptionHabitacionesMethod(){
this.subscriptionReservaHabitaciones = this.store.select('habitaciones').subscribe(({habitaciones})=>{
      this.habitaciones = [];
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          this.habitaciones.push(habitacionesTemp);
          if (this.habitacionTop!=null) {
            this.hotelTopMethod();
          }
        });
    });
  }

  subcriptionReservaPlanes() {
    this.subscriptionReservasPlanes = this.store
      .select("reserva_planes")
      .subscribe(({ reservas_planes }) => {
        this.reservasPlanes = [];
        Object.keys(reservas_planes).forEach((key) => {
          let reservaTemp: ReservaPlanInterface = reservas_planes[key];
          this.reservasPlanes.push(reservaTemp);
        });
        this.reservasTop();

      });

  }

  subcriptionReservaHabitaciones() {
    this.subscriptionReservaHabitaciones = this.store
      .select("reserva_habitaciones")
      .subscribe(({ reservas_habitaciones }) => {
        this.reservaHabitaciones = [];
        Object.keys(reservas_habitaciones).forEach((key) => {

          let reservaTemp: ReservaHabitacionInterface =
            reservas_habitaciones[key];
            
          this.reservaHabitaciones.push(reservaTemp);
        });
        this.habitacionesTop();
        if (this.habitacionTop!=null) {
          this.hotelTopMethod();
        }
      });
  }
  subscriptionFiltro(){
    this.filtroPrimarioSubscription = this.store.select('filtro_segundario').subscribe(({filtro_segundario})=>{
      this.filtroActualPrimario = filtro_segundario;
      
    });
  }
  aplicarFiltroPrimario(filtro:filtroReservaValidos){
    
    this.store.dispatch(setFiltroSegundario({filtro_segundario:filtro}));    
  }
  mostrarInfoPlan(plan:PlanInterface){
    this.plan = plan;
    this.reservasFiltradasPlan = this.reservasPlanes?.filter((reserva)=> reserva.id_plan == plan?._id);
    this.reservaLength = this.reservasFiltradasPlan?.length;
    this.reservasActivas = this.reservasFiltradasPlan?.filter((reserva)=>reserva.estado==='ACTIVO').length;
    this.reservasCanceladas = this.reservasFiltradasPlan?.filter((reserva)=>reserva.estado==='CANCELADO').length;
    this.reservasFinalizadas = this.reservasFiltradasPlan?.filter((reserva)=>reserva.estado==='FINALIZADO').length;
  }
  mostrarInfoReservaHotel(hotel:HotelInterface){
    this.hotel = hotel;
    this.reservasFiltradasHabitaciones = this.reservaHabitaciones?.filter((reserva)=> reserva.hotel[0]._id==hotel._id);
    this.reservaLength = this.reservasFiltradasHabitaciones?.length;
    this.reservasActivas = this.reservasFiltradasHabitaciones?.filter((reserva)=>reserva.estado==='ACTIVO').length;
    this.reservasCanceladas = this.reservasFiltradasHabitaciones?.filter((reserva)=>reserva.estado==='CANCELADO').length;
    this.reservasFinalizadas = this.reservasFiltradasHabitaciones?.filter((reserva)=>reserva.estado==='FINALIZADO').length;
    this.habitacionesFiltradas = this.habitaciones?.filter((habitacion)=>habitacion.id_hotel==this.hotel._id);

  }
  mostrarInfoReservaHabitacion(habitacionParam:HabitacionInterface){
    this.habitacion = this.habitacionesFiltradas?.find((habitacion)=>habitacion._id===habitacionParam._id);
    this.habitacionSeleccionadas = this.reservaHabitaciones?.filter((reserva)=>reserva.id_habitacion==habitacionParam._id);
    this.reservaHabitacionesLength = this.habitacionSeleccionadas?.length;
    this.reservasHabitacionesActivas = this.habitacionSeleccionadas?.filter((reserva)=>reserva.estado==='ACTIVO').length;
    this.reservasHabitacionesCanceladas = this.habitacionSeleccionadas?.filter((reserva)=>reserva.estado==='CANCELADO').length;
    this.reservasHabitacionesFinalizadas = this.habitacionSeleccionadas?.filter((reserva)=>reserva.estado==='FINALIZADO').length;
  }
  reservasTop(){
    let acumuladorReservas = [];
    let reservas: Map<String,number> = new Map();
    
    this.reservasPlanes.forEach((reserva)=>{
      acumuladorReservas.push(reserva.plan[0].nombre);
    });
    let topReserva;
    let topCount = 0;
    let contador = 0;

    acumuladorReservas.forEach((reserva)=>{
      if (reservas.has(reserva)) {
        contador = reservas.get(reserva)+1;
        reservas.set(reserva,contador);
      }else{
        reservas.set(reserva,1);
      }
    });
    
    reservas.forEach((value,key)=>{
      if (value>=topCount) {
        topReserva = key;
        topCount = value;
        this.planTopCount = value;
      }
    });
    
    this.planTop = this.planes.find((plan)=>plan.nombre==topReserva);
    
  }
  habitacionesTop(){
    let acumuladorReservas = [];
    let reservas: Map<String,number> = new Map();
    
    this.reservaHabitaciones.forEach((reserva)=>{
      acumuladorReservas.push(reserva.habitacion[0]._id);
    });
    
    let topReserva;
    let topCount = 0;
    let contador = 0;

    acumuladorReservas.forEach((reserva)=>{
      if (reservas.has(reserva)) {
        contador = reservas.get(reserva)+1;
        reservas.set(reserva,contador);
      }else{
        reservas.set(reserva,1);
      }
    });
    
    reservas.forEach((value,key)=>{
      if (value>=topCount) {
        topReserva = key;
        topCount = value;
        this.habitacionTopCount = value;
      }
    });
    this.habitacionTop = this.habitaciones.find((habitacion)=>habitacion._id==topReserva);
    
  }
  hotelTopMethod(){
    this.hotelTop = this.hoteles.find((hotel)=> hotel._id==this.habitacionTop.id_hotel);
  }
}
