import { createReducer, on } from '@ngrx/store';
import { setUsuarios,unSetUsuarios } from './usuarios.actions';
import { UsuarioInterface } from '../../models/usuario.model';

export interface State {
    usuarios: UsuarioInterface[]; 
}

export const initialState: State = {
   usuarios: [],
}

const _usuariosReducer = createReducer(initialState,
    on(setUsuarios, (state,{usuarios}) => ({ ...state, usuarios: {...usuarios}})),
    on(unSetUsuarios, state => ({ ...state, usuarios: []})),

);

export function usuariosReducer(state, action) {
    return _usuariosReducer(state, action);
}