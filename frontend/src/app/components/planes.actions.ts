import { createAction, props } from '@ngrx/store';
import { PlanesModel } from '../models/plan.model';

export const setPlanes = createAction('[Planes Component] Set Planes',props<{planes:PlanesModel[]}>());
export const unSetPlanes = createAction('[Planes Component] UnSet Planes');
export const editarPlan = createAction('[Planes Component] Editar Plan',props<{plan:PlanesModel}>());
export const eliminarPlan = createAction('[Planes Component] Eliminar Plan',props<{id:string}>());