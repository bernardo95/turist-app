import { createAction, props } from '@ngrx/store';
import { estadosValidosReservas } from '../reservas_habitaciones.actions';

export const setFiltroPrimario = createAction('[Filtro Primario Component] Set Filtro',props<{filtro_primario:estadosValidosReservas}>());