import { Component, OnInit, OnDestroy } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducer";
import { ReservasService } from "../../services/reservas.service";
import {
  ReservaPlanInterface,
  ReservaHabitacionInterface,
} from "../../models/reserva.model";
import { Subscription } from "rxjs";
import { estadosValidosReservas, filtroReservaValidos } from '../reservas_habitaciones.actions';
import { Router } from '@angular/router';
import { setFiltroPrimario } from './filtro_primario.actions';
import { setFiltroSegundario } from './filtro_segundario.actions';

@Component({
  selector: "app-reservas",
  templateUrl: "./reservas.component.html",
  styleUrls: ["./reservas.component.css"],
})
export class ReservasComponent implements OnInit, OnDestroy {
  filtrosPrimarios:estadosValidosReservas[]=["ACTIVO","CANCELADO","FINALIZADO"];
  filtrosSegundario:filtroReservaValidos[]=["HABITACIONES","PLANES","NINGUNO"];
  filtroActualPrimario = 'ACTIVO';
  filtroActualSegundario = 'NINGUNO';
  reservasPlanes: ReservaPlanInterface[] = [];
  reservaHabitaciones: ReservaHabitacionInterface[] = [];
  reservaHotelesSubscription: Subscription;
  reservaHabitacionesSubscription: Subscription;
  filtroPrimarioSubscription:Subscription;
  filtroSegundarioSubscription:Subscription;

  constructor(
    private store: Store<AppState>,
    private router:Router
  ) {
    this.subscriptionReservasHoteles();
    this.subscriptionReservasHabitaciones();
  }

  ngOnInit(): void {
    this.subscriptionFiltro();
  }
  ngOnDestroy(): void {
    this.reservaHotelesSubscription?.unsubscribe();
    this.reservaHabitacionesSubscription?.unsubscribe();
  }
  subscriptionFiltro(){
    this.filtroPrimarioSubscription = this.store.select('filtro_primario').subscribe(({filtro_primario})=>{
      this.filtroActualPrimario = filtro_primario;
    });
    this.filtroSegundarioSubscription = this.store.select('filtro_segundario').subscribe(({filtro_segundario})=>{
      this.filtroActualSegundario = filtro_segundario;
    });
  }

  subscriptionReservasHoteles() {
    this.reservaHotelesSubscription = this.store
      .select("reserva_planes")
      .subscribe(({ reservas_planes }) => {
        this.reservasPlanes = [];
        Object.keys(reservas_planes).forEach((key) => {
          let reservaTemp: ReservaPlanInterface = reservas_planes[key];
          this.reservasPlanes.push(reservaTemp);
        });
      });
  }

  subscriptionReservasHabitaciones() {
    this.reservaHabitacionesSubscription = this.store
      .select("reserva_habitaciones")
      .subscribe(({ reservas_habitaciones }) => {
        this.reservaHabitaciones = [];
        Object.keys(reservas_habitaciones).forEach((key) => {

          let reservaTemp: ReservaHabitacionInterface =
            reservas_habitaciones[key];
            
          this.reservaHabitaciones.push(reservaTemp);
        });
      });
  }
  editarReservaHotel(reserva:ReservaPlanInterface){
    this.router.navigate(['/editarReservaPlan',reserva._id]);
  }
  editarReservaHabitacion(reserva:ReservaHabitacionInterface){
    this.router.navigate(['/editarReservaHabitacion',reserva._id]);
  }

  aplicarFiltroPrimario(filtro:estadosValidosReservas){
    this.store.dispatch(setFiltroPrimario({filtro_primario:filtro}));
  }
  aplicarFiltroSegundario(filtro:filtroReservaValidos){
    this.store.dispatch(setFiltroSegundario({filtro_segundario:filtro}));
  }
}
