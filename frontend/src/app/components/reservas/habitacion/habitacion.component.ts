import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { estadosValidosReservas } from '../../reservas_habitaciones.actions';
import { ReservaHabitacionInterface } from '../../../models/reserva.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { ReservasService } from 'src/app/services/reservas.service';
import { isLoading, stopLoading } from 'src/app/shared/ui.actions';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-habitacion',
  templateUrl: './habitacion.component.html',
  styleUrls: ['./habitacion.component.css']
})
export class HabitacionComponent implements OnInit,OnDestroy {
  id: string;
  reserva:ReservaHabitacionInterface;
  reservas:ReservaHabitacionInterface[]= [];
  forma: FormGroup;
  estados:estadosValidosReservas[]=["ACTIVO","CANCELADO","FINALIZADO"];
  cargando: boolean = false;
  reservasSubscription:Subscription;
  cargandoSubscription: Subscription;


  constructor(
    private activateRoute: ActivatedRoute,
    private store:Store<AppState>,
    private fb: FormBuilder,
    private router: Router,
    private reservaService:ReservasService
  ) { 
    this.activateRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.obtenerReserva();
  }
  ngOnDestroy(): void {
    this.reservasSubscription?.unsubscribe();
    this.cargandoSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
    .select("ui")
    .subscribe(({ isLoading }) => {
      this.cargando = isLoading;
    });
    this.crearFormulario();
  }
  obtenerReserva(){
    this.reservasSubscription = this.store.select('reserva_habitaciones').subscribe(({reservas_habitaciones})=>{
      Object.keys(reservas_habitaciones).forEach((key) => {
        let reservaTmp: ReservaHabitacionInterface = reservas_habitaciones[key];
        this.reservas.push(reservaTmp);
      });
      this.reserva = this.reservas.find((reserva)=>{
        return reserva._id == this.id;
      });
    });
  }
  crearFormulario(){
    this.forma = this.fb.group({
      estado:[this.reserva.estado,Validators.required]
    })
  }
  editarReservaHabitacion(){
    this.store.dispatch(isLoading());
    this.reservaService.updateReservaHabitacion(this.id,this.forma.value.estado).subscribe(async (resp)=>{
      if (resp['ok']==true) {
        this.store.dispatch(stopLoading());
        Swal.fire({
          icon: "success",
          title: "Reserva actualizada",
          showConfirmButton: false,
          timer: 1000,
        });
        await this.router.navigate(['/reservas']);
        window.location.reload();
      } else {
        this.store.dispatch(stopLoading());
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: resp["error"],
        });
        return;
      }
      
    });
    
  }
}
