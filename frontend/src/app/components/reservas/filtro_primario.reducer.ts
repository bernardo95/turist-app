import { createReducer, on } from '@ngrx/store';
import { setFiltroPrimario } from './filtro_primario.actions';
import { estadosValidosReservas } from '../reservas_habitaciones.actions';

export interface State {
    filtro_primario: estadosValidosReservas; 
}

export const initialState: State = {
    filtro_primario: 'NINGUNO',
}

const _filtroPrimarioReducer = createReducer(initialState,

    on(setFiltroPrimario, (state,{filtro_primario}) => ({filtro_primario})),

);

export function filtroPrimarioReducer(state, action) {
    return _filtroPrimarioReducer(state, action);
}