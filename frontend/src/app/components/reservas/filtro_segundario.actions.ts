import { createAction, props } from '@ngrx/store';
import { filtroReservaValidos } from '../reservas_habitaciones.actions';

export const setFiltroSegundario = createAction('[Filtro Segundario Component] Set Filtro',props<{filtro_segundario:filtroReservaValidos}>());
