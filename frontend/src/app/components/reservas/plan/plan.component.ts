import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.reducer';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReservasService } from '../../../services/reservas.service';
import { Subscription } from 'rxjs';
import { ReservaPlanInterface } from '../../../models/reserva.model';
import { estadosValidosReservas } from '../../reservas_habitaciones.actions';
import { isLoading, stopLoading } from '../../../shared/ui.actions';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit,OnDestroy {
  id: string;
  reserva:ReservaPlanInterface;
  reservas:ReservaPlanInterface[]= [];
  forma: FormGroup;
  estados:estadosValidosReservas[]=["ACTIVO","CANCELADO","FINALIZADO"];
  cargando: boolean = false;
  reservasSubscription:Subscription;
  cargandoSubscription: Subscription;
  
  constructor(
    private activateRoute: ActivatedRoute,
    private store:Store<AppState>,
    private fb: FormBuilder,
    private router: Router,
    private reservaService:ReservasService
    
  ) {
    this.activateRoute.params.subscribe((params) => {
      this.id = params["id"];
    });
    this.obtenerReserva();
   }
  ngOnDestroy(): void {
    this.reservasSubscription?.unsubscribe();
    this.cargandoSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
    .select("ui")
    .subscribe(({ isLoading }) => {
      this.cargando = isLoading;
    });
    this.store.dispatch(stopLoading());
    this.crearFormulario();
    
  }
  obtenerReserva(){
    this.reservasSubscription = this.store.select('reserva_planes').subscribe(({reservas_planes})=>{
      Object.keys(reservas_planes).forEach((key) => {
        let reservaTmp: ReservaPlanInterface = reservas_planes[key];
        this.reservas.push(reservaTmp);
      });
      this.reserva = this.reservas.find((reserva)=>{
        return reserva._id == this.id;
      });
    });
  }
  crearFormulario(){
    this.forma = this.fb.group({
      estado:[this.reserva.estado,Validators.required]
    })
  }
  editarReservaHotel(){
    this.store.dispatch(isLoading());
    this.reservaService.updateReservasPlanes(this.id,this.forma.value.estado).subscribe(async (resp)=>{
      if (resp['ok']==true) {
        this.store.dispatch(stopLoading());
        Swal.fire({
          icon: "success",
          title: "Reserva actualizada",
          showConfirmButton: false,
          timer: 1000,
        });
        await this.router.navigate(['/reservas']);
        window.location.reload();
      } else {
        this.store.dispatch(stopLoading());
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: resp["error"],
        });
        return;
      }
      
    });
    
  }
}
