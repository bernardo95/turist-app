import { createReducer, on } from '@ngrx/store';
import { setFiltroSegundario } from './filtro_segundario.actions';
import { filtroReservaValidos } from '../reservas_habitaciones.actions';

export interface State {
    filtro_segundario: filtroReservaValidos; 
}

export const initialState: State = {
   filtro_segundario: 'NINGUNO',
}

const _filtroSegundarioReducer = createReducer(initialState,

    on(setFiltroSegundario, (state,{filtro_segundario}) => ({filtro_segundario})),

);

export function filtroSegundarioReducer(state, action) {
    return _filtroSegundarioReducer(state, action);
}