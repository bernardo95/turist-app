import { createReducer, on } from '@ngrx/store';
import { setHabitaciones,unSetHabitaciones } from './habitaciones.actions';
import { HabitacionInterface } from '../models/habitacion.model';

export interface State {
    habitaciones: HabitacionInterface[]; 
}

export const initialState: State = {
   habitaciones: [],
}

const _habitacionesReducer = createReducer(initialState,

    on(setHabitaciones, (state,{habitaciones}) => ({ ...state, habitaciones: {...habitaciones}})),
    on(unSetHabitaciones, (state) => ({ ...state, habitaciones: []})),
);

export function habitacionesReducer(state, action) {
    return _habitacionesReducer(state, action);
}