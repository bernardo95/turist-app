import { createAction, props } from '@ngrx/store';
import { InterfaceRestaurante } from '../models/restaurante.model';

export const setRestaurantes = createAction('[Restaurantes Component] Set Restaurantes',props<{restaurantes:InterfaceRestaurante[]}>());
export const unSetRestaurantes = createAction('[Restaurantes Component] unSet Restaurantes');