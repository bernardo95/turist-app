import { createReducer, on } from '@ngrx/store';
import { setReservasPlanes,unSetReservasPlanes } from './reservas_planes.actions';
import { ReservaPlanInterface } from '../models/reserva.model';

export interface State {
    reservas_planes: ReservaPlanInterface[]; 
}

export const initialState: State = {
    reservas_planes: [],
}

const _reservasPlanesReducer = createReducer(initialState,

    on(setReservasPlanes, (state, { reservas_planes }) => ({ ...state, reservas_planes: {...reservas_planes}})),
    on(unSetReservasPlanes, state => ({ ...state, reservas_hoteles: []})),

);

export function reservasPlanesReducer(state, action) {
    return _reservasPlanesReducer(state, action);
}