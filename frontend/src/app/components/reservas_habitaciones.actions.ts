import { createAction, props } from '@ngrx/store';
import { ReservaHabitacionInterface } from '../models/reserva.model';
export type estadosValidosReservas = 'ACTIVO' | 'CANCELADO' | 'FINALIZADO'|'NINGUNO';
export type filtroReservaValidos = 'PLANES' | 'HABITACIONES'|'NINGUNO';

export const setReservasHabitaciones = createAction('[ReservaHabitacion Component] set Reservas Habitaciones',props<{reservas_habitaciones:ReservaHabitacionInterface[]}>());
export const unSetReservasHabitaciones = createAction('[ReservaHabitacion Component] unSet Reservas Habitaciones');
