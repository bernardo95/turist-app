import { createReducer, on } from '@ngrx/store';
import { setReservasHabitaciones,unSetReservasHabitaciones } from './reservas_habitaciones.actions';
import { ReservaHabitacionInterface } from '../models/reserva.model';

export interface State {
    reservas_habitaciones: ReservaHabitacionInterface[]; 
}

export const initialState: State = {
   reservas_habitaciones: [],
}

const _reservaHabitacionesReducer = createReducer(initialState,

    on(setReservasHabitaciones, (state, { reservas_habitaciones }) => ({ ...state, reservas_habitaciones: {...reservas_habitaciones}})),
    on(unSetReservasHabitaciones, state => ({ ...state, reservas_habitaciones: []})),

);

export function reservaHabitacionesReducer(state, action) {
    return _reservaHabitacionesReducer(state, action);
}