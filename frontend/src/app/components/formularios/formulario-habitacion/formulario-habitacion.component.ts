import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { HabitacionService } from "../../../services/habitacion.service";
import { HotelInterface } from "../../../models/hotel.model";
import { Store } from "@ngrx/store";
import { AppState } from "../../../app.reducer";
import { Subscription } from "rxjs";
import { isLoading, stopLoading } from "../../../shared/ui.actions";
import Swal from "sweetalert2";
import { HabitacionInterface } from '../../../models/habitacion.model';
import { setHabitaciones } from '../../habitaciones.actions';

@Component({
  selector: "app-formulario-habitacion",
  templateUrl: "./formulario-habitacion.component.html",
  styleUrls: ["./formulario-habitacion.component.css"],
})
export class FormularioHabitacionComponent implements OnInit, OnDestroy {
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  hoteles: Array<HotelInterface> = [];
  habitaciones: HabitacionInterface[] = [];
  hotelesSubscription:Subscription;
  habitacionesSubscription:Subscription;
  forma: FormGroup;
  cargando: boolean = false;
  cargandoSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private habitacionService: HabitacionService,
    private store: Store<AppState>
  ) {
    this.subscribeHabitaciones();
    this.subscribeHoteles();
    this.crearFormulario();
  }
  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.hotelesSubscription?.unsubscribe();
    this.habitacionesSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());
  }
  subscribeHoteles(){
    this.store.select('hoteles').subscribe(({hoteles})=>{
      this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
    });
  }
  subscribeHabitaciones(){
    this.store.select('habitaciones').subscribe(({habitaciones})=>{
      this.habitaciones = [];
        Object.keys(habitaciones).forEach((key) => {
          let habitacionesTemp: HabitacionInterface = habitaciones[key];
          this.habitaciones.push(habitacionesTemp);
        });
    });
  }
  get id_hotelNoValido() {
    return (
      this.forma.controls.id_hotel.invalid &&
      this.forma.controls.id_hotel.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get tipoNoValido() {
    return this.forma.controls.tipo.invalid && this.forma.controls.tipo.touched;
  }
  get precioNoValido() {
    return (
      this.forma.controls.precio.invalid && this.forma.controls.precio.touched
    );
  }
  get personasNoValido() {
    return (
      this.forma.controls.personas.invalid &&
      this.forma.controls.personas.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  crearFormulario() {
    this.forma = this.fb.group({
      id_hotel: ["", Validators.required],
      imagenes: ["", Validators.required],
      tipo: ["", Validators.required],
      precio: ["", Validators.required],
      personas: ["", Validators.required],
      descripcion: ["", Validators.required],
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }

  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.habitacionService
        ._crearHabitacion(this.forma.value)
        .subscribe((resp) => {
          if (resp["ok"] === false) {
            this.store.dispatch(stopLoading());

            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["habitacion"]._id;
            this.habitacionService
              ._cargarImagenesHabitacion(formData, id)
              .subscribe((resp) => {
                const habitacion:HabitacionInterface = resp['habitacion'];
                this.habitaciones.push(habitacion);
                this.store.dispatch(setHabitaciones({habitaciones:this.habitaciones}))
                this.store.dispatch(stopLoading());
                Swal.fire({
                  icon: "success",
                  title: "Habitacion creada",
                  showConfirmButton: false,
                  timer: 1500,
                });
                this.forma.reset();
                this.uploadFiles = [];
                this.previewUrls = [];
              });
          }
        });
    }
  }
}
