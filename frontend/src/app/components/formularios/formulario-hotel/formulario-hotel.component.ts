import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { HotelService } from "../../../services/hotel.service";
import { Store } from "@ngrx/store";
import { AppState } from "../../../app.reducer";
import { Subscription } from "rxjs";
import { isLoading, stopLoading } from "../../../shared/ui.actions";
import Swal from "sweetalert2";
import { HotelInterface } from '../../../models/hotel.model';
import { setHoteles } from '../../hoteles.actions';

@Component({
  selector: "app-formulario-hotel",
  templateUrl: "./formulario-hotel.component.html",
  styleUrls: ["./formulario-hotel.component.css"],
})
export class FormularioHotelComponent implements OnInit, OnDestroy {
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  forma: FormGroup;
  hoteles:HotelInterface[] = [];
  hotelesSubscription:Subscription;
  cargando: boolean = false;
  cargandoSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private hotelService: HotelService,
    private store: Store<AppState>
  ) {
    this.crearFormulario();
    
  }
  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.hotelesSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());

  }
  subscripcionHoteles(){
    this.hotelesSubscription = this.store.select('hoteles').subscribe(({hoteles})=>{
      this.hoteles = [];
        Object.keys(hoteles).forEach((key) => {
          let hotelesTemp: HotelInterface = hoteles[key];
          this.hoteles.push(hotelesTemp);
        });
    });
  }
  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get checkInValido(){
    return (this.forma.controls.checkIn.invalid && this.forma.controls.checkIn.touched);
  }
  get checkOutValido(){
    return (this.forma.controls.checkOut.invalid && this.forma.controls.checkOut.touched);
  }
  get calificacionValido(){
    return (this.forma.controls.calificacion.invalid && this.forma.controls.calificacion.touched);
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get categoriaNoValido() {
    return (
      this.forma.controls.categoria.invalid &&
      this.forma.controls.categoria.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      nombre: ["", Validators.required],
      imagenes: ["", Validators.required],
      ubicacion: ["", Validators.required],
      coordenadas: this.fb.group({
        latitud: ["", Validators.required],
        longitud: ["", Validators.required],
      }),
      calificacion:["",Validators.required],
      checkIn: this.fb.group({
        desde: ['',Validators.required],
        hasta: ['',Validators.required],
      }),
      checkOut: this.fb.group({
        desde: ['',Validators.required],
        hasta: ['',Validators.required],
      }),
      categoria: ["", Validators.required],
      descripcion: ["", Validators.required],
    });
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }
  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.hotelService._crearHotel(this.forma.value).subscribe((resp) => {
        if (resp["ok"] == false) {
          this.store.dispatch(isLoading());

          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: resp["error"],
          });
          return;
        } else {
          const id = resp["hotel"]._id;
          this.hotelService
            ._cargarImagenesHotel(formData, id)
            .subscribe((resp) => {
              console.log(resp);
              
              const hotel:HotelInterface = resp['hotel'];
              this.hoteles.push(hotel);
              this.store.dispatch(setHoteles({hoteles:this.hoteles}));
              this.store.dispatch(stopLoading());
              Swal.fire({
                icon: "success",
                title: "Hotel creado",
                showConfirmButton: false,
                timer: 1500,
              });
              this.forma.reset();
              this.uploadFiles = [];
              this.previewUrls = [];
            });
        }
      });
    }
  }
}
