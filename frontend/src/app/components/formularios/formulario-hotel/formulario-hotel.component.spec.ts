import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioHotelComponent } from './formulario-hotel.component';

describe('FormularioHotelComponent', () => {
  let component: FormularioHotelComponent;
  let fixture: ComponentFixture<FormularioHotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioHotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioHotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
