import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { PlanService } from "../../../services/plan.service";
import Swal from "sweetalert2";
import { Store } from "@ngrx/store";
import { AppState } from "../../../app.reducer";
import { Subscription } from "rxjs";
import { isLoading, stopLoading } from '../../../shared/ui.actions';
import { PlanesModel } from '../../../models/plan.model';
import { setPlanes } from '../../planes.actions';

@Component({
  selector: "app-formulario-plan",
  templateUrl: "./formulario-plan.component.html",
  styleUrls: ["./formulario-plan.component.css"],
})
export class FormularioPlanComponent implements OnInit, OnDestroy {
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  forma: FormGroup;
  planes:PlanesModel[]= [];
  planesSubscription:Subscription;
  cargando: boolean = false;
  cargandoSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private planService: PlanService,
    private store: Store<AppState>
  ) { 

    this.crearFormulario();
    this.agregarServicio();
    this.agregarServicioExtra();
    this.agregarRecomendacion();
    this.agregarItinerario();
    this.agregarObservacion();
  }
  ngOnInit(): void {
    this.cargandoSubscription = this.store
      .select("ui")
      .subscribe(({ isLoading }) => {
        this.cargando = isLoading;
      });
    this.store.dispatch(stopLoading());

  }

  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.planesSubscription?.unsubscribe();
  }

  subscribePlanes(){
    this.store.select('planes').subscribe(({planes})=>{
      this.planes = [];
        Object.keys(planes).forEach((key) => {
          let planesTemp: PlanesModel = planes[key];
          this.planes.push(planesTemp);
        });
    });
  }

  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.forma.controls.descripcion.invalid &&
      this.forma.controls.descripcion.touched
    );
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  get duracionNoValido() {
    return (
      this.forma.controls.duracion.invalid &&
      this.forma.controls.duracion.touched
    );
  }
  get personasNoValido() {
    return (
      this.forma.controls.personas.invalid &&
      this.forma.controls.personas.touched
    );
  }
  get precioNoValido() {
    return (
      this.forma.controls.precio.invalid && this.forma.controls.precio.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get serviciosNoValido() {
    return (
      this.forma.controls.servicios.invalid &&
      this.forma.controls.servicios.touched
    );
  }
  get serviciosExtraNoValido() {
    return (
      this.forma.controls.servicios_extra.invalid &&
      this.forma.controls.servicios_extra.touched
    );
  }
  get recomendacionesNoValido() {
    return (
      this.forma.controls.recomendaciones.invalid &&
      this.forma.controls.recomendaciones.touched
    );
  }
  get itinerarioNoValido() {
    return (
      this.forma.controls.itinerario.invalid &&
      this.forma.controls.itinerario.touched
    );
  }
  get observacionesNoValido() {
    return (
      this.forma.controls.observaciones.invalid &&
      this.forma.controls.observaciones.touched
    );
  }
  get horaInicioNoValido() {
    return (
      this.forma.controls.hora_inicio.invalid &&
      this.forma.controls.hora_inicio.touched
    );
  }
  get horaFinNoValido() {
    return (
      this.forma.controls.hora_fin.invalid &&
      this.forma.controls.hora_fin.touched
    );
  }
  get puntoEncuentroNoValido() {
    return (
      this.forma.controls.punto_encuentro.invalid &&
      this.forma.controls.punto_encuentro.touched
    );
  }
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get sitioNoValido() {
    return (
      this.forma.controls.sitio.invalid && this.forma.controls.sitio.touched
    );
  }
  get categoriaNoValido() {
    return (
      this.forma.controls.categoria.invalid &&
      this.forma.controls.categoria.touched
    );
  }
  get servicios() {
    return this.forma.get("servicios") as FormArray;
  }
  get serviciosExtra() {
    return this.forma.get("servicios_extra") as FormArray;
  }
  get recomendaciones() {
    return this.forma.get("recomendaciones") as FormArray;
  }
  get itinerario() {
    return this.forma.get("itinerario") as FormArray;
  }
  get observaciones() {
    return this.forma.get("observaciones") as FormArray;
  }

  crearFormulario() {
    this.forma = this.fb.group({
      sitio: ["", Validators.required],
      nombre: ["", Validators.required],
      descripcion: ["", Validators.required],
      ubicacion: ["", Validators.required],
      duracion: ["", Validators.required],
      personas: ["", Validators.required],
      precio: ["", Validators.required],
      servicios: this.fb.array([]),
      servicios_extra: this.fb.array([]),
      hora_inicio: ["", Validators.required],
      hora_fin: ["", Validators.required],
      punto_encuentro: ["", Validators.required],
      coordenadas: this.fb.group({
        latitud: ["", Validators.required],
        longitud: ["", Validators.required],
      }),
      recomendaciones: this.fb.array([]),
      observaciones: this.fb.array([]),
      itinerario: this.fb.array([]),
      categoria: ["", Validators.required],
      imagenes: ["", Validators.required],
    });
  }
  agregarServicio() {
    this.servicios.push(this.fb.control("", Validators.required));
  }
  borrarServicio(i: number) {
    this.servicios.removeAt(i);
  }
  agregarServicioExtra() {
    this.serviciosExtra.push(this.fb.control("", Validators.required));
  }
  borrarServicioExtra(i: number) {
    this.serviciosExtra.removeAt(i);
  }
  agregarRecomendacion() {
    this.recomendaciones.push(this.fb.control("", Validators.required));
  }
  borrarRecomendacion(i: number) {
    this.recomendaciones.removeAt(i);
  }
  agregarItinerario() {
    this.itinerario.push(this.fb.control(""));
  }
  borrarItinerario(i: number) {
    this.itinerario.removeAt(i);
  }
  agregarObservacion() {
    this.observaciones.push(this.fb.control("", Validators.required));
  }
  borrarObservacion(i: number) {
    this.observaciones.removeAt(i);
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }

  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.planService
        ._crearPlanTuristico(this.forma.value)
        .subscribe((resp) => {
          if (resp["ok"] == false) {
            this.store.dispatch(stopLoading());

            Swal.fire({
              icon: "error",
              title: "Oops...",
              text: resp["error"],
            });
            return;
          } else {
            const id = resp["plan_turistico"]._id;
            this.planService
              ._cargarImagenesPlanTuristico(formData, id)
              .subscribe((resp) => {
                const plan:PlanesModel = resp['sitioDB'];
                this.planes.push(plan);
                this.store.dispatch(setPlanes({planes:this.planes}));
                this.store.dispatch(stopLoading());

                Swal.fire({
                  icon: "success",
                  title: "Plan creado",
                  showConfirmButton: false,
                  timer: 1500,
                });
                this.forma.reset();
                this.uploadFiles = [];
                this.previewUrls = [];
              });
          }
        });
    }
  }
}
