import { Component, OnInit, OnDestroy } from '@angular/core';
import { InterfaceRestaurante } from 'src/app/models/restaurante.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { RestauranteService } from '../../../services/restaurante.service';
import { isLoading, stopLoading } from 'src/app/shared/ui.actions';
import Swal from 'sweetalert2';
import { setRestaurantes } from '../../restaurantes.actions';

@Component({
  selector: 'app-formulario-restaurante',
  templateUrl: './formulario-restaurante.component.html',
  styleUrls: ['./formulario-restaurante.component.css']
})
export class FormularioRestauranteComponent implements OnInit,OnDestroy {
  uploadFiles: Array<File> = [];
  previewUrls: Array<any> = [];
  restaurantes: InterfaceRestaurante[] = [];
  forma: FormGroup;
  cargando: boolean = false;
  restaurantesSubscription:Subscription;
  cargandoSubscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private restauranteService:RestauranteService

  ) {
    this.subscribeRestaurantes();
    this.crearFormulario();
   }
  ngOnDestroy(): void {
    this.cargandoSubscription?.unsubscribe();
    this.restaurantesSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.store.select('ui').subscribe(({isLoading})=>{
      this.cargando = isLoading;
    });
    this.store.dispatch(stopLoading());
  }

  subscribeRestaurantes(){
    this.restaurantesSubscription = this.store.select('restaurantes').subscribe(({restaurantes})=>{
      this.restaurantes = [];
      Object.keys(restaurantes).forEach((key) => {
        let restaurantesTemp: InterfaceRestaurante = restaurantes[key];
        this.restaurantes.push(restaurantesTemp);
      });
    });
  }
  get nombreNoValido() {
    return (
      this.forma.controls.nombre.invalid && this.forma.controls.nombre.touched
    );
  }
  get calificacionValido(){
    return (this.forma.controls.calificacion.invalid && this.forma.controls.calificacion.touched);
  }
  get horarioValido(){
    return (this.forma.controls.horario.invalid && this.forma.controls.horario.touched);
  }
  get informacionValido(){
    return (this.forma.controls.informacion.invalid && this.forma.controls.informacion.touched);
  }
  get ubicacionNoValido() {
    return (
      this.forma.controls.ubicacion.invalid &&
      this.forma.controls.ubicacion.touched
    );
  }
  
  get coordenadasNoValido() {
    return (
      this.forma.controls.coordenadas.invalid &&
      this.forma.controls.coordenadas.touched
    );
  }
  get imagenesNoValido() {
    return (
      this.forma.controls.imagenes.invalid &&
      this.forma.controls.imagenes.touched
    );
  }
  get imagenesGet() {
    return this.forma.get("imagenes") as FormArray;
  }
  crearFormulario() {
    this.forma = this.fb.group({
      nombre: ["", Validators.required],
      imagenes: ["", Validators.required],
      informacion: [""],
      horario: [""],
      ubicacion: ["", Validators.required],
      coordenadas: this.fb.group({
        latitud: ["", Validators.required],
        longitud: ["", Validators.required],
      }),
      calificacion:["",Validators.required],
    });
  }

  agregarImagen() {
    this.imagenesGet.push(this.fb.control([]));
  }
  borrarImagen(i: number) {
    this.imagenesGet.removeAt(i);
  }
  onFileChange(event) {
    console.log(event.target.files);
    this.uploadFiles = event.target.files;
    this.previewImage();
  }
  async previewImage() {
    for await (let image of this.uploadFiles) {
      let reader = new FileReader();
      reader.onload = async (event) => {
        this.previewUrls.push(reader.result);
      };
      reader.readAsDataURL(image);
    }
  }
  guardar() {
    if (this.forma.invalid) {
      Object.values(this.forma.controls).forEach((control) => {
        control.markAsTouched();
        return;
      });
    } else {
      this.store.dispatch(isLoading());
      const formData = new FormData();
      for (let i = 0; i < this.uploadFiles.length; i++) {
        formData.append(
          "photos",
          this.uploadFiles[i],
          this.uploadFiles[i].name
        );
      }
      this.restauranteService._crearRestaurante(this.forma.value).subscribe((resp) => {
        if (resp["ok"] == false) {
          this.store.dispatch(isLoading());
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: resp["error"],
          });
          return;
        } else {
          const id = resp["restaurante"]._id;
          this.restauranteService
            ._cargarImagenesRestaurante(formData, id)
            .subscribe((resp) => {
              console.log(resp);
              
              const restaurante:InterfaceRestaurante = resp['restaurante'];
              this.restaurantes.push(restaurante);
              this.store.dispatch(setRestaurantes({restaurantes:this.restaurantes}));
              this.store.dispatch(stopLoading());
              Swal.fire({
                icon: "success",
                title: "Restaurante creado",
                showConfirmButton: false,
                timer: 1500,
              });
              this.forma.reset();
              this.uploadFiles = [];
              this.previewUrls = [];
            });
        }
      });
    }
  }
}
