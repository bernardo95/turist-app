import { createReducer, on } from "@ngrx/store";
import { setRestaurantes,unSetRestaurantes } from "./restaurantes.actions";
import { InterfaceRestaurante } from "../models/restaurante.model";

export interface State {
  restaurantes: InterfaceRestaurante[],
}

export const initialState: State = {
  restaurantes: [],
};

const _restaurantesReducer = createReducer(
  initialState,

  on(setRestaurantes, (state, { restaurantes }) => ({ ...state, restaurantes: {...restaurantes}})),
  on(unSetRestaurantes, state => ({ ...state, restaurantes: []})),

);

export function restaurantesReducer(state, action) {
  return _restaurantesReducer(state, action);
}
