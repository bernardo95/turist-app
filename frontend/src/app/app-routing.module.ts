import { RouterModule, Routes } from '@angular/router';
import { DashBoardRoutes } from './dashboard/dashboard.routes';
import { FormularioPlanComponent } from './components/formularios/formularioPlan/formulario-plan.component';
import { FormularioHotelComponent } from './components/formularios/formulario-hotel/formulario-hotel.component';
import { FormularioHabitacionComponent } from './components/formularios/formulario-habitacion/formulario-habitacion.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { AuthGuard } from './services/auth.guard';
import { FormularioRestauranteComponent } from './components/formularios/formulario-restaurante/formulario-restaurante.component';
import { NodemailerComponent } from './components/nodemailer/nodemailer.component';

const routes: Routes = [
    {path: '', component:DashboardComponent,children:DashBoardRoutes,canActivate:[AuthGuard]},
    {path: 'login',component:LoginComponent},
    {path:'nodemailer',component:NodemailerComponent},
    {path: 'formularioPlan', component: FormularioPlanComponent,canActivate:[AuthGuard]},
    {path: 'formularioHotel', component: FormularioHotelComponent,canActivate:[AuthGuard]},
    {path: 'formularioHabitacion', component: FormularioHabitacionComponent,canActivate:[AuthGuard]},
    {path: 'formularioRestaurante', component: FormularioRestauranteComponent,canActivate:[AuthGuard]},
    { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
    imports:[
        RouterModule.forRoot(routes,{useHash:true})
    ],
    exports:[
        RouterModule
    ]
})
export class AppRoutingModule {}
