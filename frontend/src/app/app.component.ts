import { Component, OnDestroy } from "@angular/core";
import { AuthService } from "./services/auth.service";
import { AppState } from "./app.reducer";
import { Store } from "@ngrx/store";
import { HotelService } from "./services/hotel.service";
import { HabitacionService } from "./services/habitacion.service";
import { PlanService } from "./services/plan.service";
import { PlanesModel } from "./models/plan.model";
import { setPlanes } from "./components/planes.actions";
import { HotelInterface } from "./models/hotel.model";
import { setHoteles } from "./components/hoteles.actions";
import { HabitacionInterface } from "./models/habitacion.model";
import { setHabitaciones } from "./components/habitaciones.actions";
import { Subscription } from "rxjs";
import { RestauranteService } from "./services/restaurante.service";
import { InterfaceRestaurante } from "./models/restaurante.model";
import { setRestaurantes } from "./components/restaurantes.actions";
import { ReservasService } from "./services/reservas.service";
import { ReservaHabitacionInterface, ReservaPlanInterface } from './models/reserva.model';

import { setReservasPlanes } from "./components/reservas_planes.actions";
import { setReservasHabitaciones } from "./components/reservas_habitaciones.actions";
import { UsuarioInterface } from './models/usuario.model';
import { UsuariosService } from './services/usuarios.service';
import { setUsuarios } from './components/reportes/usuarios.actions';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnDestroy {
  userSubscriptcion: Subscription;

  constructor(
    private authService: AuthService,
    private planService: PlanService,
    private store: Store<AppState>,
    private hotelService: HotelService,
    private habitacionService: HabitacionService,
    private restauranteService: RestauranteService,
    private reservasService: ReservasService,
    private usuariosService:UsuariosService
  ) {
    this.authService.initAuthListener();
    this.userSubscription();
  }
  ngOnDestroy(): void {
    this.userSubscriptcion?.unsubscribe();
  }

  userSubscription() {
    this.userSubscriptcion = this.store.select("user").subscribe(({ user }) => {
      if (user != null) {
        this.initData();
      }
    });
  }
  initData() {
    this.obtenerPlanes();
    this.obtenerHoteles();
    this.obtenerHabitaciones();
    this.obtenerRestaurantes();
    this.obtenerReservasPlanes();
    this.obtenerReservasHabitaciones();
    this.obtenerUsuarios();
  }
  obtenerPlanes() {
    this.planService.obtenerPlanes().subscribe((resp) => {
      const planes: PlanesModel[] = resp["planDB"];
      this.store.dispatch(setPlanes({ planes: planes }));
    });
  }
  obtenerHoteles() {
    this.hotelService.obtenerHoteles().subscribe((resp) => {
      const hoteles: HotelInterface[] = resp["hotelDB"];
      this.store.dispatch(setHoteles({ hoteles: hoteles }));
    });
  }
  obtenerHabitaciones() {
    this.habitacionService.obtenerHabitaciones().subscribe((resp) => {
      const habitaciones: HabitacionInterface[] = resp["habitacionDB"];
      this.store.dispatch(setHabitaciones({ habitaciones: habitaciones }));
    });
  }
  obtenerRestaurantes() {
    this.restauranteService.obtenerRestaurantes().subscribe((resp) => {
      const restaurantes: InterfaceRestaurante[] = resp["restauranteDB"];
      this.store.dispatch(setRestaurantes({ restaurantes: restaurantes }));
    });
  }
  obtenerReservasPlanes() {
    this.reservasService.getReservasPlanes().subscribe((resp) => {
      const reservasPlanes: ReservaPlanInterface[] = resp["reservas"];
      this.store.dispatch(
        setReservasPlanes({ reservas_planes: reservasPlanes })
      );
    });
  }
  obtenerReservasHabitaciones() {
    this.reservasService.getReservasHabitaciones().subscribe((resp) => {
      const reservaHabitaciones: ReservaHabitacionInterface[] =
        resp["reservas"];
      this.store.dispatch(
        setReservasHabitaciones({ reservas_habitaciones: reservaHabitaciones })
      );
    });
  }
  obtenerUsuarios(){
    this.usuariosService.getUsuarios().subscribe((resp)=>{
      const usuarios:UsuarioInterface[] = resp['usuarios'];
      this.store.dispatch(setUsuarios({usuarios:usuarios}));
    })
  }
}
