import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modulos
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

// Ngrx
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

// Routes
import { AppRoutingModule } from './app-routing.module';

// Componentes
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AppComponent } from './app.component';
import { FormularioPlanComponent } from './components/formularios/formularioPlan/formulario-plan.component';
import { FormularioHotelComponent } from './components/formularios/formulario-hotel/formulario-hotel.component';
import { FormularioHabitacionComponent } from './components/formularios/formulario-habitacion/formulario-habitacion.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { environment } from 'src/environments/environment';
import { appReducers } from './app.reducer';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { EditarComponent } from './components/editar/editar.component';
import { EliminarComponent } from './components/eliminar/eliminar.component';
import { HomeComponent } from './components/home/home.component';
import { EditarPlanComponent } from './components/editar/editar-plan/editar-plan.component';
import { EditarHotelComponent } from './components/editar/editar-hotel/editar-hotel.component';
import { EditarHabitacionComponent } from './components/editar/editar-habitacion/editar-habitacion.component';
import { FormularioRestauranteComponent } from './components/formularios/formulario-restaurante/formulario-restaurante.component';
import { EditarRestauranteComponent } from './components/editar/editar-restaurante/editar-restaurante.component';
import { NodemailerComponent } from './components/nodemailer/nodemailer.component';
import { ReservasComponent } from './components/reservas/reservas.component';
import { PlanComponent } from './components/reservas/plan/plan.component';
import { HabitacionComponent } from './components/reservas/habitacion/habitacion.component';
import { ReportesComponent } from './components/reportes/reportes.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FormularioPlanComponent,
    FormularioHotelComponent,
    FormularioHabitacionComponent,
    LoginComponent,
    DashboardComponent,
    SidebarComponent,
    EditarComponent,
    EliminarComponent,
    HomeComponent,
    EditarPlanComponent,
    EditarHotelComponent,
    EditarHabitacionComponent,
    FormularioRestauranteComponent,
    EditarRestauranteComponent,
    NodemailerComponent,
    ReservasComponent,
    PlanComponent,
    HabitacionComponent,
    ReportesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
