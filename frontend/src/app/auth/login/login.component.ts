import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { AppState } from "../../app.reducer";
import * as uiActions from "../../shared/ui.actions";
import { AuthService } from "../../services/auth.service";
import { Usuario } from "../../models/usuario.model";
import { Router } from '@angular/router';
import * as userActions from '../auth.actions';
import Swal from 'sweetalert2'


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  cargando: boolean = false;
  uiSubscription: Subscription;

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private authService: AuthService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this.uiSubscription = this.store.select("ui").subscribe(({ isLoading }) => {
      this.cargando = isLoading;
    });
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
    });
  }

  ngOnDestroy(): void {
    this.uiSubscription.unsubscribe();
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    this.store.dispatch(uiActions.isLoading());
    // console.log(this.loginForm.value);
    const { email, password } = this.loginForm.value;
    this.authService.logearAdmin(email, password).subscribe((resp) => {
      console.log(resp);
      if (resp['ok']==true) {
        const usuario: Usuario = resp["usuario"];
        if (usuario.role === "ADMIN_ROLE") {
          localStorage.setItem('id',usuario._id);
          localStorage.setItem('token',resp['token']);
          this.store.dispatch(userActions.setUser({user:usuario}));
          this.router.navigate(['dashboard']);
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Usuario o contraseña incorrecta'
        });
        }
      }else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Usuario o contraseña incorrecta'
      });
      }
    });
    this.store.dispatch(uiActions.stopLoading());
  }
}
