import { ActionReducerMap } from '@ngrx/store';
import * as auth from './auth/auth.reducer';
import * as ui from './shared/ui.reducer';
import * as planes from './components/planes.reducer';
import * as hoteles from './components/hoteles.reducer';
import * as habitaciones from './components/habitaciones.reducer';
import * as filtro from './components/filtro.reducer';
import * as restaurantes from './components/restaurantes.reducer';
import * as reserva_habitaciones from './components/reservas_habitaciones.reducer';
import * as reserva_planes from './components/reservas_planes.reducer';
import * as filtro_primario from './components/reservas/filtro_primario.reducer';
import * as filtro_segundario from './components/reservas/filtro_segundario.reducer';
import * as usuarios from './components/reportes/usuarios.reducer';


export interface AppState {
    user: auth.State,
    ui: ui.State,
    planes: planes.State,
    hoteles:hoteles.State,
    habitaciones: habitaciones.State,
    filtro: filtro.State
    restaurantes: restaurantes.State,
    reserva_planes: reserva_planes.State,
    reserva_habitaciones: reserva_habitaciones.State,
    filtro_primario: filtro_primario.State,
    filtro_segundario:filtro_segundario.State,
    usuarios: usuarios.State
 }

 export const appReducers: ActionReducerMap<AppState> = {
    user: auth.authReducer,
    ui: ui.uiReducer,
    planes: planes.planesReducer,
    hoteles: hoteles.hotelReducer,
    habitaciones: habitaciones.habitacionesReducer,
    filtro: filtro.filtroReducer,
    restaurantes: restaurantes.restaurantesReducer,
    reserva_planes: reserva_planes.reservasPlanesReducer,
    reserva_habitaciones: reserva_habitaciones.reservaHabitacionesReducer,
    filtro_primario: filtro_primario.filtroPrimarioReducer,
    filtro_segundario:filtro_segundario.filtroSegundarioReducer,
    usuarios: usuarios.usuariosReducer
 }