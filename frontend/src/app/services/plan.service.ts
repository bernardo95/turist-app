import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PlanesModel } from '../models/plan.model';

@Injectable({
  providedIn: 'root'
})
export class PlanService {
  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();

  constructor(private http:HttpClient) { 
    
  }

  _crearPlanTuristico(planTuristico){
    this.headers.set('Content-Type','application/json');
    return this.http.post(`${this.urlHeroku}/plan`,planTuristico,{headers:this.headers});
  }
  _cargarImagenesPlanTuristico(imagenes,id){
  return this.http.post(`${this.urlHeroku}/plan/upload/${id}`,imagenes);
  }

  obtenerPlanes(){
    return this.http.get(`${this.urlHeroku}/planes`);
  }
  // obtenerPlan(id:string){
  //   return this.http.get(`${this.urlHeroku}/planes/${id}`);
  // }
  actualizarPlan(plan:PlanesModel){
    return this.http.put(`${this.urlHeroku}/plan/${plan._id}`,plan);
  }
  editarPlan(id:string,plan:PlanesModel){
    return this.http.put(`${this.urlHeroku}/plan/${id}`,plan);
  }
  actualizarImagenes(imagenes, id){
    return this.http.put(`${this.urlHeroku}/plan/imagenes/${id}`,imagenes);
  }
  eliminarPlan(id:string){
    return this.http.delete(`${this.urlHeroku}/plan/${id}`);
  }
}
