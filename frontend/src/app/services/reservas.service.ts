import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReservasService {
  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();
  
  constructor(private http:HttpClient) { }

  getReservasPlanes(){
    return this.http.get(`${this.urlHeroku}/reservas/plan/admin`);
  }

  getReservasHabitaciones(){
    return this.http.get(`${this.urlHeroku}/reservas/habitacion/admin`);
  }

  updateReservasPlanes(id:string,estado:boolean){
    const body = {
      'estado': estado
    };
    return this.http.put(`${this.urlHeroku}/reserva/${id}`,body,{headers:this.headers});
  }
  updateReservaHabitacion(id:string,estado:boolean){
    const body = {
      'estado': estado
    };
    return this.http.put(`${this.urlHeroku}/reserva/habitacion/${id}`,body,{headers:this.headers});
  }
}
