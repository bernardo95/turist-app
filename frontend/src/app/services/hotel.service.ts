import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HotelInterface } from '../models/hotel.model';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();

  constructor(private http:HttpClient) { 
    
  }
  _crearHotel(hotel){
    this.headers.set('Content-Type','application/json');
    return this.http.post(`${this.urlHeroku}/hotel`,hotel,{headers:this.headers});
  }
  _cargarImagenesHotel(imagenes,id){
    return this.http.post(`${this.urlHeroku}/hotel/upload/${id}`,imagenes);
  }
  obtenerHoteles(){
    return this.http.get(`${this.urlHeroku}/hoteles`);
  }
  obtenerHotel(id:string){
    return this.http.get(`${this.urlHeroku}/hotel/${id}`);
  }
  actualizarHotel(hotel:HotelInterface){
    return this.http.put(`${this.urlHeroku}/hotel/${hotel._id}`,hotel);
  }
  editarHotel(id:string,hotel:HotelInterface){
    return this.http.put(`${this.urlHeroku}/hotel/${id}`,hotel);
  }
  eliminarHotel(id:string){
    return this.http.delete(`${this.urlHeroku}/hotel/${id}`);
  }
}
