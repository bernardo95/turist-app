import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();

  constructor(private http:HttpClient) { }

  getUsuarios(){
    return this.http.get(`${this.urlHeroku}/usuarios`);
  }
}
