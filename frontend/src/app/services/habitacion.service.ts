import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { HabitacionInterface } from '../models/habitacion.model';

@Injectable({
  providedIn: 'root'
})
export class HabitacionService {
  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();

  constructor(private http:HttpClient) { 
    
  }
  _crearHabitacion(hotel){
    this.headers.set('Content-Type','application/json');
    return this.http.post(`${this.urlHeroku}/habitacion`,hotel,{headers:this.headers});
  }
  _cargarImagenesHabitacion(imagenes,id){
    return this.http.post(`${this.urlHeroku}/habitacion/upload/${id}`,imagenes);
  }
  _cargarHoteles(){
    return this.http.get(`${this.urlHeroku}/hoteles`);
  }
  obtenerHabitaciones(){
    return this,this.http.get(`${this.urlHeroku}/habitaciones`);
  }
  actualizarHabitacion(habitacion:HabitacionInterface){
    return this.http.put(`${this.urlHeroku}/habitacion/${habitacion._id}`,habitacion);
  }
  editarHabitacion(id:string,habitacion:HabitacionInterface){
    return this.http.put(`${this.urlHeroku}/habitacion/${id}`,habitacion);
  }
  eliminarHabitacion(id:string){
    return this.http.delete(`${this.urlHeroku}/habitacion/${id}`);
  }
}
