import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InterfaceRestaurante } from '../models/restaurante.model';

@Injectable({
  providedIn: 'root'
})
export class RestauranteService {
  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();

  constructor(private http:HttpClient) { }

  _crearRestaurante(restaurante){
    this.headers.set('Content-Type','application/json');
    return this.http.post(`${this.urlHeroku}/restaurante`,restaurante,{headers:this.headers});
  }
  _cargarImagenesRestaurante(imagenes,id){
    return this.http.post(`${this.urlHeroku}/restaurante/upload/${id}`,imagenes);
  }
  obtenerRestaurantes(){
    return this.http.get(`${this.urlHeroku}/restaurantes`);
  }
  obtenerRestaurante(id:string){
    return this.http.get(`${this.urlHeroku}/restaurante/${id}`);
  }
  actualizarRestaurante(restaurante:InterfaceRestaurante){
    return this.http.put(`${this.urlHeroku}/restaurante/${restaurante._id}`,restaurante);
  }
  editarRestaurante(id:string,restaurante:InterfaceRestaurante){
    return this.http.put(`${this.urlHeroku}/restaurante/${id}`,restaurante);
  }
  eliminarRestaurante(id:string){
    return this.http.delete(`${this.urlHeroku}/restaurante/${id}`);
  }
}
