import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from './auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store:Store<AppState>,private router:Router) {
    
  }
  canActivate():boolean{
    return this.verificaAdmin();
  }

  verificaAdmin(): boolean {
    if (localStorage.getItem('id')!=null) {
      return true
    } else {
      this.router.navigate(['login']);
      return false;

    }
  }
  
}
