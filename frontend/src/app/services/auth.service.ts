import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Usuario } from "../models/usuario.model";
import { Store } from "@ngrx/store";
import { AppState } from "../app.reducer";
import { setUser } from '../auth/auth.actions';
import { Subscription } from 'rxjs';


@Injectable({
  providedIn: "root",
})
export class AuthService {
  private  urlLocal = 'http://10.0.2.2:3000';
  private  urlHeroku = 'https://turist-app.herokuapp.com';
  private headers = new HttpHeaders();
  initAuthListenerSubscription:Subscription;

  constructor(private http: HttpClient, private store: Store<AppState>) {}

  logearAdmin(email:string, password:string ){
    const body = {
      email,
      password,
    };
    return this.http.post(`${this.urlHeroku}/login`, body);
  }
  getAdminData(id:string){
    return this.http.get(`${this.urlHeroku}/usuario/${id}`);
  }
  initAuthListener() {
    this.initAuthListenerSubscription = this.store.select('user').subscribe(({user})=>{
      
      if (user === null && localStorage.getItem('id') != null) {
        this.getAdminData(localStorage.getItem('id')).subscribe((resp)=>{
          const usuario:Usuario = resp['usuarioDB'];
          this.store.dispatch(setUser({user:usuario}));

        });
      }else{
        this.initAuthListenerSubscription?.unsubscribe();
      }
      
    });
  }
}
