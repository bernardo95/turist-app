import { Routes } from '@angular/router';
import { HomeComponent } from '../components/home/home.component';
import { EditarComponent } from '../components/editar/editar.component';
import { EliminarComponent } from '../components/eliminar/eliminar.component';
import { EditarPlanComponent } from '../components/editar/editar-plan/editar-plan.component';
import { EditarHotelComponent } from '../components/editar/editar-hotel/editar-hotel.component';
import { EditarHabitacionComponent } from '../components/editar/editar-habitacion/editar-habitacion.component';
import { EditarRestauranteComponent } from '../components/editar/editar-restaurante/editar-restaurante.component';
import { ReservasComponent } from '../components/reservas/reservas.component';
import { PlanComponent } from '../components/reservas/plan/plan.component';
import { HabitacionComponent } from '../components/reservas/habitacion/habitacion.component';
import { ReportesComponent } from '../components/reportes/reportes.component';

export const DashBoardRoutes: Routes = [
    {path: '' , component:HomeComponent},
    {path: 'editar', component:EditarComponent},
    {path: 'eliminar', component:EliminarComponent},
    {path: 'editarPlan/:id', component:EditarPlanComponent},
    {path: 'editarHotel/:id', component:EditarHotelComponent},
    {path: 'editarHabitacion/:id', component:EditarHabitacionComponent},
    {path: 'editarRestaurante/:id', component:EditarRestauranteComponent},
    {path: 'reservas',component:ReservasComponent},
    {path: 'editarReservaPlan/:id',component:PlanComponent},
    {path: 'editarReservaHabitacion/:id',component:HabitacionComponent},
    {path:'reportes',component:ReportesComponent}
]