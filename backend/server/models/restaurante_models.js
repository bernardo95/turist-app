const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let restauranteSchema = new Schema({
    nombre:{
        type:String,
        required:true,
        trim:true
    },
    imagenes:{
        type:[String],
        required:false,
    },
    informacion:{
        type:String,
        required:false,
        trim:true
    },
    horario:{
        type:String,
        required:false,
        trim:true
    },
    ubicacion:{
        type:String,
        required:true,
        trim:true
    },
    coordenadas:{
        type: Schema.Types.Mixed,
        required:true,
        trim:true
    },
    calificacion:{
        type:Number,
        required:true,
        trim:true
    },
    estado:{
        type:Boolean,
        required:true,
        default:false
    }

},{
    timestamps: true
});

module.exports = mongoose.model('restaurante', restauranteSchema);
