const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let estadosValidos = {
    values: ['CANCELADO', 'ACTIVO', 'FINALIZADO'],
    message: '{VALUE} no es una categoria valida'
};
let reservaSitio = new Schema({
    id_usuario:{
        type: Schema.Types.ObjectId,
        required: true
    },
    id_plan:{
        type: Schema.Types.ObjectId,
        required: true
    },
    estado:{
        type: String,
        required: true,
        enum: estadosValidos,
        default: 'ACTIVO'
    },
    precio:{
        type: Number,
        required:true,
    },
    personas:{
        type: Number,
        required: true,
    },
    fecha:{
        type: String,
        required: true
    }
},{
    timestamps: true
});

module.exports = mongoose.model('reservaSitio', reservaSitio);
