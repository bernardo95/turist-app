const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let habitacionSchema = new Schema(
  {
    id_hotel: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    tipo: {
      type: String,
      required: true,
    },
    imagenes: {
      type: [String],
      required: false,
    },
    precio: {
      type: Number,
      required: true,
    },
    personas: {
      type: Number,
      required: true,
    },
    descripcion: {
      type: String,
      required: true,
    },
    estado: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("habitacion", habitacionSchema);
