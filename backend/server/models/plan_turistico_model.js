/* eslint-disable no-unused-vars */
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
let categoriasValidas = {
    values: ['ROMANTICO', 'AVENTURA', 'NATURALEZA', 'PARQUES_TEMATICOS', 'CULTURA', 'SALUD_BIENESTAR'],
    message: '{VALUE} no es una categoria valida'
};

let Schema = mongoose.Schema;

let planTuristicoScheme = new Schema({
    sitio: {
        type: String,
        required: true,
        trim: true
    },
    nombre: {
        type: String,
        required: [true, 'El nombre es necesario'],
        trim: true,
        unique: true
    },
    imagenes: {
        type: [String],
        required: false
    },
    descripcion: {
        type: String,
        required: true
    },
    ubicacion: {
        type: String,
        trim: true,
        required: true
    },
    duracion: {
        type: String,
        trim: true,
        required: true,
    },
    personas: {
        type: Number,
        trim: true,
        required: true
    },
    precio: {
        type: Number,
        trim: true,
        required: true
    },
    servicios: {
        type: [String],
        required: true
    },
    servicios_extra: {
        type: [String],
        required: false,
        trim: true
    },
    punto_encuentro: {
        type: String,
        trim: true,
        required: true,
    },
    coordenadas:{
        type: Schema.Types.Mixed,
        required: true
    },
    recomendaciones: {
        type: [String],
        required: false
    },
    categoria: {
        type: String,
        required: true
    },
    observaciones: {
        type: [String],
        required: false
    },
    itinerario: {
        type: [String],
        required: false
    },
    hora_inicio: {
        type: String,
        required: true
    },
    hora_fin: {
        type: String,
        required: true
    },
    calificacion:{
        type: Schema.Types.Number,
        required: false
    },  
    estado: {
        type: Boolean,
        default: false
    }

}, {
    timestamps: true
});

planTuristicoScheme.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = mongoose.model('Plan_Turistico', planTuristicoScheme);