const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let comentarioScheme = new Schema({
    id_usuario:{
        type: Schema.Types.ObjectId,
        required: true
    },
    id_ref: {
        type: Schema.Types.ObjectId,
        required: true
    },
    rate : {
        type: Number,
        required: true,
    },
    comentario: {
        type: String,
        required: false,
        trim: true
    }
},{
    timestamps: true
});

module.exports = mongoose.model('comentario', comentarioScheme);
