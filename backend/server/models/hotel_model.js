const mongoose = require("mongoose");

let Schema = mongoose.Schema;

let hotelSchema = new Schema({
  nombre: {
    type: String,
    required: true,
    trim: true
  },
  imagenes: {
    type: [String],
    required: false,
  },
  ubicacion: {
    type: String,
    required: true,
    trim: true
  },
  coordenadas: {
    type: Schema.Types.Mixed,
    required: true,
  },
  checkIn:{
    type: Schema.Types.Mixed,
    required: true,
  },
  checkOut:{
    type: Schema.Types.Mixed,
    required: true,
  },
  calificacion:{
      type: Number,
      required:false
  },
  categoria:{
      type:String,
      required: false
  },
  descripcion:{
      type:String,
      required:true,
      trim: true
  },
  estado:{
      type:Boolean,
      required: true,
      default: false
  }
},{
    timestamps: true
});

module.exports = mongoose.model('hotel', hotelSchema);
