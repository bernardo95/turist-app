/* eslint-disable no-unused-vars */
const express = require("express");
const Restaurante = require('../models/restaurante_models');
const AWS = require("aws-sdk");
const _ = require("underscore");
const lodash = require("lodash");
const app = express();
AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET,
  });

  app.post('/restaurante', [], async (req, res) => {
    let body = req.body;
    let restaurante = new Restaurante({
        nombre: body.nombre,
        informacion: body.informacion,
        horario: body.horario,
        ubicacion: body.ubicacion,
        coordenadas: body.coordenadas,
        calificacion: body.calificacion,
        estado: body.estado,
    });
  
    try {
      let _restaurante = await restaurante.save();
      if (!_restaurante) {
        res.status(400).json({
          ok: false,
          message: "No se ha creado el restaurante en la base de datos.",
        });
      }
      res.status(200).json({
        ok: true,
        restaurante: _restaurante,
      });
    } catch (error) {
      res.status(500).json({
        ok: false,
        message:
          "Error interno del servidor, no se pudó realizar esta operación.",
        error,
      });
    }
  });

  app.post('/restaurante/upload/:id', [], async (req, res) => {
    let id = req.params.id;
  
    if (!req.files) {
      return res.status(400).json({
        ok: false,
        message: "No file source",
      });
    }
    let AWSurl =
      "https://" +
      process.env.AWS_BUCKET_NAME +
      ".s3.amazonaws.com/restaurantes/";
    let images = [];
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
      let photo = req.files.photos[key];
      //creamos un nombre unico para guardarlo en la DB/uploads compartidos
      let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${
        photo.name
      }`;
  
      const paramsUpload = {
        Bucket: process.env.AWS_BUCKET_NAME + "/restaurantes/" + `${id}`,
        Key: nombreArchivoUnico, // File name you want to save as in S3
        Body: photo.data,
        ACL: "public-read",
        ContentType: "image",
      };
  
      images.push(AWSurl + `${id}/${nombreArchivoUnico}`);
  
      s3.upload(paramsUpload, (error, data) => {
        if (error) {
          return res.status(500).json({
            ok: false,
            error: {
              message:
                "ocurrio un error al subir las imagenes para la habitacion asociada",
              error,
            },
          });
        }
      });
    });
    Restaurante.findById(id, (err, restauranteDB) => {
      if (err) {
        // borrarArchivo(nombreArchivoUnico);
        return res.status(500).json({
          ok: false,
          error: {
            message:
              "ocurrio un error al subir las imagenes para el restaurante asociado",
            err,
          },
        });
      }
      if (!restauranteDB) {
        return res.status(400).json({
          ok: false,
          err: {
            message: "restaurante no existe",
          },
        });
      }
      restauranteDB.imagenes = images;
      restauranteDB.save((err, restauranteActualizado) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            message: "error al actualizar las imagenes del restaurante",
            err,
          });
        }
        return res.status(200).json({
          status: true,
          message: "Files are uploaded",
          data: images,
          restaurante: restauranteActualizado,
        });
      });
    });
  });

  app.get('/restaurantes', [], (req, res) => {
    let param = req.body.estado;
     Restaurante.find({},
      "nombre imagenes informacion horario ubicacion coordenadas calificacion estado"
    )
    .sort({nombre:1})
    .exec((err, restauranteDB) => {
      //verifica si hay error en la busquedo de los usuarios a la DB
      if (err) {
        return res.status(500).json({
          ok: false,
          error: {
            message: "No se pudo buscar los restaurantes en la base de datos",
            err,
          },
        });
      }
      Restaurante.countDocuments({ estado: param }, (err, conteo) => {
        res.status(200).json({
          ok: true,
          cuantos: conteo,
          restauranteDB,
        });
      });
    });
  });
  
  app.get('/restaurante/:id', [], (req, res) => {
    let id = req.params.id;
    //metodo del mongose para encontrar por parametro en la BD
    Restaurante.findOne({ _id: id }).exec((err, restauranteDB) => {
      //verifica si hay un error al buscar en la DB
      if (err) {
        return res.status(500).json({
          ok: false,
          error: {
            message: "No se pudo buscar el restaurante en la base de datos",
            err,
          },
        });
      }
      res.status(200).json({
        ok: true,
        restauranteDB,
      });
    });
  });

app.put('/restaurante/:id', [], (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, [
        "nombre",    "imagenes",    "informacion",    "horario",    "ubicacion",    "coordenadas",    "calificacion",    "estado" 
    ]);
  
    //metodo de mongose para encontrar uno y actualizar
    Restaurante.findOneAndUpdate(
      { _id: id },
      body,
      { new: true },
      (err, restauranteDB) => {
        //verifica si hay un error al actualizar en la DB
        if (err) {
          return res.status(500).json({
            ok: false,
            error: {
              message: "No se pudo actualizar el restaurante en la base de datos",
              err,
            },
          });
        }
        res.status(200).json({
          ok: true,
          message: "Restaurante actualizado correctamente",
          restauranteDB,
        });
      }
    );
  });

  app.delete('/restaurante/:id', [], (req, res) => {
    let id = req.params.id;

    //metodo de mongose para buscar y actualizar
    Restaurante.findOneAndRemove({ _id: id }, (err, restauranteDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el restaurante de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!restauranteDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'restaurante no encontrado'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            habitacion: restauranteDB,
            message: 'Habitacion eliminada correctamente'
        });

    });
});
module.exports = app;
