const express = require("express");
const mongoose = require("mongoose");
const _ = require('underscore');
const Reserva = require("../models/reserva_habitacion");

const app = express();

app.post('/reserva/habitacion',[],async(req,res)=>{
    let body = req.body;
    console.log(req.body);
    let reserva = new Reserva({
        id_usuario:body.id_usuario,
        id_habitacion:body.id_habitacion,
        id_hotel:body.id_hotel,
        precio:body.precio,
        personas:body.personas,
        habitaciones:body.habitaciones,
        fechaInicio:body.fechaInicio,
        fechaFin:body.fechaFin
    });
    
    reserva.save((err, reservaDB) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            error: {
              message: "Error en la creacion de la reserva en la DB",
              err,
            },
          });
        }
        res.status(201).json({
          ok: true,
          reserva: reservaDB,
        });
      });
});

app.get('/reservas/habitaciones',[],async(req,res)=>{
  try {
    const _reservas = await Reserva.find({});
    if (!_reservas) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reservas: _reservas,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.get('/reserva/habitacion/:id',[],async(req,res)=>{
    let id = req.params.id;

    try {
        const _reserva = await Reserva.findOne({
          _id: id,
        });
        if (!_reserva) {
          res.status(400).json({
            ok: false,
            message: "No se ha encontrado la reserva en la base de datos.",
          });
        }
        res.status(200).json({
          ok: true,
          reserva: _reserva,
        });
      } catch (error) {
        res.status(500).json({
          ok: false,
          message:
            "Error interno del servidor, no se pudó realizar esta operación.",
          error,
        });
      }
});

app.get('/reservas/habitacion/usuario/:id',[],async(req,res)=>{
  let id = req.params.id;
  try {
      const _reserva = await Reserva.find({
        id_usuario: mongoose.Types.ObjectId(id),
      });
      if (!_reserva) {
        res.status(400).json({
          ok: false,
          message: "No se ha encontrado la reserva en la base de datos.",
        });
      }
      res.status(200).json({
        ok: true,
        reservas: _reserva,
      });
    } catch (error) {
      res.status(500).json({
        ok: false,
        message:
          "Error interno del servidor, no se pudó realizar esta operación.",
        error,
      });
    }
});

app.put('/reserva/habitacion/:id',[],async(req,res)=>{
  let id = req.params.id;

  try {
    let body = _.pick(req.body,['estado']);
    const _reserva = await Reserva.findOneAndUpdate({_id:id},body,{new:true});
    if (!_reserva) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reserva: _reserva,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }

});

app.get('/reservas/habitacion/admin',[],async(req,res)=>{

  try {
    const _reservas = await Reserva.aggregate([
      {
        '$lookup': {
          'from': 'hotels', 
          'localField': 'id_hotel', 
          'foreignField': '_id', 
          'as': 'hotel'
        }
      }, {
        '$lookup': {
          'from': 'habitacions', 
          'localField': 'id_habitacion', 
          'foreignField': '_id', 
          'as': 'habitacion'
        }
      }, {
        '$lookup': {
          'from': 'usuarios', 
          'localField': 'id_usuario', 
          'foreignField': '_id', 
          'as': 'usuario'
        }
      }
    ]);
    if (!_reservas) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reservas: _reservas,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
  
});

module.exports = app;
