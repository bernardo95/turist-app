/* eslint-disable no-unused-vars */
const express = require("express");
const mongoose = require("mongoose");
const Comentario = require("../models/calificacion_model");

const app = express();

//peticion para crear un comentario
app.post("/comentario", [], async (req, res) => {
  let body = req.body;
  // console.log(req.body);

  let comentario = new Comentario({
    id_usuario: body.id_usuario,
    id_ref: body.id_ref,
    rate: body.rate,
    comentario: body.comentario,
  });

  comentario.save((err, comentarioDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        error: {
          message: "Error en la creacion del comentario en la DB",
          err,
        },
      });
    }
    res.status(201).json({
      ok: true,
      comentario: comentarioDB,
    });
  });
});

//obtiene el comentario del usuario en una categoria
app.get("/comentario/:id", [], async (req, res) => {
  let id = mongoose.Types.ObjectId(req.params.id);
  try {
    const _comentario = await Comentario.aggregate([
      {
        $match: {
          id_ref: id,
        },
      },
    ]);
    if (!_comentario) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado el comentario en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      comment: _comentario,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.get("/comentario/:ref/:usuario", [], async (req, res) => {
  let id_ref = req.params.ref;
  let id_usuario = req.params.usuario;
  try {
    const _comentario = await Comentario.findOne({
      id_ref: id_ref,
      id_usuario: id_usuario,
    });
    if (!_comentario) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado el comentario en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      comment: _comentario,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
  
});


app.delete("/comentario/:id", [], async (req, res) => {
    let id = req.params.id;

    Comentario.findOneAndRemove({ _id: id }, (err, comentarioDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el plan turistico de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!comentarioDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Plan turistico no encontrado'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            comment: comentarioDB,
            message: 'Comentario eliminado correctamente'
        });

    });
});

module.exports = app;
