const express = require("express");
const mongoose = require("mongoose");
const _ = require('underscore');
const Reserva = require("../models/reserva_sitio");

const app = express();

app.post('/reserva/plan',[],async(req,res)=>{
    let body = req.body;

    let reserva = new Reserva({
        id_usuario:body.id_usuario, 
        id_plan:body.id_plan, 
        precio:body.precio, 
        personas:body.personas, 
        fecha:body.fecha, 
    });
    
    reserva.save((err, reservaDB) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            error: {
              message: "Error en la creacion de la reserva en la DB",
              err,
            },
          });
        }
        res.status(201).json({
          ok: true,
          reserva: reservaDB,
        });
      });
});

app.get('/reservas/planes',[],async(req,res)=>{
  try {
    const _reservas = await Reserva.find({});
    if (!_reservas) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reservas: _reservas,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.get('/reserva/plan/:id',[],async(req,res)=>{
    let id = req.params.id;

    try {
        const _reserva = await Reserva.findOne({
          _id: id,
        });
        if (!_reserva) {
          res.status(400).json({
            ok: false,
            message: "No se ha encontrado la reserva en la base de datos.",
          });
        }
        res.status(200).json({
          ok: true,
          reserva: _reserva,
        });
      } catch (error) {
        res.status(500).json({
          ok: false,
          message:
            "Error interno del servidor, no se pudó realizar esta operación.",
          error,
        });
      }
});

app.get('/reservas/usuario/:id',[],async(req,res)=>{
  let id = req.params.id;
  try {
      const _reserva = await Reserva.find({
        id_usuario: mongoose.Types.ObjectId(id),
      });
      if (!_reserva) {
        res.status(400).json({
          ok: false,
          message: "No se ha encontrado la reserva en la base de datos.",
        });
      }
      res.status(200).json({
        ok: true,
        reservas: _reserva,
      });
    } catch (error) {
      res.status(500).json({
        ok: false,
        message:
          "Error interno del servidor, no se pudó realizar esta operación.",
        error,
      });
    }
});
app.get('/reservas/plan/admin',[],async(req,res)=>{

  try {
    const _reservas = await Reserva.aggregate([
      {
        '$lookup': {
          'from': 'plan_turisticos', 
          'localField': 'id_plan', 
          'foreignField': '_id', 
          'as': 'plan'
        }
      },
      {
        '$lookup': {
          'from': 'usuarios', 
          'localField': 'id_usuario', 
          'foreignField': '_id', 
          'as': 'usuario'
        }
      }
    ]);
    if (!_reservas) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reservas: _reservas,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
  
});
app.put('/reserva/:id',[],async(req,res)=>{
  let id = req.params.id;

  try {
    let body = _.pick(req.body,['estado']);
    const _reserva = await Reserva.findOneAndUpdate({_id:id},body,{new:true});
    if (!_reserva) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado la reserva en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      reserva: _reserva,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }

});

module.exports = app;
