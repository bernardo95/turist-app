/* eslint-disable no-unused-vars */
const express = require('express');
// const mongoose = require('mongoose');
const PlanTuristico = require('../models/plan_turistico_model');
const { verificaToken, verificaAdmin_Role } = require('../middleware/autentication');
const AWS = require("aws-sdk");

// const fileUpload = require('express-fileupload');
const _ = require('underscore');
const lodash = require('lodash');
const fileUpload = require("express-fileupload");

const app = express();
AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET,
});

app.use(
    fileUpload({
        createParentPath: true,
    })
);
//Peticion post para crear un plan turistico
app.post('/plan', [], async(req, res) => {
    let body = req.body;
    let plan = new PlanTuristico({
        sitio: body.sitio,
        nombre: body.nombre,
        imagenes: body.imagenes,
        descripcion: body.descripcion,
        ubicacion: body.ubicacion,
        duracion: body.duracion,
        personas: body.personas,
        precio: body.precio,
        servicios: body.servicios,
        servicios_extra: body.servicios_extra,
        punto_encuentro: body.punto_encuentro,
        coordenadas: body.coordenadas,
        recomendaciones: body.recomendaciones,
        categoria: body.categoria,
        observaciones: body.observaciones,
        hora_inicio: body.hora_inicio,
        hora_fin: body.hora_fin,
        itinerario: body.itinerario
    });

    plan.save((err, planDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error en la creacion del plan turistico en la DB',
                    err
                }
            });
        }
        res.status(201).json({
            ok: true,
            plan_turistico: planDB
        });
    });
});

//cargar imagenes a un plan turistico
app.post('/plan/upload/:id', async(req, res) => {
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }
    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/planes/';

    let images = [];
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];
        //creamos un nombre unico para guardarlo en la DB/uploads compartidos
        let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;

        const paramsUpload = {
            Bucket: process.env.AWS_BUCKET_NAME + '/planes/' + `${id}`,
            Key: nombreArchivoUnico, // File name you want to save as in S3
            Body: photo.data,
            ACL: "public-read",
            ContentType: "image",
        };

        images.push(AWSurl + `${id}/${nombreArchivoUnico}`);

        
        s3.upload(paramsUpload, (error, data) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: "ocurrio un error al subir las imagenes para el plan turistico asociado",
                        error,
                    },
                });
            }
        });


    });

    PlanTuristico.findById(id, (err, planDB) => {
        if (err) {
            // borrarArchivo(nombreArchivoUnico);   
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir las imagenes para el plan asociado',
                    err
                }
            });
        }
        if (!planDB) {
            // borrarArchivo(nombreArchivoUnico);
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'plan no existe'
                }
            });
        }
        planDB.imagenes = images;
        planDB.save((err, planActualizado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'error al actualizar las imagenes del plan',
                    err
                });
            }
            return res.status(200).json({
                ok: true,
                message: 'Files are uploaded',
                data: images,
                sitioDB: planActualizado
            });
        });
    });
});

    //Peticion get para listar los planes turisticos
app.get('/planes', [], (req, res) => {
    let param = req.body.estado;
    param = true;
    PlanTuristico.find({}, '_id  sitio    nombre    imagenes    descripcion    ubicacion    duracion    personas    precio    servicios    servicios_extra    punto_encuentro    coordenadas    recomendaciones    categoria    observaciones    itinerario    hora_inicio    hora_fin    calificacion    estado    createdAt    updatedAt')
        .sort({nombre: 1})
        .exec((err, planDB) => {
            //verifica si hay error en la busquedo de los usuarios a la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar los planes turisticos en la base de datos',
                        err
                    }
                });
            }
            //devuelve el listado de los usuarios en la DB
            PlanTuristico.countDocuments({ estado: param }, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    cuantos: conteo,
                    planDB,
                });
            });
        });
});


//Peticion get para mostar un sitio turistico
app.get('/planes/:id', [], (req, res) => {
    let id = req.params.id;
    //metodo del mongose para encontrar por parametro en la BD
    PlanTuristico.findOne({ _id: id })
        .exec((err, planDB) => {
            //verifica si hay un error al buscar en la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar el plan en la base de datos',
                        err
                    }
                });
            }
            //devuelve el usuario encontrado
            res.status(200).json({
                ok: true,
                planDB
            });
        });
});

//Peticion put para actualizar plan
app.put('/plan/:id', [], (req, res) => {

    let id = req.params.id;

    let body = _.pick(req.body,['sitio',  'nombre',  'imagenes',  'descripcion', 'calificacion', 'ubicacion',  'duracion',  'personas',  'precio',  'servicios',  'servicios_extra',  'punto_encuentro',  'coordenadas',  'recomendaciones',  'categoria',  'observaciones',  'itinerario',  'hora_inicio',  'hora_fin', 'estado']);

    //metodo de mongose para encontrar uno y actualizar
    PlanTuristico.findOneAndUpdate({ _id: id }, body, { new: true }, (err, planDB) => {
        //verifica si hay un error al actualizar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo actualizar el plan turistico en la base de datos',
                    err
                }
            });
        }

        //devuelve el usuario actualizado
        res.status(200).json({
            ok: true,
            planDB: planDB,
            message: 'Plan turistico actualizado correctamente'
        });

    });
});

//actualizar imagenes plan
app.put('/plan/imagenes/:id',[],async(req,res)=>{
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }
    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/planes/';
    //empty buckter
    const paramsDelete = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `/planes/${id}/`, // File name you want to delete in S3
    };
        
    let images = [];
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];
        //creamos un nombre unico para guardarlo en la DB/uploads compartidos
        let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;

        const paramsUpload = {
            Bucket: process.env.AWS_BUCKET_NAME + '/planes/' + `${id}`,
            Key: nombreArchivoUnico, // File name you want to save as in S3
            Body: photo.data,
            ACL: "public-read",
            ContentType: "image",
        };


        s3.upload(paramsUpload, (error, data) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: "ocurrio un error al subir las imagenes para el plan turistico asociado",
                        error,
                    },
                });
            }
        });

        images.push(AWSurl + `${id}/${nombreArchivoUnico}`);
    });

    PlanTuristico.findById(id, (err, planDB) => {
        if (err) {
            // borrarArchivo(nombreArchivoUnico);   
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir las imagenes para el plan asociado',
                    err
                }
            });
        }
        if (!planDB) {
            // borrarArchivo(nombreArchivoUnico);
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'plan no existe'
                }
            });
        }
        planDB.imagenes = images;
        planDB.save((err, planActualizado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'error al actualizar las imagenes del plan',
                    err
                });
            }
            return res.status(200).json({
                ok: true,
                message: 'Files are uploaded',
                data: images,
                sitioDB: planActualizado
            });
        });
    });
});

//Peticion delete para deshabilitar un usuario
app.delete('/plan/:id', [], (req, res) => {
    let id = req.params.id;

    //metodo de mongose para buscar y actualizar
    PlanTuristico.findOneAndRemove({ _id: id }, (err, planDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el plan turistico de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!planDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Plan turistico no encontrado'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            planDB: planDB,
            message: 'Plan turistico eliminado correctamente'
        });

    });
});

module.exports = app;