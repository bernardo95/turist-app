/* eslint-disable no-unused-vars */
const express = require("express");
const Habitacion = require("../models/habitacion_model");
const mongoose = require("mongoose");
const {
  verificaToken,
  verificaAdmin_Role,
} = require("../middleware/autentication");
const AWS = require("aws-sdk");
const _ = require("underscore");
const lodash = require("lodash");

const app = express();
AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ID,
  secretAccessKey: process.env.AWS_SECRET,
});

app.post('/habitacion', [], async (req, res) => {
  let body = req.body;
  let habitacion = new Habitacion({
    id_hotel: mongoose.Types.ObjectId(body.id_hotel),
    tipo: body.tipo,
    imagenes: body.imagenes,
    precio: body.precio,
    personas: body.personas,
    descripcion: body.descripcion,
  });

  try {
    let _habitacion = await habitacion.save();
    if (!_habitacion) {
      res.status(400).json({
        ok: false,
        message: "No se ha creado la habitacion en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      habitacion: _habitacion,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.post('/habitacion/upload/:id', [], async (req, res) => {
  let id = req.params.id;

  if (!req.files) {
    return res.status(400).json({
      ok: false,
      message: "No file source",
    });
  }
  let AWSurl =
    "https://" +
    process.env.AWS_BUCKET_NAME +
    ".s3.amazonaws.com/habitaciones/";
  let images = [];
  lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
    let photo = req.files.photos[key];
    //creamos un nombre unico para guardarlo en la DB/uploads compartidos
    let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${
      photo.name
    }`;

    const paramsUpload = {
      Bucket: process.env.AWS_BUCKET_NAME + "/habitaciones/" + `${id}`,
      Key: nombreArchivoUnico, // File name you want to save as in S3
      Body: photo.data,
      ACL: "public-read",
      ContentType: "image",
    };

    images.push(AWSurl + `${id}/${nombreArchivoUnico}`);

    s3.upload(paramsUpload, (error, data) => {
      if (error) {
        return res.status(500).json({
          ok: false,
          error: {
            message:
              "ocurrio un error al subir las imagenes para la habitacion asociada",
            error,
          },
        });
      }
    });
  });
  Habitacion.findById(id, (err, habitacionDB) => {
    if (err) {
      // borrarArchivo(nombreArchivoUnico);
      return res.status(500).json({
        ok: false,
        error: {
          message:
            "ocurrio un error al subir las imagenes para la habitacion asociada",
          err,
        },
      });
    }
    if (!habitacionDB) {
      // borrarArchivo(nombreArchivoUnico);
      return res.status(400).json({
        ok: false,
        err: {
          message: "habitacion no existe",
        },
      });
    }
    habitacionDB.imagenes = images;
    habitacionDB.save((err, habitacionActualizada) => {
      if (err) {
        return res.status(500).json({
          ok: false,
          message: "error al actualizar las imagenes de la habitacion",
          err,
        });
      }
      return res.status(200).json({
        status: true,
        message: "Files are uploaded",
        data: images,
        habitacion: habitacionActualizada,
      });
    });
  });
});

app.get('/habitaciones', [], (req, res) => {
  let param = req.body.estado;
  Habitacion.find({},
    "id_hotel tipo imagenes precio personas descripcion estado"
  ).exec((err, habitacionDB) => {
    //verifica si hay error en la busquedo de los usuarios a la DB
    if (err) {
      return res.status(500).json({
        ok: false,
        error: {
          message: "No se pudo buscar los hoteles en la base de datos",
          err,
        },
      });
    }
    //devuelve el listado de los usuarios en la DB
    Habitacion.countDocuments({ estado: param }, (err, conteo) => {
      res.status(200).json({
        ok: true,
        cuantos: conteo,
        habitacionDB,
      });
    });
  });
});

app.get('/habitaciones/hoteles',[],async(req,res)=>{
  try {
    const _habitaciones = await Habitacion.aggregate([
      {
        '$lookup': {
          'from': 'hotels', 
          'localField': 'id_hotel', 
          'foreignField': '_id', 
          'as': 'hotel'
        }
      }
    ]);
    if (!_habitaciones) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado las habitaciones en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      habitaciones: _habitaciones,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.get('/habitacion/:id', [], (req, res) => {
  let id = req.params.id;
  //metodo del mongose para encontrar por parametro en la BD
  Habitacion.findOne({ _id: id }).exec((err, habitacionDB) => {
    //verifica si hay un error al buscar en la DB
    if (err) {
      return res.status(500).json({
        ok: false,
        error: {
          message: "No se pudo buscar la habitacion en la base de datos",
          err,
        },
      });
    }
    //devuelve el usuario encontrado
    res.status(200).json({
      ok: true,
      habitacionDB,
    });
  });
});

app.get('/habitaciones/hotel/:id',[],async(req,res)=>{
  let id = mongoose.Types.ObjectId(req.params.id);
  try {
    const _habitaciones = await Habitacion.aggregate([
      { $match: {  id_hotel: id } },
      { $sort: {precio: 1 }}
    ]);
    if (!_habitaciones) {
      res.status(400).json({
        ok: false,
        message: "No se ha encontrado habitaciones en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      habitaciones: _habitaciones,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.put('/habitacion/:id', [], (req, res) => {
  let id = req.params.id;

  let body = _.pick(req.body, [
    "id_hotel",
    "tipo",
    "imagenes",
    "precio",
    "personas",
    "descripcion",
    "estado",
  ]);

  //metodo de mongose para encontrar uno y actualizar
  Habitacion.findOneAndUpdate(
    { _id: id },
    body,
    { new: true },
    (err, habitacionDB) => {
      //verifica si hay un error al actualizar en la DB
      if (err) {
        return res.status(500).json({
          ok: false,
          error: {
            message: "No se pudo actualizar la habitacion en la base de datos",
            err,
          },
        });
      }
      res.status(200).json({
        ok: true,
        message: "Habitacion actualizada correctamente",
        habitacionDB,
      });
    }
  );
});

app.delete('/habitacion/:id', [], (req, res) => {
    let id = req.params.id;

    //metodo de mongose para buscar y actualizar
    Habitacion.findOneAndRemove({ _id: id }, (err, habitacionDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar la habitacion de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!habitacionDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'habitacion no encontrada'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            habitacion: habitacionDB,
            message: 'Habitacion eliminada correctamente'
        });

    });
});

module.exports = app;
