/* eslint-disable no-unused-vars */
const express = require('express');
const bcrypt = require('bcryptjs');
const Usuario = require('../models/usuario_model');
const { verificaToken, verificaAdmin_Role } = require('../middleware/autentication');
const _ = require('underscore');
const AWS = require("aws-sdk");
const fs = require('fs');
const path = require('path');
// const fileUpload = require("express-fileupload");

const app = express();

AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET,
});
// app.use(
//     fileUpload({
//         createParentPath: true,
//     })
// );

//Peticion post de creacion de un nuevo usuario
app.post('/usuario', async(req, res) => {
    let body = req.body;
    const salt = bcrypt.genSaltSync(10);
    const password = body.password;
    const hashedPassword = bcrypt.hashSync(password, salt);

    let usuario = new Usuario({
        nombre: body.nombre,
        apellido: body.apellido,
        genero: body.genero,
        fecha_nacimiento: body.fecha_nacimiento,
        email: body.email,
        password: hashedPassword,
        role: body.role
    });

    //llamado al metodo para crear usuario en la DB
    usuario.save((err, usuarioDB) => {

        //Verifica si hay error al crear el usuario en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'Error en la creacion del usuario en la DB',
                    err,
                    body
                }
            });
        }
        //respuesta en caso de salir bien la creacion del usuario
        res.status(201).json({
            ok: true,
            message: 'usuario creado',
            usuario: usuarioDB,
        });
    });
});

app.post('/usuario/upload/image/:id', [], async(req, res) => {
    let id = req.params.id;    
    //verifica si hay archivos en el request
    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }

    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/usuarios/';

    // //loop all files
    let data = [];

    let photo = req.files.photo;
    // //creamos un nombre unico para guardarlo en la DB/uploads compartidos
    let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;
    const paramsUpload = {
        Bucket: process.env.AWS_BUCKET_NAME + '/usuarios/' + `${id}`,
        Key: nombreArchivoUnico, // File name you want to save as in S3
        Body: photo.data,
        ACL: "public-read",
        ContentType: "image",
    };

    s3.upload(paramsUpload, (error, data) => {
        if (error) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: "ocurrio un error al subir las imagenes para el plan turistico asociado",
                    error,
                },
            });
        }
    });

    let imgUrlAws = (AWSurl + `${id}/${nombreArchivoUnico}`);

    //push file details
    data.push({
        name: photo.name,
        mimetype: photo.mimetype,
        size: photo.size
    });

    Usuario.findById(id, (err, usuarioDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir la imagen para el usuario asociado',
                    err
                }
            });
        }
        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'usuario no existe'
                }
            });
        }
        usuarioDB.img_profile = imgUrlAws;
        usuarioDB.save((err, usuarioActualizadoImg) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }
            //return response
            return res.status(200).json({
                status: true,
                message: 'Files are uploaded',
                data: data,
                sitioDB: usuarioActualizadoImg
            });
        });
    });
});


//Peticion get  para listar los usuarios
app.get('/usuarios', [], (req, res) => {
    // let desde = req.query.desde || 0;
    // desde = Number(desde);

    // let limite = req.query.limite || 10;
    // limite = Number(limite);

    //metodo de mongo para listar los usuarios
    Usuario.find({ estado: true })
        .exec((err, usuarios) => {

            //verifica si hay error en la busquedo de los usuarios a la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se ha podido buscar los usuarios en la base de datos',
                        err
                    }
                });
            }

            //devuelve el listado de los usuarios en la DB
            return res.status(200).json({
                ok: true,
                usuarios,
            });

        });
});

//Peticion get para mostar un usuario
app.get('/usuario/:id', [], (req, res) => {

    let id = req.params.id;
    //metodo del mongose para encontrar por parametro en la DB
    Usuario.findOne({ _id: id })
        .exec((err, usuarioDB) => {
            //verifica si hay un error al buscar en la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se ha podido buscar el usuario en la base de datos',
                        err
                    }
                });
            }
            //devuelve el usuario encontrado
            res.status(200).json({
                ok: true,
                usuarioDB
            });
        });
});

//Peticion get para verificar si al registrar el correo ya esta en uso
app.get('/usuario/exist/:email', [], (req, res) => {
    let email = req.params.email;
    Usuario.exists({ email: email }).then((callback) => {
        return res.status(200).json({
            callback
        });
    }).catch((error) => {
        return res.status(500).json({
            ok: false,
            message: 'no se pudo realizar la busqueda',
            error
        });
    });
});
//Peticion put para actualizar un usuario
app.put('/usuario/:id', [], (req, res) => {

let id = req.params.id;
let body = _.pick(req.body, ['nombre','apellido','fecha_nacimiento','genero','email' ]);

    //metodo de mongose para encontrar uno y actualizar
    Usuario.findOneAndUpdate({ _id: id }, body, { new: true }, (err, usuarioDB) => {
        //verifica si hay un error al actualizar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo actualizar el usuario en la base de datos',
                    err
                }
            });
        }

        //devuelve el usuario actualizado
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
            message: 'Usuario actualizado correctamente'
        });

    });
});

app.put('/usuario/image/:id',async(req,res)=>{
    let id = req.params.id;  

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }

    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/usuarios/';
    let data = [];
    let photo = req.files.photo;
    // const paramsUpload = {
    //     Bucket: process.env.AWS_BUCKET_NAME + '/usuarios/' + `${id}`,
    //     Key: , // File name you want to save as in S3
    //     Body: photo.data,
    //     ACL: "public-read",
    //     ContentType: "image",
    // };

    const imageUrl = await s3.deleteObject({
        Bucket: process.env.AWS_BUCKET_NAME + '/usuarios/' + `${id}`,
    }).promise();

    console.log(imageUrl);
    res.json({
        imageUrl
    })
    
    
    // s3.upload(paramsUpload, (error, data) => {
    //     if (error) {
    //         return res.status(500).json({
    //             ok: false,
    //             error: {
    //                 message: "ocurrio un error al subir las imagenes para el plan turistico asociado",
    //                 error,
    //             },
    //         });
    //     }
    // });



});

//Peticion delete para deshabilitar un usuario
app.delete('/usuario/:id', [verificaToken], (req, res) => {
    let id = req.params.id;

    let cambiaEstado = {
        estado: false
    };

    //metodo de mongose para buscar y actualizar
    Usuario.findOneAndUpdate({ _id: id }, cambiaEstado, { new: true }, (err, usuarioDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el usuario en la base de datos',
                    err
                }
            });
        }
        //verifica si el usuario no existe en la base de datos
        if (!usuarioDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'Usuario no encontrado'
                }
            });
        }

        //Devuelve el usuario eliminado
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
            message: 'Usuario eliminado correctamente'
        });

    });
});

module.exports = app;