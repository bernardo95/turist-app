/* eslint-disable no-unused-vars */
const express = require("express");
const Hotel = require("../models/hotel_model");
const {
  verificaToken,
  verificaAdmin_Role,
} = require("../middleware/autentication");
const AWS = require("aws-sdk");
const _ = require("underscore");
const lodash = require("lodash");

const app = express();
AWS.config.update({ region: "us-east-1" });
const s3 = new AWS.S3({
  accessKeyId: process.env.AWS_ID,
  secretAccessKey: process.env.AWS_SECRET,
});

app.post('/hotel', [], async (req, res) => {
  let body = req.body;
  let hotel = new Hotel({
    nombre: body.nombre,
    imagenes: body.imagenes,
    ubicacion: body.ubicacion,
    coordenadas: body.coordenadas,
    categoria: body.categoria,
    calificacion: body.calificacion,
    descripcion: body.descripcion,
    checkIn: body.checkIn,
    checkOut: body.checkOut,
  });

  try {
    let _hotel = await hotel.save();
    if (!_hotel) {
      res.status(400).json({
        ok: false,
        message: "No se ha creado el hotel en la base de datos.",
      });
    }
    res.status(200).json({
      ok: true,
      hotel: _hotel,
    });
  } catch (error) {
    res.status(500).json({
      ok: false,
      message:
        "Error interno del servidor, no se pudó realizar esta operación.",
      error,
    });
  }
});

app.post('/hotel/upload/:id',[],async(req,res)=>{
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }
    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/hoteles/';
    let images = [];
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];
        //creamos un nombre unico para guardarlo en la DB/uploads compartidos
        let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;

        const paramsUpload = {
            Bucket: process.env.AWS_BUCKET_NAME + '/hoteles/' + `${id}`,
            Key: nombreArchivoUnico, // File name you want to save as in S3
            Body: photo.data,
            ACL: "public-read",
            ContentType: "image",
        };

        images.push(AWSurl + `${id}/${nombreArchivoUnico}`);

        s3.upload(paramsUpload, (error, data) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: "ocurrio un error al subir las imagenes para el hotel asociado",
                        error,
                    },
                });
            }
        });

    });

    Hotel.findById(id, (err, hotelDB) => {
        if (err) {
            // borrarArchivo(nombreArchivoUnico);   
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir las imagenes para el hotel asociado',
                    err
                }
            });
        }
        if (!hotelDB) {
            // borrarArchivo(nombreArchivoUnico);
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'hotel no existe'
                }
            });
        }
        hotelDB.imagenes = images;
        hotelDB.save((err, hotelActualizado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'error al actualizar las imagenes del hotel',
                    err
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Files are uploaded',
                data: images,
                hotel: hotelActualizado
            });
        });
    });
});

app.get('/hoteles', [], (req, res) => {
    let param = req.body.estado;
     Hotel.find({}, 'nombre imagenes ubicacion coordenadas calificacion checkIn  checkOut categoria descripcion estado')
        .sort({nombre:1})
        .exec((err, hotelDB) => {
            //verifica si hay error en la busquedo de los usuarios a la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar los hoteles en la base de datos',
                        err
                    }
                });
            }
            //devuelve el listado de los usuarios en la DB
            Hotel.countDocuments({ estado: param }, (err, conteo) => {
                res.status(200).json({
                    ok: true,
                    cuantos: conteo,
                    hotelDB,
                });
            });
        });
});

app.get('/hotel/:id', [], (req, res) => {
    let id = req.params.id;
    //metodo del mongose para encontrar por parametro en la BD
    Hotel.findOne({ _id: id })
        .exec((err, hotelDB) => {
            //verifica si hay un error al buscar en la DB
            if (err) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: 'No se pudo buscar el hotel en la base de datos',
                        err
                    }
                });
            }
            //devuelve el usuario encontrado
            res.status(200).json({
                ok: true,
                hotelDB
            });
        });
});


app.put('/hotel/:id', [], (req, res) => {

    let id = req.params.id;
   
    let body = _.pick(req.body, [ 'nombre', 'imagenes', 'ubicacion', 'coordenadas', 'checkIn',  'checkOut', 'calificacion', 'categoria', 'descripcion', 'estado']);

    //metodo de mongose para encontrar uno y actualizar
    Hotel.findOneAndUpdate({ _id: id }, body, { new: true }, (err, hotelDB) => {
        //verifica si hay un error al actualizar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo actualizar el hotel en la base de datos',
                    err
                }
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Hotel actualizado correctamente',
            hotelDB
        });

    });
});

//peticion put para actualziar imagenes aws
app.post('/hotel/imagenes/:id',[],async(req,res)=>{
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: 'No file source'
        });
    }
    let AWSurl = 'https://' + process.env.AWS_BUCKET_NAME + '.s3.amazonaws.com/hoteles/';
    let images = [];
     //empty buckter
     const paramsDelete = {
        Bucket: process.env.AWS_BUCKET_NAME,
        Key: `/hoteles/${id}/`, // File name you want to delete in S3
    };
    
    await s3.deleteObject(paramsDelete,(error,data)=>{
        if (error) {
            return res.status(500).json({
                ok:false,
                message: 'Error al eliminar las imagenes en el bucket de aws'
            });
        }
    });
    lodash.forEach(lodash.keysIn(req.files.photos), (key) => {
        let photo = req.files.photos[key];
        //creamos un nombre unico para guardarlo en la DB/uploads compartidos
        let nombreArchivoUnico = `${id}-${new Date().getMilliseconds()}-${photo.name}`;

        const paramsUpload = {
            Bucket: process.env.AWS_BUCKET_NAME + '/hoteles/' + `${id}`,
            Key: nombreArchivoUnico, // File name you want to save as in S3
            Body: photo.data,
            ACL: "public-read",
            ContentType: "image",
        };

        images.push(AWSurl + `${id}/${nombreArchivoUnico}`);

        s3.upload(paramsUpload, (error, data) => {
            if (error) {
                return res.status(500).json({
                    ok: false,
                    error: {
                        message: "ocurrio un error al subir las imagenes para el hotel asociado",
                        error,
                    },
                });
            }
        });

    });

    Hotel.findById(id, (err, hotelDB) => {
        if (err) {
            // borrarArchivo(nombreArchivoUnico);   
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'ocurrio un error al subir las imagenes para el hotel asociado',
                    err
                }
            });
        }
        if (!hotelDB) {
            // borrarArchivo(nombreArchivoUnico);
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'hotel no existe'
                }
            });
        }
        hotelDB.imagenes = images;
        hotelDB.save((err, hotelActualizado) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'error al actualizar las imagenes del hotel',
                    err
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Files are uploaded',
                data: images,
                hotel: hotelActualizado
            });
        });
    });
});


//Peticion delete para deshabilitar un usuario
app.delete('/hotel/:id', [], (req, res) => {
    let id = req.params.id;

    //metodo de mongose para buscar y actualizar
    Hotel.findOneAndRemove({ _id: id }, (err, hotelDB) => {
        //verifica si hay un error al eliminar en la DB
        if (err) {
            return res.status(500).json({
                ok: false,
                error: {
                    message: 'No se pudo eliminar el hotel de la base de datos',
                    err
                }
            });
        }
        //verifica si el sitio turistico no existe en la base de datos
        if (!hotelDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'hotel no encontrado'
                }
            });
        }

        //Devuelve el sitio turistico eliminado
        res.status(200).json({
            ok: true,
            hotel: hotelDB,
            message: 'PHotel eliminado correctamente'
        });

    });
});

module.exports = app;
