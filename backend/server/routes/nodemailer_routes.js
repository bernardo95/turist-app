/* eslint-disable no-unused-vars */
const nodemailer = require("nodemailer");
const express = require("express");
const app = express();

app.post("/nodemailer/plan", [], async (req, res) => {
  let celular = "";
  const {
    numero,
    plan_nombre,
    plan_ubicacion,
    reserva_precio,
    reserva_personas,
    reserva_estado,
    reserva_fecha,
    usuario_nombre,
    usuario_correo,
  } = req.body;
  if (numero != 0) {
    celular = numero;
  }
  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_EMAIL,
      pass: process.env.NODEMAILER_PASSWORD,
    },
  });
  const contentHTMLPlan = `<div class="reserva" style="
    height: 30%;
    width: 80%;
    margin-top: 5%;
    margin-left: 10%;
    margin-right: 10%;
    border-top-color: green;
    border-top-width: 5px;
    border-top-style: solid;
    border-bottom-width: 5px;
    border-bottom-style: solid;
    border-bottom-color: gray" > 
    <h3 style="text-align: left;margin-top: 20px;">Turist App</h3>
    <hr>
    <h4>Hola ${usuario_nombre}</h4>
    <p style="margin-top: 5%;">Muchas gracias por tu reserva. Ya quedó lista</p>
    <p>Aquí un resumen de tu reserva</p>
    <ul style="margin-bottom: 30px;">
        <li >
            <p class="items">Nombre del plan: ${plan_nombre}</p>
        </li>
        <li >
            <p class="items">Ubicación: ${plan_ubicacion}</p>
        </li>
        <li >
            <p class="items">Precio: ${reserva_precio}</p>
        </li>
        <li >
            <p class="items">Número de personas: ${reserva_personas}</p>
        </li>
        <li >
            <p class="items">Fecha: ${reserva_fecha}</p>
        </li>
        <li >
            <p class="items">Estado de la reserva: ${reserva_estado}</p>
        </li>
        <li >
            <p class="items">Información de contacto: ${celular}</p>
        </li>
    </ul>
</div>`;

  let mailOptions = {
    from: process.env.NODEMAILER_PASSWORD,
    to: `${usuario_correo}`,
    subject: "Reserva plan turistico Turist App",
    html: contentHTMLPlan,
  };

  transporter.sendMail(mailOptions, (err, data) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        message: "Ocurrio un error al enviar el mensaje",
        err,
      });
    } else {
      return res.status(200).json({
        ok: true,
        data,
      });
    }
  });
});

app.post("/nodemailer/habitacion", [], async (req, res) => {

  const {
    hotel_nombre,
    habitacion_tipo,
    reserva_habitaciones,
    reserva_personas,
    reserva_precio,
    reserva_estado,
    reserva_fechaInicio,
    reserva_fechaFin,
    usuario_nombre,
    usuario_correo,
  } = req.body;

  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.NODEMAILER_EMAIL,
      pass: process.env.NODEMAILER_PASSWORD,
    },
  });
  const contentHTMLHabitacion = `<div class="reserva" style="
    height: 30%;
    width: 80%;
    margin-top: 5%;
    margin-left: 10%;
    margin-right: 10%;
    border-top-color: green;
    border-top-width: 5px;
    border-top-style: solid;
    border-bottom-width: 5px;
    border-bottom-style: solid;
    border-bottom-color: gray" > 
    <h3 style="text-align: left;margin-top: 20px;">Turist App</h3>
    <hr>
    <h4>Hola ${usuario_nombre}</h4>
    <p style="margin-top: 5%;">Muchas gracias por tu reserva. Ya quedó lista</p>
    <p>Aquí un resumen de tu reserva</p>
    <ul style="margin-bottom: 30px;">
        <li >
            <p class="items">Nombre del hotel: ${hotel_nombre}</p>
        </li>
        <li >
            <p class="items">Habitación: ${habitacion_tipo}</p>
        </li>
        <li >
        <p class="items">Número de habitaciones: ${reserva_habitaciones}</p>
        </li>
        <li >
            <p class="items">Precio: ${reserva_precio}</p>
        </li>
        <li >
            <p class="items">Número de personas: ${reserva_personas}</p>
        </li>
        <li >
            <p class="items">Fecha Inicio: ${reserva_fechaInicio}</p>
        </li>
        <li >
            <p class="items">Fecha Fin: ${reserva_fechaFin}</p>
        </li>
        <li >
            <p class="items">Estado de la reserva: ${reserva_estado}</p>
        </li>
    </ul>
</div>`;

  let mailOptions = {
    from: process.env.NODEMAILER_PASSWORD,
    to: `${usuario_correo}`,
    subject: "Reserva de habitación Turist App",
    html: contentHTMLHabitacion,
  };

  transporter.sendMail(mailOptions, (err, data) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        message: "Ocurrio un error al enviar el mensaje",
        err,
      });
    } else {
      return res.status(200).json({
        ok: true,
        data,
      });
    }
  });
});

module.exports = app;
