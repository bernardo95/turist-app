const express = require('express');

const app = express();

app.use(require('./plan_turistico_routes'));
app.use(require('./login_routes'));
app.use(require('./comentario_routes'));
app.use(require('./hotel_routes'));
app.use(require('./habitacion_routes'));
app.use(require('./restaurante_routes'));
app.use(require('./reserva_plan_routes'));
app.use(require('./reserva_habitacion'));
app.use(require('./nodemailer_routes'));
app.use(require('./usuario_routes'));

module.exports = app;