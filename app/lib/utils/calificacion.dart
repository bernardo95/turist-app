
String rateTransform(num rate){
  String value;
  try {
    value = rate.toString().substring(0,3);
  } catch (e) {
    value = rate.toString().split('.')[0]+'.0';
  }
  return value;
}