import 'package:app/providers/login_state.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MenuWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UsuarioInstancia usuario = Provider.of<UsuarioInstancia>(context);
    LoginState loginProviderUser = Provider.of<LoginState>(context);
    // String imagen;
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
      },
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                      child: FutureBuilder(
                    future: usuario.getPhotoUrl(),
                    builder: (BuildContext context,
                        AsyncSnapshot<dynamic> snapshot) {
                      return CircleAvatar(
                        radius: 30,
                        backgroundImage: (snapshot.data == null)
                            ? AssetImage('assets/img/no-profile.jpg')
                            : CachedNetworkImageProvider(snapshot.data),
                      );
                    },
                  )),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                      padding: EdgeInsets.only(bottom: 5.0),
                      child: FutureBuilder(
                          future: usuario.getNombre(),
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            return Text(
                              snapshot.data ?? '',
                              style: TextStyle(
                                  fontSize: 17.0, color: Colors.white),
                            );
                          })),
                ],
              ),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/img/menu-img.jpg'),
                      fit: BoxFit.cover)),
            ),
            ListTile(
              leading: Icon(Icons.home, color: Colors.blue),
              title: Text(
                'Inicio',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              onTap: () {},
            ),
            FutureBuilder(
              future: usuario.getId(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.data != null) {
                  return ListTile(
                      leading: Icon(Icons.receipt, color: Colors.blue),
                      title: Text('Reservas',
                          style: TextStyle(fontWeight: FontWeight.w600)),
                      onTap: () async {
                        Navigator.pushNamed(context, 'historialReservas',
                            arguments: snapshot.data);
                      });
                } else {
                  return Container();
                }
              },
            ),
            ListTile(
              leading: Icon(Icons.favorite, color: Colors.blue),
              title: Text(
                'Favoritos',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              onTap: () {
                Navigator.pushNamed(context, 'favoritos');
              },
            ),
            ListTile(
              leading: Icon(Icons.library_books, color: Colors.blue),
              title: Text(
                'Terminos y Condiciones',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              onTap: () {
                Navigator.pushNamed(context, 'terminos_condiciones');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app, color: Colors.blue),
              title: FutureBuilder(
                future: usuario.getId(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return (snapshot.data == null)
                      ? Text('Ingresar',
                          style: TextStyle(fontWeight: FontWeight.w600))
                      : Text('Salir',
                          style: TextStyle(fontWeight: FontWeight.w600));
                },
              ),
              onTap: () async {
                var logged = await usuario.isLoggedFunction();
                if (logged) {
                  Navigator.pushNamed(context, 'app');
                  // usuario.clear();
                  loginProviderUser.logout(LoginProvider.FACEBOOK);
                  loginProviderUser.logout(LoginProvider.GOOGLE);
                  loginProviderUser.logout(LoginProvider.NORMAL);
                }
                if (!logged) {
                  Navigator.pushNamed(context, 'loginPage');
                }
              }, //
            ),
          ],
        ),
      ),
    );
  }
}
