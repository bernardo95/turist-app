
import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/models/reserva_habitacion_model.dart';
import 'package:app/models/usuario_model.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class HabitacionesProvider with ChangeNotifier {

  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';
  bool _cargando = false;
  bool _creando = false;
  bool _recargar = true;

  bool get creando => this._creando;
  bool get recargar => this._recargar;

  set recargar(bool value){
    this._recargar = value;
    notifyListeners();
  }
  set creando(bool value){
    this._creando = value;
    notifyListeners();
  }
  List<Habitacion> habitaciones = new List();
  List<Habitacion> habitacionesTodas = new List();

  Future<List<Habitacion>> _procesarRespuesta(String id) async {
    http.Response resp = await http.get('$urlHeroku/habitaciones/hotel/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['habitaciones'];
    final habitaciones = new Habitaciones.fromJsonList(decodedData);
    return habitaciones.items;
  }
  Future<List<Habitacion>> getHabitacionesByHotel(String id) async {
    if (_cargando) return [];
    _cargando = true;
    final resp = await _procesarRespuesta(id);
    habitaciones.addAll(resp);
    _cargando = false;
    notifyListeners();
 
    resp.sort((a,b)=>a.precio.compareTo(b.precio));
    return resp;
  }

  Future<List<Habitacion>> _procesarRespuestaTodos() async{
    http.Response resp = await http.get('$urlHeroku/habitaciones');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['habitacionDB'];
    final habitaciones = new Habitaciones.fromJsonList(decodedData);
    return habitaciones.items;
  }
  Future<List<Habitacion>> getHabitaciones() async{
    if (_cargando) return [];
    _cargando = true;
    final resp = await _procesarRespuestaTodos();
    habitacionesTodas.addAll(resp);
    _cargando = false;
    notifyListeners();
    return resp;
  }
  
  Future<Habitacion> getHabitacion(String id) async {
    http.Response resp = await http.get('$urlHeroku/habitacion/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['habitacionDB'];
    final habitacion = new Habitacion.fromJson(decodedData);
    return habitacion;
  }
  Future<ReservaHabitacion> reservarHabitacion(ReservaHabitacion reservaInside, Hotel hotel, Habitacion habitacion,
      String usuarioId) async {

    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };

    final reservaJson = json.encode(reservaInside.toJson());
    http.Response resp = await http.post('$urlHeroku/reserva/habitacion',
        body: reservaJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['reserva'];
    final reserva = new ReservaHabitacion.fromJson(decodedData);
    await enviarCorreoReserva(hotel,habitacion, usuarioId, reserva);
    return reserva;
  }

  Future<dynamic> enviarCorreoReserva(Hotel hotel,Habitacion habitacion, String usuarioId, ReservaHabitacion reserva) async {
    final Usuario usuario = await UsuarioProvider().getUsuario(usuarioId);
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };

    final map = {
      'hotel_nombre': hotel.nombre,
      'habitacion_tipo': habitacion.tipo,
      'reserva_habitaciones':reserva.habitaciones,
      'reserva_personas': reserva.personas,
      'reserva_precio': reserva.precio,
      'reserva_estado': reserva.estado,
      'reserva_fechaInicio': reserva.fechaInicio,
      'reserva_fechaFin': reserva.fechaFin,
      'usuario_nombre': usuario.nombre,
      'usuario_correo': usuario.email,
    };

    final maptoJson = json.encode(map);
    http.Response resp = await http.post('$urlHeroku/nodemailer/habitacion',
        body: maptoJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['correo'];
    return decodedData;
  }
  Future<List<ReservaHabitacion>> getReservasHabitacionesByUser(String id) async{
    http.Response resp = await http.get('$urlHeroku/reservas/habitacion/usuario/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['reservas'];
    final reservas = new ReservasHabitaciones.fromJsonList(decodedData);
    return reservas.items;
  }
  Future<ReservaHabitacion> cancelarReservaHabitacion(String id) async{
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final body = {
      'estado': 'CANCELADO',
    };
    final bodytoJson = json.encode(body);
    http.Response resp = await http.put('$urlHeroku/reserva/habitacion/$id',body: bodytoJson,headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['reserva'];
    final reserva = new ReservaHabitacion.fromJson(decodedData);
    return reserva;
  }
}