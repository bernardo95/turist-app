import 'package:app/models/plan_model.dart';
import 'package:app/models/reserva_plan_model.dart';
import 'package:app/models/usuario_model.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class PlanesProvider with ChangeNotifier {

  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';
  bool _creando = false;
  bool _recargar = true;

  bool get creando => this._creando;
  bool get recargar => this._recargar;

  set recargar(bool value){
    this._recargar = value;
    notifyListeners();
  }
  set creando(bool value){
    this._creando = value;
    notifyListeners();
  }

  List<Plan> planes = new List();
  List<ReservaPlan> reservas = new List();

  Future<List<Plan>> _procesarRespuesta() async {
    http.Response resp = await http.get('$urlHeroku/planes');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['planDB'];
    final planes = new Planes.fromJsonList(decodedData);
    final planesActivos = planes.items.where((plan) => plan.estado==true).toList();
    return planesActivos;
  }

  Future<List<Plan>> getPlanes({String filtro}) async {
    final resp = await _procesarRespuesta();
    if (filtro!=null) {
    final listaFiltrada = filtrado(resp,filtro);
    return listaFiltrada;
    }else{
      return resp;
    }
    
  }

  List<Plan> filtrado(List<Plan> planes,String filtro) {
    List<Plan> listaFiltrada = []; 
    if (filtro=='Precio') {
      planes.sort((a,b)=>a.precio.compareTo(b.precio));
      listaFiltrada = planes.reversed.toList();
    }else if(filtro=='Reputacion'){
      planes.forEach((plan) {
        if (plan.calificacion!=null) {
          listaFiltrada.add(plan);
        }
      });
      if (listaFiltrada.length==1) {
        return planes;
      }
      listaFiltrada.sort((a,b)=>a.calificacion.compareTo(b.calificacion));
      return listaFiltrada.reversed.toList();
    }else{
      planes.forEach((plan) => {
      if (plan.nombre.contains('$filtro')) {
        listaFiltrada.add(plan)
      }else if(plan.ubicacion.contains('$filtro')){
        listaFiltrada.add(plan)
      }
    });
    }
    
    return listaFiltrada;
  }

  Future<Plan> getPlan(String id) async {
    http.Response resp = await http.get('$urlHeroku/planes/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['planDB'];
    final plan = new Plan.fromJson(decodedData);
    return plan;
  }

  Future<Plan> updatePlan(String id, double rateAcumulado) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final Plan body = await getPlan(id);
    body.calificacion = rateAcumulado;
    final map = body.toJson();
    final planJson = json.encode(map);
    http.Response resp =
        await http.put('$urlHeroku/plan/$id', body: planJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['planDB'];
    final plan = new Plan.fromJson(decodedData);
    return plan;
  }

  Future<ReservaPlan> reservarPlan(ReservaPlan reservaInside, Plan plan,
      String usuarioId, int numero) async {

    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };

    final reservaJson = json.encode(reservaInside.toJson());
    http.Response resp = await http.post('$urlHeroku/reserva/plan',
        body: reservaJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['reserva'];
    final reserva = new ReservaPlan.fromJson(decodedData);
    await enviarCorreoReserva(numero, plan, usuarioId, reserva);
    return reserva;
  }

  Future<dynamic> enviarCorreoReserva(
      int numero, Plan plan, String usuarioId, ReservaPlan reserva) async {
    final Usuario usuario = await UsuarioProvider().getUsuario(usuarioId);
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = {
      'numero': numero,
      'plan_nombre': plan.nombre,
      'plan_ubicacion': plan.ubicacion,
      'reserva_precio': reserva.precio,
      'reserva_personas': reserva.personas,
      'reserva_estado': reserva.estado,
      'reserva_fecha': reserva.fecha,
      'usuario_nombre': usuario.nombre,
      'usuario_correo': usuario.email,
    };

    final maptoJson = json.encode(map);
    http.Response resp = await http.post('$urlHeroku/nodemailer/plan',
        body: maptoJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['correo'];
    return decodedData;
  }

  Future<List<ReservaPlan>> getReservasPlanesByUser(String id) async{
    http.Response resp = await http.get('$urlHeroku/reservas/usuario/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['reservas'];
    final reservas = new ReservasPlanes.fromJsonList(decodedData);
    return reservas.items;
  }
  Future<ReservaPlan> cancelarReservaPlan(String id) async{
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final body = {
      'estado': 'CANCELADO',
    };
    final bodytoJson = json.encode(body);
    http.Response resp = await http.put('$urlHeroku/reserva/$id',body: bodytoJson,headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['reserva'];
    final reserva = new ReservaPlan.fromJson(decodedData);
    return reserva;
  }

  
}
