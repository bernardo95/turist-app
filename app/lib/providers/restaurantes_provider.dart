import 'package:app/models/restaurante_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class RestauranteProvider with ChangeNotifier {

  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';

  bool _creando = false;
  bool _recargar = true;
  bool get creando => this._creando;
  bool get recargar => this._recargar;

  set recargar(bool value){
    this._recargar = value;
    notifyListeners();
  }
  set creando(bool value){
    this._creando = value;
    notifyListeners();
  }

  List<Restaurante> restaurantes = new List();

Future<List<Restaurante>> _procesarRespuesta() async {
    http.Response resp = await http.get('$urlHeroku/restaurantes');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['restauranteDB'];
    final restaurantes = new Restaurantes.fromJsonList(decodedData);
    final restaurantesActivos = restaurantes.items.where((restaurante) => restaurante.estado==true).toList();
    return restaurantesActivos;
  }

  Future<List<Restaurante>> getRestaurantes({String filtro}) async {
    final resp = await _procesarRespuesta();
    if (filtro!=null) {
    final listaFiltrada = filtrado(resp,filtro);
    return listaFiltrada;
    }else{
      return resp;
    }
  }

  List<Restaurante> filtrado(List<Restaurante> restaurantes,String filtro) {
    List<Restaurante> listaFiltrada = []; 
    if(filtro=='Reputacion'){
      restaurantes.forEach((restaurante) {
        if (restaurante.calificacion!=null) {
          listaFiltrada.add(restaurante);
        }
      });
      if (listaFiltrada.length==1) {
        return restaurantes;
      }
      listaFiltrada.sort((a,b)=>a.calificacion.compareTo(b.calificacion));
      return listaFiltrada.reversed.toList();
    }else{
      restaurantes.forEach((restaurante) => {
      if (restaurante.nombre.contains('$filtro')) {
        listaFiltrada.add(restaurante)
      }else if(restaurante.ubicacion.contains('$filtro')){
        listaFiltrada.add(restaurante)
      }
    });
    }
    
    return listaFiltrada;
  }

  Future<Restaurante> getRestaurante(String id) async {
    http.Response resp = await http.get('$urlHeroku/restaurante/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['restauranteDB'];
    final restaurante = new Restaurante.fromJson(decodedData);
    return restaurante;
  }

  Future<Restaurante> updateRestaurante(String id, double rateAcumulado) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final Restaurante body = await getRestaurante(id);
    body.calificacion = rateAcumulado;
    final map = body.toJson();
    final restauranteJson = json.encode(map);
    http.Response resp =
        await http.put('$urlHeroku/restaurante/$id', body: restauranteJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['restauranteDB'];
    final restaurante = new Restaurante.fromJson(decodedData);
    return restaurante;
  }
}