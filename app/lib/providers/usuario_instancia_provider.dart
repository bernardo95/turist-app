import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UsuarioInstancia with ChangeNotifier {
  Future<bool> isFavorito(String id) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final value = prefs.getBool(id);
      return value;
    } catch (e) {
      return false;
    }
  }

  Future<void> setListFavoritosPlanes(List ids) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('favoritos-planes', ids);
  }
  Future<void> setListFavoritosHoteles(List ids) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('favoritos-hoteles', ids);
  }
  Future<void> setListFavoritosRestaurantes(List ids) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('favoritos-restaurantes', ids);
  }
  Future<List<String>> getListPlanesFavoritos() async {
    List<String> favoritos = [];
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var value = prefs.getStringList('favoritos-planes');
      if (value == null) {
        return favoritos;
      } else {
        favoritos = value;
      }
      return favoritos;
    } catch (e) {
      return favoritos;
    }
  }
  Future<List<String>> getListHotelesFavoritos() async {
    List<String> favoritos = [];
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var value = prefs.getStringList('favoritos-hoteles');
      if (value == null) {
        return favoritos;
      } else {
        favoritos = value;
      }
      return favoritos;
    } catch (e) {
      return favoritos;
    }
  }

  Future<List<String>> getListRestaurantesFavoritos() async {
    List<String> favoritos = [];
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var value = prefs.getStringList('favoritos-restaurantes');
      if (value == null) {
        return favoritos;
      } else {
        favoritos = value;
      }
      return favoritos;
    } catch (e) {
      return favoritos;
    }
  }

  Future<bool> isLoggedFunction() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return (prefs.getBool('logged') == null)
          ? false
          : prefs.getBool('logged');
    } catch (e) {
      return false;
    }
  }

  Future<dynamic> getId() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('id');
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> getNombre() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('nombre');
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> getEmail() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('email');
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> getPhotoUrl() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString('photoUrl');
    } catch (e) {
      return null;
    }
  }

  Future<void> clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    notifyListeners();
  }
}
