import 'package:flutter/material.dart';

class FiltroProvider with ChangeNotifier {
  String _filtroPlan = '';
  String _filtroHotel = '';
  String _filtroRestaurante = '';

  String get filtroPlan => this._filtroPlan;
  String get filtroHotel => this._filtroHotel;
  String get filtroRestaurante => this._filtroRestaurante;

  set filtroPlan(String value){
    this._filtroPlan = value;
    notifyListeners();
  }
  set filtroHotel(String value){
    this._filtroHotel = value;
    notifyListeners();
  }
  set filtroRestaurante(String value){
    this._filtroRestaurante = value;
    notifyListeners();
  }

}