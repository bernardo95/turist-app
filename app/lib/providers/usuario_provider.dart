import 'dart:io';
import 'package:app/providers/profile_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:app/models/usuario_model.dart';

class UsuarioProvider {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final String url = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';

  Future<bool> _procesarRespuesta(String urlHeroku, String email) async {
    http.Response resp = await http.get('$urlHeroku/usuario/exist/$email');
    final decode = json.decode(resp.body);
    bool data = decode['callback'];
    return data;
  }

  Future<bool> getEmailExist(String email) async {
    final resp = await _procesarRespuesta(urlHeroku, email);
    return resp;
  }

  Future<Usuario> postUsuario(Usuario user) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = user.toMap();
    final userJson = json.encode(map);
    http.Response resp =
        await http.post('$urlHeroku/usuario', body: userJson, headers: header);
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    final deco = decodedResp['usuario'];
    final respMapeadatoModel = Usuario.fromMap(deco);
    if (resp.statusCode == 500 || resp.statusCode == 400) {
      return new Usuario();
    } else {
      return respMapeadatoModel;
    }
  }

  Future<dynamic> postImageUsuario(File file, String id) async {
    if (id == null) return null;
    var stream = new http.ByteStream((file.openRead()));
    var uri = Uri.parse("$urlHeroku/usuario/upload/image/$id");
    var length = await file.length();
    var request = new http.MultipartRequest("POST", uri);
    var multipartFile = new http.MultipartFile(
      'photo',
      stream,
      length,
      filename: file.path,
    );
    request.files.add(multipartFile);
    var response = await request.send();
    if (response.statusCode == 200) {
      return response;
    } else {
      return null;
    }
  }
  Future<dynamic> updateImageUsuario(File file, String id) async {
    if (id == null) return null;
    var stream = new http.ByteStream((file.openRead()));
    var uri = Uri.parse("$urlHeroku/usuario/upload/imageUpdate/$id");
    var length = await file.length();
    var request = new http.MultipartRequest("POST", uri);
    var multipartFile = new http.MultipartFile(
      'photo',
      stream,
      length,
      filename: file.path,
    );
    request.files.add(multipartFile);
    var response = await request.send();
    if (response.statusCode == 200) {
      return response;
    } else {
      return null;
    }
  }

  Future<Usuario> crearCuenta(Usuario user, File file) async {
    Usuario responseUser;
    dynamic responseUserImage;
    if (file == null) {
      responseUser = await postUsuario(user);
      await _auth.createUserWithEmailAndPassword(
          email: user.email, password: user.password);
      return responseUser;
    } else {
      responseUser = await postUsuario(user);
      responseUserImage = await postImageUsuario(file, responseUser.id);
      if (responseUserImage == null) {
        return new Usuario();
      } else {
        responseUser = await ProfileProvider().getUsuario(responseUser.id);
        await _auth.createUserWithEmailAndPassword(
            email: user.email, password: user.password);
        return responseUser;
      }
    }
  }
  Future<Usuario> actualizarCuenta(Usuario user, File file) async{
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = user.toMap();
    final userJson = json.encode(map);
    http.Response resp =
        await http.put('$urlHeroku/usuario/${user.id}', body: userJson, headers: header);
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    final deco = decodedResp['usuario'];
    final respMapeadatoModel = Usuario.fromMap(deco);
    if (resp.statusCode == 500 || resp.statusCode == 400) {
      return new Usuario();
    } else {
      return respMapeadatoModel;
    }
  }
  Future<Usuario> getUsuario(String id)async{
    http.Response resp = await http.get('$urlHeroku/usuario/$id');
    Map<String, dynamic> decodedResp = json.decode(resp.body);
    final deco = decodedResp['usuarioDB'];
    final respMapeadatoModel = Usuario.fromMap(deco);
    return respMapeadatoModel;
  }
}
