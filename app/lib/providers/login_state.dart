import 'dart:convert';
import 'package:app/models/usuario_model.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum LoginProvider { GOOGLE, FACEBOOK, NORMAL }

class LoginState with ChangeNotifier {
  final FacebookLogin _facebookLogin = FacebookLogin();
  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: [
    'profile',
    'email',
    'https://www.googleapis.com/auth/calendar',
    'https://www.googleapis.com/auth/user.birthday.read',
    'https://www.googleapis.com/auth/user.gender.read'
  ]);
  
  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool _loggedIn = false;
  bool _normalUser = false;
  bool _loading = false;
  String _errorMessageLogin = '';
  bool _verifyUserBackend;
  FirebaseUser _userFirebase;
  Usuario _userNodeJS;

  final String url = 'http://10.0.2.2:3000';
    final String urlHeroku = 'https://turist-app.herokuapp.com';

  String existMessageError() => this._errorMessageLogin;

  set setMessageError(String value) {
    this._errorMessageLogin = value;
    notifyListeners();
  }

  bool isLoading() => this._loading;

  bool isNormalUser() => this._normalUser;

  bool isLoggedIn() => this._loggedIn;

  FirebaseUser currentUserFirebase() => this._userFirebase;

  Usuario currentUserNodeJS() => this._userNodeJS;

  void login({LoginProvider loginProvider}) async {
    _loading = true;
    notifyListeners();
    switch (loginProvider) {
      case LoginProvider.GOOGLE:
        _userFirebase = await _handleSignInGoogle();
        break;
      case LoginProvider.FACEBOOK:
        _userFirebase = await _handleSignInFacebook();
        break;
      default:
    }
    _loading = false;
    if (_userFirebase != null) {
      await setSharePreferences(_userFirebase);
      _loggedIn = true;
      notifyListeners();
    } else {
      _loggedIn = false;
      notifyListeners();
    }
  }

  void loginNormal({LoginProvider loginProvider, Usuario usuario}) async {
    _loading = true;
    notifyListeners();
    _userNodeJS = await _handleSignInNormal(usuario);
    _loading = false;
    if (_userNodeJS != null) {
      await setSharePreferencesNodeJS(_userNodeJS);
      _loggedIn = true;
      _normalUser = true;
      notifyListeners();
    } else {
      _loggedIn = false;
      notifyListeners();
    }
  }

  Future<void> setSharePreferences(FirebaseUser user) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // await preferences.setString('id', user.uid);
    await preferences.setString('nombre', user.displayName);
    await preferences.setString('email', user.email);
    await preferences.setString('photoUrl', user.photoUrl);
    await preferences.setBool('logged', true);
  }

  Future<void> setSharePreferencesNodeJS(Usuario user) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('id', user.id);
    await preferences.setString('nombre', user.nombre + ' ' + user.apellido);
    await preferences.setString('email', user.email);
    await preferences.setString('photoUrl',user.imgProfile);
    await preferences.setBool('logged', true);
  }

  Future<void> logoutPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
    await preferences.setBool('logged', false);
  }

  void logout(LoginProvider loginProvider) async {
    switch (loginProvider) {
      case LoginProvider.GOOGLE:
        _googleSignIn.signOut();
        _userFirebase = null;
        _loggedIn = false;
        notifyListeners();
        break;
      case LoginProvider.FACEBOOK:
        _facebookLogin.logOut();
        _userFirebase = null;
        _loggedIn = false;
        notifyListeners();
        break;
      case LoginProvider.NORMAL:
        _userNodeJS = null;
        _loggedIn = false;
        logoutPreferences();
        notifyListeners();
        break;
      default:
    }
  }

  Future<dynamic> _handleSignInNormal(Usuario usuario) async {
    AuthResult authResult;
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final body = {'email': usuario.email, 'password': usuario.password};
    final bodyJson = json.encode(body);

    try {
      await _auth
          .signInWithEmailAndPassword(
              email: usuario.email, password: usuario.password)
          .then((onValue) => {authResult = onValue});
    } on PlatformException catch (e) {
      if (e.code == 'ERROR_USER_NOT_FOUND') {
        _errorMessageLogin = 'Usuario no encontrado';
      }
      if (e.code == 'ERROR_INVALID_EMAIL') {
        _errorMessageLogin = 'Email o contraseña incorrecto';
      }
      if (e.code == 'ERROR_WRONG_PASSWORD') {
        _errorMessageLogin = 'Email o contraseña incorrecto';
      }
      _loading = false;
      return null;
    } catch (err) {
      _loading = false;
      _errorMessageLogin = 'Ocurrió un error';
      return null;
    }
    if (authResult.user != null) {
      http.Response resp =
          await http.post('$urlHeroku/login', body: bodyJson, headers: header);
      Map<String, dynamic> decodedResp = json.decode(resp.body);
      if (resp.statusCode == 200) {
        final resp = decodedResp['usuario'];
        usuario = this.createModelUser(resp);
        return usuario;
      }
    }
  }

  Usuario createModelUser(resp) {
    Usuario usuario = Usuario(
        nombre: resp['nombre'],
        apellido: resp['apellido'],
        email: resp['email'],
        estado: resp['estado'],
        facebook: resp['facebook'],
        fechaNacimiento: resp['fecha_nacimiento'],
        genero: resp['genero'],
        google: resp['google'],
        id: resp['_id'],
        imgProfile: resp['img_profile'],
        role: resp['role']);
    return usuario;
  }

  Future<FirebaseUser> _handleSignInGoogle() async {
    GoogleSignInAccount googleUser;
    AuthCredential credential;
    try {
      await _googleSignIn
          .signIn()
          .catchError((onError) => {_loading = false})
          .then((onValue) => {googleUser = onValue});
    } on PlatformException catch (e) {
      _loading = false;
      return null;
    } catch (error) {
      _loading = false;
      return null;
    }

    if (googleUser != null) {
      await _verifyGoogleUserBackend(googleUser);
      if (!_verifyUserBackend) {
        _loading = false;
        return null;
      }
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
    } else {
      _loading = false;
      return null;
    }

    try {
      if (googleUser != null) {
        final FirebaseUser user =
            (await _auth.signInWithCredential(credential)).user;
        return user;
      }
    } on PlatformException catch (e) {
      controlPlatformErrorException(e);
    } catch (e) {
      _loading = false;
      return null;
    }
  }

  Future<FirebaseUser> _handleSignInFacebook() async {
    FacebookLoginResult result;
    FirebaseUser user;
    try {
      await _facebookLogin
          .logIn(['email']).then((onValue) => {result = onValue});
    } catch (error) {
      _errorMessageLogin = 'Error en la aplicacion de facebook';
          notifyListeners();

      _loading = false;
      return null;
    }

    if (result.status != FacebookLoginStatus.loggedIn) {
       _errorMessageLogin = 'Error al ingresar por facebook';
          notifyListeners();
      _loading = false;
      return null;
    }

    final graphResponse = await http.get(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result.accessToken.token}');

    final profile = json.decode(graphResponse.body);

    await _authorizeFacebookUserBackend(profile);
    if (!_verifyUserBackend) {
      _loading = false;
      return null;
    }

    final AuthCredential credential = FacebookAuthProvider.getCredential(
      accessToken: result.accessToken.token,
    );
    try {
      if (!_verifyUserBackend) {
        _loading = false;
        return null;
      }
      user = (await _auth.signInWithCredential(credential)).user;
      await _createFacebookUserBackend(user);
      return user;
    } on PlatformException catch (e) {
      controlPlatformErrorException(e);
    } catch (e) {
      _loading = false;
      return null;
    }
  }

  void controlPlatformErrorException(PlatformException e) {
    if (e.code == 'ERROR_INVALID_CREDENTIAL') {
      _errorMessageLogin = 'Email o contraseña incorrecto';
          notifyListeners();

    }
    if (e.code == 'ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL') {
      _errorMessageLogin =
          'El usuario ya se autentico por otro metodo de acceso';
          notifyListeners();
    }
    _loading = false;
    return null;
  }

  Future<void> _verifyGoogleUserBackend(GoogleSignInAccount googleUser) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = {
      'nombre': googleUser.displayName.split(' ')[0],
      'apellido': googleUser.displayName.split(' ')[1],
      'email': googleUser.email,
      'fecha_nacimiento': '',
      'genero': '',
      'img_profile': googleUser.photoUrl
    };
    final userJson = json.encode(map);
    http.Response resp =
        await http.post('$urlHeroku/login/google', body: userJson, headers: header);
    final decode = json.decode(resp.body);
    print(decode);
    try {
      if (decode['logged'] == true || decode['google'] == true) {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        await preferences.setString('id', decode['usuario']['_id']);
        _verifyUserBackend = true;
      } else {
        _errorMessageLogin = 'El usuario ya se autentico por otro metodo';
        notifyListeners();
        _verifyUserBackend = false;
      }
    } catch (e) {}
  }

  Future<void> _authorizeFacebookUserBackend(profile) async {
    _verifyUserBackend = false;
    SharedPreferences preferences = await SharedPreferences.getInstance();

    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = {
      'email': profile['email'],
    };
    final userJson = json.encode(map);
    http.Response resp = await http.post('$urlHeroku/verify/facebook',
        body: userJson, headers: header);
    final decode = json.decode(resp.body);
    print(decode);
    try {
      if (decode['facebook'] == true) {
        preferences.setString('id', decode['usuarioDB']['_id']);
        _verifyUserBackend = true;
      } else if(decode['existe']==false){
        // preferences.setString('id', decode['usuarioDB']['_id']);
        _verifyUserBackend = true;
      }
      else{
        _errorMessageLogin = decode['message'];
        _verifyUserBackend = false;
        notifyListeners();
      }
    } catch (e) {}
  }

  Future<void> _createFacebookUserBackend(FirebaseUser user) async {
    _verifyUserBackend = false;
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = {
      'nombre': user.displayName.split(' ')[0],
      'apellido': user.displayName.split(' ')[1],
      'email': user.email,
      'fecha_nacimiento': '',
      'genero': '',
      'img_profile': user.photoUrl
    };
    final userJson = json.encode(map);
    http.Response resp =
        await http.post('$urlHeroku/login/facebook', body: userJson, headers: header);
    final decode = json.decode(resp.body);
    try {
      if (decode['logged'] == true || decode['facebook'] == true) {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        await preferences.setString('id', decode['usuario']['_id']);
        _verifyUserBackend = true;
      } else {
        _errorMessageLogin = 'El usuario ya se autentico por otro metodo';
        notifyListeners();
        _verifyUserBackend = false;
      }
    } catch (e) {}
  }
}
