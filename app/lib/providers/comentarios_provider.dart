import 'package:app/models/calificacion_model.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/providers/restaurantes_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ComentariosProvider with ChangeNotifier {
  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';
  bool _cargando = false;
  bool _comentando = false;

  bool get comentando => this._comentando;

  set comentando(bool value) {
    this._comentando = value;
    notifyListeners();
  }

  List<Calificacion> get getComentariosList => this.comentarios;

  List<Calificacion> comentarios = new List();

  Future<List<Calificacion>> _procesarRespuesta(String ref) async {
    http.Response resp = await http.get('$urlHeroku/comentario/$ref');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['comment'];
    final comentarios = new Calificaciones.fromJsonList(decodedData);
    return comentarios.items;
  }

  Future<List<Calificacion>> getComentarios(String ref) async {
    // if (_cargando) return [];
    // _cargando = true;
    final resp = await _procesarRespuesta(ref);
    // comentarios.addAll(resp);
    // _cargando = false;
    // notifyListeners();
    return resp;
  }

   Future<int> getComentariosCount(String ref) async {
    if (_cargando) return 0;
    _cargando = true;
    List<Calificacion> resp = await _procesarRespuesta(ref);
    _cargando = false;
    return resp.length;
  }

  Future<void> postComentario(Calificacion calificacion, String tipo) async {
    // print(calificacion);
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final map = calificacion.toJson();
    final califiacionJson = json.encode(map);
    http.Response resp = await http.post('$urlHeroku/comentario',
        body: califiacionJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodeData = mapDecodedData['comentario'];
    final comentario = new Calificacion.fromJson(decodeData);
    comentarios.add(comentario);
    notifyListeners();

    if (tipo == "plan") {
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await PlanesProvider().updatePlan(calificacion.idRef, rateAcumulado);
      } catch (e) {
        await PlanesProvider().updatePlan(calificacion.idRef, 0);
      }
    } else if (tipo == "hotel") {
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await HotelesProvider().updateHotel(calificacion.idRef, rateAcumulado);
      } catch (e) {
        await HotelesProvider().updateHotel(calificacion.idRef, 0);
      }
    }else if(tipo=="restaurante"){
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await RestauranteProvider().updateRestaurante(calificacion.idRef, rateAcumulado);
      } catch (e) {
        await RestauranteProvider().updateRestaurante(calificacion.idRef, 0);
      }
    }
  }

  Future<Calificacion> deleteComentario(
      String id, String ref, String tipo) async {
    http.Response resp = await http.delete('$urlHeroku/comentario/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['comment'];
    final comentario = new Calificacion.fromJson(decodedData);
    await getComentarios(ref);
    notifyListeners();

    if (tipo == "plan") {
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await PlanesProvider().updatePlan(ref, rateAcumulado);
      } catch (e) {
        await PlanesProvider().updatePlan(ref, 0);
      }
    } else if (tipo == "hotel") {
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await HotelesProvider().updateHotel(ref, rateAcumulado);
      } catch (e) {
        await HotelesProvider().updateHotel(ref, 0);
      }
    }else if(tipo=="restaurante"){
      try {
        final mapComentario = comentarios.map((comentario) => comentario.rate);
        final rateAcumulado =
            mapComentario.reduce((value, element) => value + element) /
                mapComentario.length;
        await RestauranteProvider().updateRestaurante(ref, rateAcumulado);
      } catch (e) {
        await RestauranteProvider().updateRestaurante(ref, 0);
      }
    }

    return comentario;
  }
}
