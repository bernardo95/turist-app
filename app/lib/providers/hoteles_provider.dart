import 'package:app/models/hotel_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class HotelesProvider with ChangeNotifier {
  // HotelesProvider() {
  //   this.getHoteles();
  // }

  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';
  // bool _cargando = false;
  List<dynamic> _hotelesSortByPrecio = List();
  List<dynamic> get hotelesSortByPrecio => this._hotelesSortByPrecio;

  set hotelesSortByPrecio(List<dynamic> data) {
    setData(data);
    notifyListeners();
  }

  Future<void> setData(List data) async {
  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString("${data[0]}", data[1]);
  } catch (e) {
    // return new List();
  }
  }


  bool _recargar = true;
  bool get recargar => this._recargar;
  
  set recargar(bool value) {
    this._recargar = value;
    notifyListeners();
  }

  Future<List<Hotel>> _procesarRespuesta() async {
    http.Response resp = await http.get('$urlHeroku/hoteles');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    List<dynamic> decodedData = mapDecodedData['hotelDB'];
    final hoteles = new Hoteles.fromJsonList(decodedData);
    final hotelesActivos = hoteles.items.where((hotel) => hotel.estado==true).toList();
    return hotelesActivos;
  }

  Future<List<Hotel>> getHoteles({String filtro}) async {
    final resp = await _procesarRespuesta();
    // hoteles.addAll(resp);
    if (filtro!=null) {
    final listaFiltrada = filtrado(resp,filtro);
    return listaFiltrada;
    }else{
      return resp;
    }
  }

  Future<List<Hotel>> filtrado(List<Hotel> hoteles,String filtro) async {
    List<Hotel> listaFiltrada = []; 
    if (filtro=='Precio') {
      List<HotelPrecio> orden = [];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      hoteles.forEach((element) {
        // orden["${element.id}"]=;
        HotelPrecio temp = HotelPrecio(element.id,int.parse(prefs.getString("${element.id}")));
        orden.add(temp);
      });
      orden.sort((a,b)=>a.precio.compareTo(b.precio));
      orden.forEach((element) {
        hoteles.forEach((hotel) {
          if (hotel.id==element.id) {
            listaFiltrada.add(hotel);
          }
        });
      });
      return listaFiltrada;
    }else if(filtro=='Reputacion'){
      hoteles.forEach((hotel) {
        if (hotel.calificacion!=null) {
          listaFiltrada.add(hotel);
        }
      });
      if (listaFiltrada.length==1) {
        return hoteles;
      }
      listaFiltrada.sort((a,b)=>a.calificacion.compareTo(b.calificacion));
      return listaFiltrada.reversed.toList();
    }else{
      hoteles.forEach((hotel) => {
      if (hotel.nombre.contains('$filtro')) {
        listaFiltrada.add(hotel)
      }else if(hotel.ubicacion.contains('$filtro')){
        listaFiltrada.add(hotel)
      }
    });
    }
    
    return listaFiltrada;
  }

  Future<Hotel> getHotel(String id) async {
    http.Response resp = await http.get('$urlHeroku/hotel/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['hotelDB'];
    final hotel = new Hotel.fromJson(decodedData);
    return hotel;
  }

  Future<Hotel> updateHotel(String id, double rateAcumulado) async {
    final header = {
      "Accept": " application/json",
      "Content-Type": "application/json"
    };
    final Hotel body = await getHotel(id);
    body.calificacion = rateAcumulado;
    final map = body.toJson();
    final hotelJson = json.encode(map);
    http.Response resp =
        await http.put('$urlHeroku/hotel/$id', body: hotelJson, headers: header);
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['hotelDB'];
    final hotel = new Hotel.fromJson(decodedData);
    return hotel;
  }
}

class HotelPrecio {
  HotelPrecio(this.id,this.precio);
  String id;
  int precio;
}