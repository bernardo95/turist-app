import 'package:app/models/usuario_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ProfileProvider with ChangeNotifier {
  final String urlLocal = 'http://10.0.2.2:3000';
  final String urlHeroku = 'https://turist-app.herokuapp.com';
  bool _cargando = false;
  Usuario usuario;

  Future<Usuario> _procesarRespuesta(String id) async {
    http.Response resp = await http.get('$urlHeroku/usuario/$id');
    Map<String, dynamic> mapDecodedData = json.decode(resp.body);
    dynamic decodedData = mapDecodedData['usuarioDB'];
    final usuario = new Usuario.fromMap(decodedData);
    return usuario;
  }

  Future<Usuario> getUsuario(String id) async {
    if (_cargando) return null;
    _cargando = true;
    final resp = await _procesarRespuesta(id);
    usuario = resp;
    _cargando = false;
    notifyListeners();
    return resp;
  }
}
