import 'package:app/providers/filtro_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FiltroRestaurantesPage extends StatelessWidget {
  const FiltroRestaurantesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 1),
      child: Column(
        children: <Widget>[
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Wrap(
              spacing: 5,
              children: <Widget>[
                FilterChipWidget(
                  chipName: 'Reputacion',
                ),
                FilterChipWidget(
                  chipName: 'Chicamocha',
                ),
                FilterChipWidget(
                  chipName: 'San Gil',
                ),
                FilterChipWidget(
                  chipName: 'Suaita',
                ),
                FilterChipWidget(
                  chipName: 'Aratoca',
                ),
                FilterChipWidget(
                  chipName: 'Charalá',
                ),
                FilterChipWidget(
                  chipName: 'Florián',
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class FilterChipWidget extends StatefulWidget {
  final String chipName;
  FilterChipWidget({Key key, this.chipName}) : super(key: key);

  @override
  _FilterChipWidgetState createState() => _FilterChipWidgetState();
}

class _FilterChipWidgetState extends State<FilterChipWidget> {
  var _isSelected = false;
  @override
  Widget build(BuildContext context) {
    return FilterChip(
      label: Text('${widget.chipName}'),
      selected: _isSelected,
      onSelected: (bool newValue) {
        setState(() {
          _isSelected = newValue;
        });
        if (_isSelected) {
        Provider.of<FiltroProvider>(context,listen: false).filtroRestaurante = widget.chipName;
        } else {
        Provider.of<FiltroProvider>(context,listen: false).filtroRestaurante = "";

        }
      },
      selectedColor: Colors.green,
      checkmarkColor: Colors.white,
      labelStyle: TextStyle(color: (_isSelected)? Colors.white : Colors.black),
    );
  }
}
