import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/pages/hotel/descripcion_hotel.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ArgumentsReserva {
  Habitacion habitacion;
  Hotel hotel;
  ArgumentsReserva(this.habitacion,this.hotel);
}
class HabitacionPage extends StatelessWidget {
  final Arguments args;
  const HabitacionPage({Key key, this.args}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.pop(context);
      },
          child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
            title: Text('Reserva tu habitación'),
            centerTitle: true,
            elevation: 0,
        ),
        body: ChangeNotifierProvider(
            create: (_) => _ModelPage(),
            child: SafeArea(child: _MainScroll(habitaciones: args.habitaciones,hotel:args.hotel))),
      ),
    );
  }
}

class _MainScroll extends StatelessWidget {
  final List<Habitacion> habitaciones;
  final Hotel hotel;
  const _MainScroll({Key key, this.habitaciones, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.vertical,
      
      child: Habitaciones(habitaciones: habitaciones,hotel:hotel),
    );
  }
}

class Habitaciones extends StatelessWidget {
  final List<Habitacion> habitaciones;
  final Hotel hotel;

  const Habitaciones({Key key, this.habitaciones, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: habitaciones.length,
      itemBuilder: (BuildContext context, int index) {
        return _CardHabitacion(
          habitacion: habitaciones[index],
          hotel:hotel
        );
      },
    );
  }
}

class _CardHabitacion extends StatelessWidget {
  final Habitacion habitacion;
  final Hotel hotel;

  const _CardHabitacion({Key key, this.habitacion, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _Carousel(habitacion: habitacion),
          _Informacion(habitacion: habitacion,hotel: hotel,),
        ],
      ),
    );
  }
}

class _Carousel extends StatelessWidget {
  final Habitacion habitacion;

  const _Carousel({Key key, this.habitacion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
          height: 200,
          initialPage: 0,
          scrollPhysics: BouncingScrollPhysics(),
          enlargeCenterPage: true,
          onPageChanged: (int index, CarouselPageChangedReason params) {
            Provider.of<_ModelPage>(context).index = index;
          }),
      items: habitacion.imagenes.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return GestureDetector(
              onLongPress: () {
                mostrarImagenCreditos(context, imgUrl);
              },
              child: Container(
                width: size.width,
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: FadeInImage(
                    image: CachedNetworkImageProvider(imgUrl),
                    fit: BoxFit.cover,
                    placeholder: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Future<void> mostrarImagenCreditos(
      BuildContext context, String imgUrl) async {
    // final size = MediaQuery.of(context).size;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          final size = MediaQuery.of(context).size;
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                height: 320,
                width: size.width,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: size.width,
                      height: 250,
                      // margin: EdgeInsets.only(top: 5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: FadeInImage(
                          image: CachedNetworkImageProvider(imgUrl),
                          fit: BoxFit.cover,
                          // width: 350,
                          placeholder: AssetImage('assets/img/loading.gif'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Container(
                        // height: 150,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Creditos  '),
                            SizedBox(
                              width: 20,
                            ),
                            FadeInImage(
                                fit: BoxFit.contain,
                                width: size.width * 0.2,
                                placeholder:
                                    AssetImage('assets/img/loading.gif'),
                                image: AssetImage(
                                    'assets/img/weekendSantander.png')),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}

class _Informacion extends StatelessWidget {
  final Habitacion habitacion;
  final Hotel hotel;
  const _Informacion({Key key, this.habitacion, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var provider = Provider.of<_ModelPage>(context);

    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Text(
                    "${habitacion.tipo}",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 28.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(25.0),
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  //width: double.infinity,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                    color: Colors.green,
                                    textColor: Colors.white,
                                    child: Text(
                                      "    Reservar    ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal),
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 16.0,
                                      horizontal: 32.0,
                                    ),
                                    onPressed: () {
                                      Navigator.pushNamed(context, 'reservaHabitacion',arguments: ArgumentsReserva(habitacion, hotel));
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "\$ ${habitacion.precio}",
                                style: TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0),
                              ),
                              Text(
                                "/ por noche",
                                style: TextStyle(
                                    fontSize: 12.0, color: Colors.grey),
                              )
                            ],
                          )
                        ],
                      ),
                      const SizedBox(height: 25.0),
                      Text(
                        "Descripción".toUpperCase(),
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 14.0),
                      ),
                      const SizedBox(height: 10.0),
                      Text(
                        "${habitacion.descripcion}",
                        textAlign: TextAlign.justify,
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _ModelPage with ChangeNotifier {
  int _index = 0;
  int _indexScrollTabBar = 0;
  bool _comentar = false;

  int get index => this._index;
  int get indexScrollTabBar => this._indexScrollTabBar;
  bool get mostrarCajaComentario => this._comentar;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  set indexScrollTabBar(int value) {
    _indexScrollTabBar = value;
    notifyListeners();
  }

  set mostrarCajaComentario(bool value) {
    this._comentar = value;
    notifyListeners();
  }
}
