import 'package:app/models/hotel_model.dart';
import 'package:app/models/plan_model.dart';
import 'package:app/models/restaurante_model.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/providers/restaurantes_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:flutter/material.dart';

class FavoritosPage extends StatelessWidget {
  const FavoritosPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Tus Favoritos'),
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: _MainPage(),
          ),
        ),
      ),
    );
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.1,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainPage extends StatelessWidget {
  const _MainPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          FutureBuilder(
            future: UsuarioInstancia().getListPlanesFavoritos(),
            builder:
                (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length == 0) {
                  return Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Planes favortios',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 23,
                                fontFamily: 'BalooChettan',
                                fontWeight: FontWeight.w700),
                          ),
                          Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),
                        ],
                      ),
                      Center(
                        child: Text('Aún no tienes planes favoritos guardados'),
                      ),
                    ],
                  );
                } else {
                  return ListaFavoritosPlanes(favoritos: snapshot.data);
                }
              } else {
                return _Loading();
              }
            },
          ),
          SizedBox(
            height: 20,
          ),
          FutureBuilder(
            future: UsuarioInstancia().getListHotelesFavoritos(),
            builder:
                (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length == 0) {
                  return Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Hoteles favortios',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 23,
                                fontFamily: 'BalooChettan',
                                fontWeight: FontWeight.w700),
                          ),
                          Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),
                        ],
                      ),
                      Center(
                        child:
                            Text('Aún no tienes hoteles favoritos guardados'),
                      ),
                    ],
                  );
                } else {
                  return ListaFavoritosHoteles(favoritos: snapshot.data);
                }
              } else {
                return _Loading();
              }
            },
          ),
          SizedBox(
            height: 20,
          ),
          FutureBuilder(
            future: UsuarioInstancia().getListRestaurantesFavoritos(),
            builder:
                (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length == 0) {
                  return Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Restaurantes favortios',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 23,
                                fontFamily: 'BalooChettan',
                                fontWeight: FontWeight.w700),
                          ),
                          Icon(
                            Icons.favorite,
                            color: Colors.red,
                          ),
                        ],
                      ),
                      Center(
                        child: Text(
                            'Aún no tienes restaurantes favoritos guardados'),
                      ),
                    ],
                  );
                } else {
                  return ListaFavoritosRestaurantes(favoritos: snapshot.data);
                }
              } else {
                return _Loading();
              }
            },
          ),
        ],
      ),
    );
  }
}

class ListaFavoritosPlanes extends StatelessWidget {
  final List<String> favoritos;
  const ListaFavoritosPlanes({Key key, this.favoritos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Planes favortios',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 23,
                  fontFamily: 'BalooChettan',
                  fontWeight: FontWeight.w700),
            ),
            Icon(
              Icons.favorite,
              color: Colors.red,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          child: ListView.builder(
            itemCount: favoritos.length,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return PlanFavorito(id: favoritos[index]);
            },
          ),
        ),
      ],
    );
  }
}

class PlanFavorito extends StatelessWidget {
  final String id;
  const PlanFavorito({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: PlanesProvider().getPlan(id),
      builder: (BuildContext context, AsyncSnapshot<Plan> snapshot) {
        if (snapshot.hasData) {
          return Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: FadeInImage(
                        width: size.width * 0.45,
                        placeholder: AssetImage('assets/img/loading.gif'),
                        image: CachedNetworkImageProvider(snapshot.data.imagenes[0])),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        '${snapshot.data.nombre}',
                        style: TextStyle(
                            fontSize: 19, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Colors.green,
                              textColor: Colors.white,
                              child: Text('Ver plan'),
                              onPressed: () {
                                Navigator.pushNamed(context, 'planTuristico',
                                    arguments: snapshot.data);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        } else {
          return _Loading();
        }
      },
    );
  }
}

class ListaFavoritosHoteles extends StatelessWidget {
  final List<String> favoritos;
  const ListaFavoritosHoteles({Key key, this.favoritos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Hoteles favortios',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 23,
                  fontFamily: 'BalooChettan',
                  fontWeight: FontWeight.w700),
            ),
            Icon(
              Icons.favorite,
              color: Colors.red,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          child: ListView.builder(
            itemCount: favoritos.length,
            shrinkWrap: true,
            // scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return HotelFavorito(id: favoritos[index]);
            },
          ),
        ),
      ],
    );
  }
}

class HotelFavorito extends StatelessWidget {
  final String id;
  const HotelFavorito({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: HotelesProvider().getHotel(id),
      builder: (BuildContext context, AsyncSnapshot<Hotel> snapshot) {
        if (snapshot.hasData) {
          return Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: FadeInImage(
                        width: size.width * 0.45,
                        placeholder: AssetImage('assets/img/loading.gif'),
                        image: CachedNetworkImageProvider(snapshot.data.imagenes[0])),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        '${snapshot.data.nombre}',
                        style: TextStyle(
                            fontSize: 19, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Colors.green,
                              textColor: Colors.white,
                              child: Text('Ver hotel'),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, 'descripcion_hotel',
                                    arguments: snapshot.data);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        } else {
          return _Loading();
        }
      },
    );
  }
}

class ListaFavoritosRestaurantes extends StatelessWidget {
  final List<String> favoritos;
  const ListaFavoritosRestaurantes({Key key, this.favoritos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Restaurantes favortios',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 23,
                  fontFamily: 'BalooChettan',
                  fontWeight: FontWeight.w700),
            ),
            Icon(
              Icons.favorite,
              color: Colors.red,
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          child: ListView.builder(
            itemCount: favoritos.length,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return RestauranteFavorito(id: favoritos[index]);
            },
          ),
        ),
      ],
    );
  }
}

class RestauranteFavorito extends StatelessWidget {
  final String id;
  const RestauranteFavorito({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder(
      future: RestauranteProvider().getRestaurante(id),
      builder: (BuildContext context, AsyncSnapshot<Restaurante> snapshot) {
        if (snapshot.hasData) {
          return Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: FadeInImage(
                        width: size.width * 0.45,
                        placeholder: AssetImage('assets/img/loading.gif'),
                        image: CachedNetworkImageProvider(snapshot.data.imagenes[0])),
                  ),
                ],
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        '${snapshot.data.nombre}',
                        style: TextStyle(
                            fontSize: 19, fontWeight: FontWeight.w500),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15)),
                              color: Colors.green,
                              textColor: Colors.white,
                              child: Text('Ver Resturante'),
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, 'descripcion_restaurante',
                                    arguments: snapshot.data);
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          );
        } else {
          return _Loading();
        }
      },
    );
  }
}
