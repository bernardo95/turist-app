import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class TerminosCondicionesPage extends StatelessWidget {
  const TerminosCondicionesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Terminos y Condiciones'),
        ),
        body: MainView(),
      ),
    );
  }
}

class MainView extends StatelessWidget {
  final String terminos;
  const MainView({Key key, this.terminos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadAsset(context),
      builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasData) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    snapshot.data,
                    style: TextStyle(fontSize: 18),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Future<String> loadAsset(BuildContext context) async {
    return await DefaultAssetBundle.of(context)
        .loadString('assets/res/termino.txt');
  }
}
