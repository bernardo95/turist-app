import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/models/reserva_habitacion_model.dart';
import 'package:app/pages/habitacion/habitacion_page.dart';
import 'package:app/providers/habitacion_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ReservaRestaurantePage extends StatelessWidget {
  final ArgumentsReserva args;
  const ReservaRestaurantePage({Key key, this.args}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context);
      },
        child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Reserva tu plan'),
          leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
            Navigator.pop(context);
          }),
        ),
        body: SingleChildScrollView(
          child: SafeArea(
            child: FutureBuilder(
              future: UsuarioInstancia().getId(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return _Main(
                      hotel: args.hotel,
                      habitacion: args.habitacion,
                      usuarioId: snapshot.data);
                } else {
                  return Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: size.height * 0.4,
                        ),
                        Text('Inicia sesión primero'),
                      ],
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _Main extends StatefulWidget {
  final Hotel hotel;
  final Habitacion habitacion;
  final String usuarioId;

  _Main({Key key, this.hotel, this.habitacion, this.usuarioId})
      : super(key: key);

  @override
  __MainState createState() => __MainState();
}

class __MainState extends State<_Main> {
  String _fechaInicio = '';
  String _fechaFin = '';
  DateTime _dateTimeInit;
  DateTime _dateTimeEnd;
  int _personas = 0;
  int _habitaciones = 0;
  int _noches = 0;

  @override
  Widget build(BuildContext context) {
    int _precio = widget.habitacion.precio;
    
    final size = MediaQuery.of(context).size;

    // GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Column(
              children: <Widget>[
                Text(
                  'Habitación ${widget.habitacion.tipo}',
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                
                  children: <Widget>[
                    Expanded(
                                          child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: FadeInImage(
                                // width: size.width*0.5,
                                height: 200,
                                fit: BoxFit.cover,
                                placeholder: AssetImage('assets/img/loading.gif'),
                                image:
                                    CachedNetworkImageProvider(widget.habitacion.imagenes[0])),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                    child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                  child: Text(
                    'Programa tu estadia',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  ),
                ))
              ],
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Colors.blue[400],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.calendar_today),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Fecha de entrada',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                    onPressed: () {
                      selectDateInicio(context);
                    }),
                RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Colors.blue[400],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Icon(Icons.calendar_today),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Fecha de salida',
                          style: TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    ),
                    onPressed: () {
                      selectDateFin(context,_fechaInicio);
                    }),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Wrap(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  'Fecha inicio: $_fechaInicio',
                  style: TextStyle(fontSize: 17),
                ),
                Divider(),
                Text('Fecha fin: $_fechaFin', style: TextStyle(fontSize: 17)),
              ],
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            if (noches().length!=0)
            Row(
              children: <Widget>[
                Text('Estancia por ${noches()} noches',
                    style: TextStyle(fontSize: 18))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Número de personas por habitación: ',
                    style: TextStyle(fontSize: 18)),
                Divider(),
                Text('${widget.habitacion.personas}',
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                Divider()
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Numero de personas: ', style: TextStyle(fontSize: 18)),
                Spacer(),
                IconButton(
                    color: Colors.red[200],
                    icon: Icon(Icons.remove_circle_outline),
                    onPressed: () {
                      setState(() {
                        if (_personas > 0) {
                          _personas = _personas - 1;
                        }
                      });
                    }),
                Spacer(),
                Text(
                  '$_personas',
                  style: TextStyle(fontSize: 22),
                ),
                Spacer(),
                IconButton(
                    color: Colors.green[200],
                    icon: Icon(Icons.add_circle_outline),
                    onPressed: () {
                      setState(() {
                        if (_personas==0 && _habitaciones==0) {
                        _personas = _personas + 1;
                        _habitaciones = _habitaciones + 1;
                        }else{
                        _personas = _personas + 1;
                        }
                        if (calcularAumentoHabitaciones()) {
                          _habitaciones = _habitaciones + 1;
                        }
                      });
                    }),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Numero de habitaciones: ',
                    style: TextStyle(fontSize: 18)),
                Spacer(),
                IconButton(
                    color: Colors.red[200],
                    icon: Icon(Icons.remove_circle_outline),
                    onPressed: () {
                      setState(() {
                        if (_habitaciones > 0 &&
                            calcularDismunucionHabitaciones()) {
                          _habitaciones = _habitaciones - 1;
                        }
                      });
                    }),
                Spacer(),
                Text(
                  '$_habitaciones',
                  style: TextStyle(fontSize: 22),
                ),
                Spacer(),
                IconButton(
                    color: Colors.green[200],
                    icon: Icon(Icons.add_circle_outline),
                    onPressed: () {
                      setState(() {
                        _habitaciones = _habitaciones + 1;
                      });
                    }),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: <Widget>[
                Text('Precio: ', style: TextStyle(fontSize: 20)),
                Spacer(),
                Text('\$${_precio * _habitaciones*_noches}',
                    style: TextStyle(fontSize: 22)),
                SizedBox(
                  width: 35,
                )
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    if (!Provider.of<HabitacionesProvider>(context).creando)
                    SizedBox(
                      width: size.width * 0.8,
                      child: RaisedButton(
                          color: Colors.green,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 16.0,
                              horizontal: 15.0,
                            ),
                            child: Text(
                              'Reservar ahora',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                          onPressed: () {
                            _submit();
                          }),
                    ),
                    if (Provider.of<HabitacionesProvider>(context).creando)
                    Container(
                      child: CircularProgressIndicator(),
                    )
                  ],
                )
              ],
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      ),
    );
  }

  selectDateInicio(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day),
      lastDate: DateTime(DateTime.now().year, DateTime.now().month + 6),
      locale: Locale('es'),
      builder: (BuildContext context, Widget child) {
        return child;
      },
    );

    if (picked != null) {
      setState(() {
        _fechaInicio = picked.toString().split(' ')[0];
        _dateTimeInit = picked;
      });
    }
  }

  String noches() {
    String noches;
    try {
      final Duration diferencia = _dateTimeEnd.difference(_dateTimeInit);
      noches =  diferencia.inDays.toString();
      _noches = int.tryParse(noches);
    } catch (e) {
      noches = '';
      _noches = 0;
    }
    // _noches = int.tryParse(noches);
    return noches;
  }

  selectDateFin(BuildContext context,String _fechaInicio) async {
        final DateTime picked = await showDatePicker(
          context: context,
          initialDate: DateTime( _dateTimeInit.year,_dateTimeInit.month, _dateTimeInit.day+1),
      firstDate: DateTime(
          _dateTimeInit.year,_dateTimeInit.month, _dateTimeInit.day+1),
      lastDate: DateTime(DateTime.now().year + 1, DateTime.now().month),
      locale: Locale('es'),
      builder: (BuildContext context, Widget child) {
        return child;
      },
    );

    if (picked != null) {
      setState(() {
        _fechaFin = picked.toString().split(' ')[0];
        _dateTimeEnd = picked;
      });
    }
  }

  bool calcularAumentoHabitaciones() {
    bool aumentar;
    String division = (_personas / widget.habitacion.personas).toString();
    String parteEntera = division.substring(0, 1);
    int residuo = (_personas % widget.habitacion.personas);
    int habitaciones = int.parse(parteEntera);
    if (residuo > 0) {
      habitaciones++;
    }
    if (habitaciones > _habitaciones) {
      aumentar = true;
    } else {
      aumentar = false;
    }
    return aumentar;
  }

  bool calcularDismunucionHabitaciones() {
    bool disminuir;
    String division = (_personas / widget.habitacion.personas).toString();
    String parteEntera = division.substring(0, 1);
    int residuo = (_personas % widget.habitacion.personas);
    int habitaciones = int.parse(parteEntera);
    if (residuo > 0) {
      habitaciones++;
    }
    if (habitaciones < _habitaciones) {
      disminuir = true;
    } else {
      disminuir = false;
    }
    return disminuir;
  }

  Future<void> _submit() async {
    if (_fechaFin == '' || _fechaInicio == '' || _habitaciones == 0 || _personas==0) {
      mensaje();
    } else {
      bool confirmar = await mensajeAdvertencia(context);
      if (confirmar) {
          Provider.of<HabitacionesProvider>(context, listen: false).creando = true;

        final precio = widget.habitacion.precio * _habitaciones*_noches;
        final reserva = new ReservaHabitacion(idHabitacion: widget.habitacion.id,idHotel: widget.hotel.id,idUsuario: widget.usuarioId,personas: _personas,habitaciones: _habitaciones,precio: precio,fechaInicio: _fechaInicio,fechaFin: _fechaFin);
        await HabitacionesProvider().reservarHabitacion(reserva, widget.hotel, widget.habitacion, widget.usuarioId);
        Provider.of<HabitacionesProvider>(context, listen: false).creando = false;
        await mensajeCompletado();
        Navigator.pop(context);
      }
    }
  }

  Future<void> mensaje() async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        useRootNavigator: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text(
              'La reserva no esta completa',
              style: TextStyle(fontSize: 17),
            ),
            content: Text(
                'Recuerda debes seleccionar una fecha de ingreso y salida y el número de personas'),
          );
        });
  }

  Future<bool> mensajeAdvertencia(BuildContext context) async {
    bool confirmar = false;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('¿Desea realizar la reserva?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('No')),
              FlatButton(
                  onPressed: () {
                    confirmar = true;
                    Navigator.of(context).pop();
                  },
                  child: Text('Si'))
            ],
          );
        });
    return confirmar;
  }

  Future<void> mensajeCompletado() async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text(
              'Reserva realizada',
              style: TextStyle(fontSize: 17),
            ),
            content:
                Text('Puedes ver tus reservas en el historial de tu cuenta'),
          );
        });
  }
}
