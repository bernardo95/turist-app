import 'package:app/models/plan_model.dart';
import 'package:app/models/reserva_plan_model.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:flutter/material.dart';
import 'package:app/utils/validators.dart' as utils;
import 'package:provider/provider.dart';

class ReservaPlanPage extends StatelessWidget {
  final Plan plan;
  const ReservaPlanPage({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Reserva tu plan'),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          future: UsuarioInstancia().getId(),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return _Main(plan: plan, usuarioId: snapshot.data);
            } else {
              return Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: size.height * 0.4,
                    ),
                    Text('Inicia sesión primero'),
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }
}

class _Main extends StatefulWidget {
  final Plan plan;
  final String usuarioId;
  const _Main({Key key, this.plan, this.usuarioId}) : super(key: key);

  @override
  __MainState createState() => __MainState();
}

class __MainState extends State<_Main> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _controllerNumeroTelefono = TextEditingController();
  String _fecha = '';
  int _personas = 0;
  @override
  Widget build(BuildContext context) {
    int _precio = widget.plan.precio;
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Colors.blue[400],
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Programa tu salida',
                        style: TextStyle(color: Colors.white, fontSize: 17),
                      ),
                    ),
                    onPressed: () {
                      selectDate(context);
                    }),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Fecha Programada: $_fecha',
                  style: TextStyle(fontSize: 19),
                ),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text('Numero de personas: ', style: TextStyle(fontSize: 19)),
                SizedBox(
                  width: 15,
                ),
                IconButton(
                    color: Colors.red[200],
                    icon: Icon(Icons.remove_circle_outline),
                    onPressed: () {
                      setState(() {
                        if (_personas > 0) {
                          _personas = _personas - 1;
                        }
                      });
                    }),
                Spacer(),
                Text(
                  '$_personas',
                  style: TextStyle(fontSize: 22),
                ),
                Spacer(),
                IconButton(
                    color: Colors.green[200],
                    icon: Icon(Icons.add_circle_outline),
                    onPressed: () {
                      setState(() {
                        _personas = _personas + 1;
                      });
                    }),
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              children: <Widget>[
                Text('Valor: ', style: TextStyle(fontSize: 22)),
                Spacer(),
                Text('\$${_precio * _personas}',
                    style: TextStyle(fontSize: 22)),
                SizedBox(
                  width: 35,
                )
              ],
            ),
            SizedBox(
              height: 35,
            ),
            Form(
              key: _formKey,
              child: SizedBox(
                width: size.width * 0.6,
                child: TextFormField(
                  controller: _controllerNumeroTelefono,
                  style: TextStyle(
                    fontSize: 19.0,
                  ),
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Numero de celular',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {},
                  validator: (value) {
                    if (utils.numeroTelefonoValido(value)) {
                      return null;
                    } else {
                      return 'El numero no es valido';
                    }
                  },
                ),
              ),
            ),
            Text('(Opcional)'),
            SizedBox(
              height: 15,
            ),
            Text(
              'Si deseas que te contacten vian WhatsApp puedes ingresar tu número',
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    if (!Provider.of<PlanesProvider>(context).creando)
                      SizedBox(
                        width: size.width * 0.8,
                        child: RaisedButton(
                            color: Colors.green,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 16.0,
                                horizontal: 15.0,
                              ),
                              child: Text(
                                'Haz tu reserva ahora, es gratis!',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                            onPressed: () {
                              _submit();
                            }),
                      ),
                    if (Provider.of<PlanesProvider>(context).creando)
                      Container(
                        child: CircularProgressIndicator(),
                      )
                  ],
                )
              ],
            ),
            SizedBox(
              height: 50,
            ),
            Text(
                'Los precios y valor de los servicios adicionales serán cotizados posteriormente con la entidad privada a la cual pertenece este plan turistico, serás contactado via correo electronico o whatsapp si lo deseas')
          ],
        ),
      ),
    );
  }

  selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day+1),
      firstDate: DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day+1),
      lastDate: DateTime(DateTime.now().year, DateTime.now().month + 6),
      locale: Locale('es'),
      builder: (BuildContext context, Widget child) {
        return child;
      },
    );

    if (picked != null) {
      setState(() {
        _fecha = picked.toString().split(' ')[0];
      });
    }
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) return;
    if (_formKey.currentState.validate()) {
      if (_personas == 0 || _fecha == '') {
        mensaje();
      } else {
        bool confirmar = await mensajeAdvertencia(context);
        if (confirmar) {
          Provider.of<PlanesProvider>(context, listen: false).creando = true;
          int numero;
          final precio = widget.plan.precio * _personas;
          final reserva = new ReservaPlan(
              fecha: _fecha,
              idPlan: widget.plan.id,
              idUsuario: widget.usuarioId,
              precio: precio,
              personas: _personas);
          try {
            numero = num.parse(_controllerNumeroTelefono.text);
          } catch (e) {
            numero = 0;
          }
          await PlanesProvider()
              .reservarPlan(reserva, widget.plan, widget.usuarioId, numero);
          Provider.of<PlanesProvider>(context, listen: false).creando = false;
          await mensajeCompletado();
          Navigator.pop(context);
        }
      }
    }
  }

  Future<void> mensaje() async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        useRootNavigator: true,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text(
              'La reserva no esta completa',
              style: TextStyle(fontSize: 17),
            ),
            content: Text(
                'Recuerda debes seleccionar una fecha y el número de personas'),
          );
        });
  }

  Future<void> mensajeCompletado() async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text(
              'Reserva realizada',
              style: TextStyle(fontSize: 17),
            ),
            content:
                Text('Puedes ver tus reservas en el historial de tu cuenta'),
          );
        });
  }

  Future<bool> mensajeAdvertencia(BuildContext context) async {
    bool confirmar = false;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('¿Desea realizar la reserva?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('No')),
              FlatButton(
                  onPressed: () {
                    confirmar = true;
                    Navigator.of(context).pop();
                  },
                  child: Text('Si'))
            ],
          );
        });
    return confirmar;
  }
}
