import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/models/plan_model.dart';
import 'package:app/models/reserva_habitacion_model.dart';
import 'package:app/models/reserva_plan_model.dart';
import 'package:app/providers/habitacion_provider.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HistorialReservasPage extends StatelessWidget {
  final String idUsuario;
  const HistorialReservasPage({Key key, this.idUsuario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Tus reservas'),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: _MainPage(idUsuario: idUsuario),
        ),
      ),
    );
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.1,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainPage extends StatelessWidget {
  final String idUsuario;

  const _MainPage({Key key, this.idUsuario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          if (Provider.of<PlanesProvider>(context).recargar)
            FutureBuilder(
              future: PlanesProvider().getReservasPlanesByUser(idUsuario),
              builder: (BuildContext context,
                  AsyncSnapshot<List<ReservaPlan>> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return Center(
                      child: Text('Aún no tienes reservas'),
                    );
                  } else {
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      physics: BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListarReservaPlanes(
                          reservaPlan: snapshot.data[index],
                        );
                      },
                    );
                  }
                } else {
                  return _Loading();
                }
              },
            ),
          if (Provider.of<HabitacionesProvider>(context).recargar)
            FutureBuilder(
              future: HabitacionesProvider()
                  .getReservasHabitacionesByUser(idUsuario),
              builder: (BuildContext context,
                  AsyncSnapshot<List<ReservaHabitacion>> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length==0) {
                    return Center(
                    child: Text('Aún no tienes reservas'),
                  );
                  } else {
                    return ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListarReservaHabitaciones(
                          reservaHabitacion: snapshot.data[index]);
                    },
                  );
                  }
                  
                } else {
                  return _Loading();
                }
              },
            )
        ],
      ),
    );
  }
}

class ListarReservaPlanes extends StatelessWidget {
  final ReservaPlan reservaPlan;
  const ListarReservaPlanes({Key key, this.reservaPlan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w400);
    return FutureBuilder(
      future: PlanesProvider().getPlan(reservaPlan.idPlan),
      builder: (BuildContext context, AsyncSnapshot<Plan> snapshot) {
        if (snapshot.hasData) {
          
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Container(
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        snapshot.data.nombre,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      if (reservaPlan.estado == 'ACTIVO')
                        GestureDetector(
                            onTap: () async {
                              bool confirmar =
                                  await mensajeAdvertencia(context);
                              if (confirmar) {
                                Provider.of<PlanesProvider>(context,
                                        listen: false)
                                    .creando = true;
                                await PlanesProvider()
                                    .cancelarReservaPlan(reservaPlan.id);
                                Provider.of<PlanesProvider>(context,
                                        listen: false)
                                    .creando = false;
                                Provider.of<PlanesProvider>(context,
                                        listen: false)
                                    .recargar = false;
                                Provider.of<PlanesProvider>(context,
                                        listen: false)
                                    .recargar = true;

                                await mensajeCompletado(context);
                              }
                            },
                            child: Column(
                              children: <Widget>[
                                if (Provider.of<PlanesProvider>(context)
                                    .creando)
                                  CircularProgressIndicator(),
                                Text(
                                  'Cancelar',
                                  style: TextStyle(color: Colors.blue),
                                ),
                              ],
                            )),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Fecha ' + reservaPlan.fecha,
                        style: textStyle,
                      ),
                      Text(
                        'Estado ' + reservaPlan.estado,
                        style: textStyle,
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Número de personas ' + reservaPlan.personas.toString(),
                        style: textStyle,
                      ),
                      Text(
                        'Precio ' + reservaPlan.precio.toString(),
                        style: textStyle,
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        } else {
          return _Loading();
        }
      },
    );
  }
}

Future<bool> mensajeAdvertencia(BuildContext context) async {
  bool confirmar = false;
  await showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: Text('¿Desea cancelar la reserva?'),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('No')),
            FlatButton(
                onPressed: () {
                  confirmar = true;
                  Navigator.of(context).pop();
                },
                child: Text('Si'))
          ],
        );
      });
  return confirmar;
}

Future<void> mensajeCompletado(BuildContext context) async {
  await showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          title: Text(
            'Reserva cancelada',
            style: TextStyle(fontSize: 17),
          ),
          content: Text(
              'La reserva aun sigue conservando su historial en tu cuenta'),
        );
      });
}

class ListarReservaHabitaciones extends StatelessWidget {
  final ReservaHabitacion reservaHabitacion;
  const ListarReservaHabitaciones({Key key, this.reservaHabitacion})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(fontSize: 17, fontWeight: FontWeight.w400);

    return FutureBuilder(
      future: HotelesProvider().getHotel(reservaHabitacion.idHotel),
      builder: (BuildContext context, AsyncSnapshot<Hotel> snapshot) {
        if (snapshot.hasData) {
          final hotel = snapshot.data;
          return FutureBuilder(
            future: HabitacionesProvider()
                .getHabitacion(reservaHabitacion.idHabitacion),
            builder:
                (BuildContext context, AsyncSnapshot<Habitacion> snapshot) {
              if (snapshot.hasData) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              hotel.nombre,
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w600),
                            ),
                            if (reservaHabitacion.estado == 'ACTIVO')
                              GestureDetector(
                                  onTap: () async {
                                    bool confirmar =
                                        await mensajeAdvertencia(context);
                                    if (confirmar) {
                                      Provider.of<HabitacionesProvider>(context,
                                              listen: false)
                                          .creando = true;
                                      await HabitacionesProvider()
                                          .cancelarReservaHabitacion(
                                              reservaHabitacion.id);
                                      Provider.of<HabitacionesProvider>(context,
                                              listen: false)
                                          .creando = false;
                                      Provider.of<HabitacionesProvider>(context,
                                              listen: false)
                                          .recargar = false;
                                      Provider.of<HabitacionesProvider>(context,
                                              listen: false)
                                          .recargar = true;

                                      await mensajeCompletado(context);
                                    }
                                  },
                                  child: Column(
                                    children: <Widget>[
                                      if (Provider.of<HabitacionesProvider>(
                                              context)
                                          .creando)
                                        CircularProgressIndicator(),
                                      Text(
                                        'Cancelar',
                                        style: TextStyle(color: Colors.blue),
                                      ),
                                    ],
                                  )),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Habitación ' + snapshot.data.tipo,
                              style: textStyle,
                            ),
                            Text(
                              'Estado ' + reservaHabitacion.estado,
                              style: textStyle,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Fecha de entrada: ' +
                                  reservaHabitacion.fechaInicio,
                              style: textStyle,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              'Fecha de salida: ' + reservaHabitacion.fechaFin,
                              style: textStyle,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Habitaciones reservadas: ' +
                                  reservaHabitacion.habitaciones.toString(),
                              style: textStyle,
                            ),
                            Text(
                              'Precio ' + reservaHabitacion.precio.toString(),
                              style: textStyle,
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              'Número de personas: ' +
                                  reservaHabitacion.personas.toString(),
                              style: textStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return _Loading();
              }
            },
          );
        } else {
          return _Loading();
        }
      },
    );
  }
}
