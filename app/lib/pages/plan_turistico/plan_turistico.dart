import 'package:app/models/plan_model.dart';
import 'package:app/pages/comentario/crear_comentario.dart';
import 'package:app/pages/comentario/listar_comentarios.dart';
import 'package:app/pages/comentario/placeholder.dart';
import 'package:app/pages/map/map_page.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;
import 'package:cached_network_image/cached_network_image.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class PlanTuristicoPage extends StatelessWidget {
  final Plan plan;
  const PlanTuristicoPage({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Provider.of<PlanesProvider>(context, listen: false).recargar = false;
        Provider.of<PlanesProvider>(context, listen: false).recargar = true;
        Navigator.pushNamedAndRemoveUntil(context, 'app',ModalRoute.withName('/app'));
      },
      child: Scaffold(
        body: SafeArea(
          child: ChangeNotifierProvider(
              create: (_) => _ModelPage(), child: _MainScroll(plan: plan)),
        ),
      ),
    );
  }
}
class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}
class _MainScroll extends StatelessWidget {
  final Plan plan;
  const _MainScroll({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Provider.of<PlanesProvider>(context, listen: false).recargar =
                    false;
                Provider.of<PlanesProvider>(context, listen: false).recargar =
                    true;
                Navigator.of(context).pop();
              }),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          _Carousel(plan: plan),
          _MainView(plan: plan),
        ])),
      ],
    );
  }
}

class _Carousel extends StatelessWidget {
  final Plan plan;

  const _Carousel({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
          height: 200,
          initialPage: 0,
          scrollPhysics: BouncingScrollPhysics(),
          enlargeCenterPage: true,
          onPageChanged: (int index, CarouselPageChangedReason params) {
            Provider.of<_ModelPage>(context,listen: false).index = index;
          }),
      items: plan.imagenes.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return GestureDetector(
              onLongPress: () {
                mostrarImagenCreditos(context, imgUrl);
              },
              child: Container(
                width: size.width,
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: FadeInImage(
                    image: CachedNetworkImageProvider(imgUrl),
                    fit: BoxFit.cover,
                    placeholder: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Future<void> mostrarImagenCreditos(
      BuildContext context, String imgUrl) async {
    // final size = MediaQuery.of(context).size;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          final size = MediaQuery.of(context).size;
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                height: 320,
                width: size.width,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: size.width,
                      height: 250,
                      // margin: EdgeInsets.only(top: 5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: FadeInImage(
                          image: CachedNetworkImageProvider(imgUrl),
                          fit: BoxFit.cover,
                          // width: 350,
                          placeholder: AssetImage('assets/img/loading.gif'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Container(
                        // height: 150,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Creditos  '),
                            SizedBox(
                              width: 20,
                            ),
                            FadeInImage(
                                fit: BoxFit.contain,
                                width: size.width * 0.2,
                                placeholder:
                                    AssetImage('assets/img/loading.gif'),
                                image: AssetImage(
                                    'assets/img/weekendSantander.png')),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}

class _MainView extends StatefulWidget {
  final Plan plan;

  const _MainView({Key key, this.plan}) : super(key: key);

  @override
  __MainViewState createState() => __MainViewState();
}

class __MainViewState extends State<_MainView>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text(
        'Informacion',
      ),
    ),
    Tab(
      child: Text(
        'Ubicacion',
      ),
    ),
    Tab(
      child: Text(
        'Calificacion',
      ),
    )
  ];
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    _tabController.addListener(() {
      Provider.of<_ModelPage>(context, listen: false).indexScrollTabBar =
          _tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    int index = Provider.of<_ModelPage>(context).indexScrollTabBar;
    return Column(children: <Widget>[
      TabBar(
        indicatorColor: Colors.green,
        indicatorWeight: 2,
        labelColor: Colors.black,
        labelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.normal, fontSize: 13),
        controller: _tabController,
        tabs: myTabs,
      ),
      ListView(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          if (index == 0) _Informacion(plan: widget.plan),
          if (index == 1) _Ubicacion(plan: widget.plan),
          if (index == 2)
            _Calificacion(
              plan: widget.plan,
            )
        ],
      )
    ]);
  }
}

// ignore: must_be_immutable
class _Informacion extends StatelessWidget {
  Plan plan;
  _Informacion({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<_ModelPage>(context);

    final size = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        if (Provider.of<PlanesProvider>(context).recargar)
          FutureBuilder(
            future: PlanesProvider().getPlan(plan.id),
            builder: (BuildContext context, AsyncSnapshot<Plan> snapshot) {
              if (snapshot.hasData) {
                plan = snapshot.data;
                return Container(
                  width: size.width,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    child: Column(
                      children: <Widget>[
                        _Titulo(plan: plan),
                        InformacionGeneralTitulo(
                          plan: plan,
                        ),
                        if (provider.mostrarInformacionGeneral)
                          InformacionGeneralContenido(plan: plan),
                        DescripcionTitulo(),
                        if (provider.mostrarDescripcion)
                          DescripcionContenido(plan: plan),
                        ServiciosTitulo(),
                        if (provider.mostrarServicios)
                          ServiciosContenido(
                            plan: plan,
                          ),
                        TarifaTitulo(),
                        if (provider.mostrarTarifa)
                          TarifaContenido(
                            plan: plan,
                          ),
                        ServiciosAdicionalesTitulo(),
                        if (provider.mostrarServiciosAdicional)
                          ServiciosAdicionalesContenido(
                            plan: plan,
                          ),
                        ItinerariosTitulo(),
                        if (provider.mostrarItinerario)
                        ItinerarioContenido(plan: plan,),
                        RecomendacionesTitulo(),
                        if (provider.mostrarRecomendaciones)
                          RecomendacionesContenido(
                            plan: plan,
                          ),
                        ObervacionesTitulo(),
                        if (provider.mostrarObservaciones)
                          ObervacionesContenido(
                            plan: plan,
                          ),
                        TerminosCondiciones(plan: plan)
                      ],
                    ),
                  ),
                );
              } else {
                return _Loading();
              }
            },
          ),
      ],
    );
  }
}

class _Titulo extends StatelessWidget {
  const _Titulo({
    Key key,
    @required this.plan,
  }) : super(key: key);

  final Plan plan;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
                child: Text(plan.nombre,
                    style:
                        TextStyle(fontSize: 28, fontWeight: FontWeight.bold))),
          ],
        ),
        SizedBox(
          height: 5,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // if(Provider.of<PlanesProvider>(context).recargar)
            Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 8.0,
                  horizontal: 16.0,
                ),
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    borderRadius: BorderRadius.circular(20.0)),
                child: FutureBuilder(
                  future: ComentariosProvider().getComentariosCount(plan.id),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      return Row(
                        children: <Widget>[
                          if(plan.calificacion!=null && plan.calificacion!=0)
                          Text(
                            '${utils.rateTransform(plan.calificacion)} / ',
                            style: TextStyle(fontSize: 16),
                          ),
                          if(plan.calificacion!=null && plan.calificacion!=0)
                          Icon(
                            Icons.star,
                            size: 16,
                            color: Colors.white,
                          ),
                          Text('${snapshot.data} Comentarios',
                              style: TextStyle(fontSize: 16)),
                        ],
                      );
                    } else {
                      return Center(
                        child: Container(
                          height: 15,
                          width: 15,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    }
                  },
                )),
            Spacer(),
            if(Provider.of<PlanesProvider>(context).recargar)
            FutureBuilder(
              future: UsuarioInstancia().isFavorito(plan.id),
              builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data) {
                  return IconButton(
                    color: Colors.red[600],
                    icon: Icon(Icons.favorite),
                    onPressed: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool(plan.id, false);
                      List<String> ids = await UsuarioInstancia().getListPlanesFavoritos();
                      ids.remove(plan.id);
                      UsuarioInstancia().setListFavoritosPlanes(ids);
                      Provider.of<PlanesProvider>(context,listen:false).recargar = false;
                      Provider.of<PlanesProvider>(context,listen:false).recargar = true;
                    },
                  );
                } else {
                  return IconButton(
                    color: Colors.grey[600],
                    icon: Icon(Icons.favorite_border),
                    onPressed: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool(plan.id, true);
                      List<String> ids = await UsuarioInstancia().getListPlanesFavoritos();
                      ids.add(plan.id);
                      UsuarioInstancia().setListFavoritosPlanes(ids);
                      Provider.of<PlanesProvider>(context,listen:false).recargar = false;
                      Provider.of<PlanesProvider>(context,listen:false).recargar = true;
                    },
                  );
                }
                } else {
                  return IconButton(
                    color: Colors.grey[600],
                    icon: Icon(Icons.favorite_border),
                    onPressed: () async {
                      SharedPreferences prefs =
                          await SharedPreferences.getInstance();
                      prefs.setBool(plan.id, true);
                      List<String> ids = await UsuarioInstancia().getListPlanesFavoritos();
                      ids.add(plan.id);
                      UsuarioInstancia().setListFavoritosPlanes(ids);
                      Provider.of<PlanesProvider>(context,listen:false).recargar = false;
                      Provider.of<PlanesProvider>(context,listen:false).recargar = true;
                    },
                  );
                }
                
              },
            )
          ],
        ),
        SizedBox(
          height: 25,
        ),
        Row(
          children: <Widget>[
            Icon(Icons.monetization_on,color: Colors.green,),
            Text('Desde \$ ${plan.precio}',style: TextStyle(fontSize: 17),)
          ],
        ),
        SizedBox(height: 25,),
        SizedBox(
          width: double.infinity,
          child: RaisedButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            color: Colors.green,
            textColor: Colors.white,
            child: Text(
              "Reservar",
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 16),
            ),
            padding: const EdgeInsets.symmetric(
              vertical: 16.0,
              horizontal: 32.0,
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'reservaPlan', arguments: plan);
            },
          ),
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }
}

class _Ubicacion extends StatelessWidget {
  final Plan plan;
  const _Ubicacion({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height * 0.56,
      child: GoogleMapPage(plan: plan),
    );
  }
}

class _Calificacion extends StatelessWidget {
  final Plan plan;
  const _Calificacion({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    UsuarioInstancia usuario = Provider.of<UsuarioInstancia>(context);
    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            FutureBuilder(
              future: usuario.getId(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return GestureDetector(
                    onTap: () {
                      Provider.of<ComentariosProvider>(context, listen: false)
                          .comentando = true;
                    },
                    child:
                        (Provider.of<ComentariosProvider>(context).comentando ==
                                true)
                            ? CrearComentario(
                                ref: plan.id,
                                idUsuario: snapshot.data,
                                tipo: "plan",
                              )
                            : PlacehorderCommentPage(),
                  );
                } else {
                  return Container();
                }
              },
            ),
            ListarComentariosPage(
              ref: plan.id,
              tipo: "plan",
            )
          ],
        ),
      ),
    );
  }
}

class InformacionGeneralTitulo extends StatelessWidget {
  final Plan plan;

  const InformacionGeneralTitulo({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarInformacionGeneral =
        Provider.of<_ModelPage>(context).mostrarInformacionGeneral;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarInformacionGeneral)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarInformacionGeneral = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Informacion general',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarInformacionGeneral)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarInformacionGeneral = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Informacion general',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                      textAlign: TextAlign.justify,
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}

class InformacionGeneralContenido extends StatelessWidget {
  final Plan plan;
  const InformacionGeneralContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.location_on,
                  color: Colors.green[200],
                ),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: Text(
                    'Ubicación: ${plan.ubicacion}',
                    style: TextStyle(fontSize: 21),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.access_time,
                  color: Colors.green[200],
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  'Duracion: ',
                  style: TextStyle(fontSize: 21),
                ),
                Expanded(
                  child: Text(
                    plan.duracion,
                    style: TextStyle(fontSize: 21),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.access_time,
                  color: Colors.green[200],
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  'Hora de inicio: ',
                  style: TextStyle(fontSize: 21),
                ),
                Expanded(
                    child: Text(
                  plan.horaInicio,
                  style: TextStyle(fontSize: 21),
                  overflow: TextOverflow.ellipsis,
                )),
              ],
            ),
            SizedBox(
              height: 2,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.access_time,
                  color: Colors.green[200],
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  'Hora de finalización: ',
                  style: TextStyle(fontSize: 21),
                ),
                Expanded(
                    child: Text(
                  plan.horaFin,
                  style: TextStyle(fontSize: 21),
                  overflow: TextOverflow.ellipsis,
                )),
              ],
            ),
            SizedBox(
              height: 2,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.add_location,
                  color: Colors.green[200],
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  'Punto de encuentro:',
                  style: TextStyle(fontSize: 21),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    plan.puntoEncuentro,
                    style: TextStyle(fontSize: 21),
                    textAlign: TextAlign.justify,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class DescripcionTitulo extends StatelessWidget {
  const DescripcionTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarDescripcion =
        Provider.of<_ModelPage>(context).mostrarDescripcion;
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarDescripcion)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarDescripcion = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Descripcion',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarDescripcion)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarDescripcion = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Descripcion',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class DescripcionContenido extends StatelessWidget {
  final Plan plan;
  const DescripcionContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Text(
                plan.descripcion,
                style: TextStyle(fontSize: 21),
                textAlign: TextAlign.justify,
              ))
            ],
          )
        ],
      ),
    );
  }
}

class ServiciosTitulo extends StatelessWidget {
  const ServiciosTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarServicios = Provider.of<_ModelPage>(context).mostrarServicios;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarServicios)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarServicios = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Servicios',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarServicios)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarServicios = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Servicios',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class ServiciosContenido extends StatelessWidget {
  final Plan plan;
  const ServiciosContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[GenerarListaServicios(plan: plan)],
      ),
    );
  }
}

class GenerarListaServicios extends StatelessWidget {
  final Plan plan;
  const GenerarListaServicios({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: plan.servicios.length,
      itemBuilder: (BuildContext context, int index) {
        return Servicio(servicio: plan.servicios[index]);
      },
    );
  }
}

class Servicio extends StatelessWidget {
  final String servicio;
  const Servicio({Key key, this.servicio}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.check_circle_outline,
            color: Colors.green[200],
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            servicio,
            style: TextStyle(fontSize: 21),
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }
}

class TarifaTitulo extends StatelessWidget {
  const TarifaTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarTarifa = Provider.of<_ModelPage>(context).mostrarTarifa;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarTarifa)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarTarifa = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Tarifas',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarTarifa)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarTarifa = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Tarifas',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class TarifaContenido extends StatelessWidget {
  final Plan plan;
  const TarifaContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.monetization_on, color: Colors.green[200]),
              SizedBox(
                width: 10,
              ),
              Text(
                '\$' +
                    plan.precio.toString() +
                    ' para ' +
                    plan.personas.toString() +
                    ' persona(s).',
                style: TextStyle(fontSize: 21),
                textAlign: TextAlign.justify,
              )
            ],
          )
        ],
      ),
    );
  }
}

class ServiciosAdicionalesTitulo extends StatelessWidget {
  const ServiciosAdicionalesTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarServiciosAdicional =
        Provider.of<_ModelPage>(context).mostrarServiciosAdicional;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarServiciosAdicional)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarServiciosAdicional = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Servicios Adicionales',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarServiciosAdicional)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarServiciosAdicional = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Servicios Adicionales',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class ServiciosAdicionalesContenido extends StatelessWidget {
  final Plan plan;
  const ServiciosAdicionalesContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[GenerarListaServiciosAdicionales(plan: plan)],
      ),
    );
  }
}

class GenerarListaServiciosAdicionales extends StatelessWidget {
  final Plan plan;
  const GenerarListaServiciosAdicionales({Key key, this.plan})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: plan.serviciosExtra.length,
      itemBuilder: (BuildContext context, int index) {
        return ServicioAdicional(servicio: plan.serviciosExtra[index]);
      },
    );
  }
}

class ServicioAdicional extends StatelessWidget {
  final String servicio;
  const ServicioAdicional({Key key, this.servicio}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.check_circle_outline,
            color: Colors.green[200],
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            servicio,
            style: TextStyle(fontSize: 21),
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }
}

class RecomendacionesTitulo extends StatelessWidget {
  const RecomendacionesTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarRecomendaciones =
        Provider.of<_ModelPage>(context).mostrarRecomendaciones;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarRecomendaciones)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarRecomendaciones = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Recomendaciones',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarRecomendaciones)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarRecomendaciones = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Recomendaciones',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class RecomendacionesContenido extends StatelessWidget {
  final Plan plan;
  const RecomendacionesContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[GenerarListaRecomendaciones(plan: plan)],
      ),
    );
  }
}

class GenerarListaRecomendaciones extends StatelessWidget {
  final Plan plan;
  const GenerarListaRecomendaciones({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: plan.recomendaciones.length,
      itemBuilder: (BuildContext context, int index) {
        return Recomendacion(recomendacion: plan.recomendaciones[index]);
      },
    );
  }
}

class Recomendacion extends StatelessWidget {
  final String recomendacion;
  const Recomendacion({Key key, this.recomendacion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.check_circle_outline,
            color: Colors.green[200],
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            recomendacion,
            style: TextStyle(fontSize: 21),
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }
}

class ObervacionesTitulo extends StatelessWidget {
  const ObervacionesTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarObservaciones =
        Provider.of<_ModelPage>(context).mostrarObservaciones;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarObservaciones)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarObservaciones = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Observaciones',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarObservaciones)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarObservaciones = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Observaciones',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}

class ObervacionesContenido extends StatelessWidget {
  final Plan plan;
  const ObervacionesContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[GenerarListaObservaciones(plan: plan)],
      ),
    );
  }
}

class GenerarListaObservaciones extends StatelessWidget {
  final Plan plan;
  const GenerarListaObservaciones({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: plan.observaciones.length,
      itemBuilder: (BuildContext context, int index) {
        return Observacion(observacion: plan.observaciones[index]);
      },
    );
  }
}

class Observacion extends StatelessWidget {
  final String observacion;
  const Observacion({Key key, this.observacion}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.check_circle_outline,
            color: Colors.green[200],
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            observacion,
            style: TextStyle(fontSize: 21),
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }
}
class ItinerariosTitulo extends StatelessWidget {
  const ItinerariosTitulo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool mostrarItinerario =
        Provider.of<_ModelPage>(context).mostrarItinerario;
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: Container(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            if (mostrarItinerario)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarItinerario = false;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Itinerario',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_up)
                  ],
                ),
              ),
            if (!mostrarItinerario)
              GestureDetector(
                onTap: () {
                  Provider.of<_ModelPage>(context, listen: false)
                      .mostrarItinerario = true;
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'Itinerario',
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.w600),
                    ),
                    Icon(Icons.keyboard_arrow_down)
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
class ItinerarioContenido extends StatelessWidget {
  final Plan plan;
  const ItinerarioContenido({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[GenerarListaItinearios(plan: plan)],
      ),
    );
  }
}

class GenerarListaItinearios extends StatelessWidget {
  final Plan plan;
  const GenerarListaItinearios({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.zero,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: plan.itinerario.length,
      itemBuilder: (BuildContext context, int index) {
        return Itinerario(itinerario:plan.itinerario[index]);
      },
    );
  }
}

class Itinerario extends StatelessWidget {
  final String itinerario;
  const Itinerario({Key key, this.itinerario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.check_circle_outline,
            color: Colors.green[200],
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
              child: Text(
            itinerario,
            style: TextStyle(fontSize: 21),
            textAlign: TextAlign.justify,
          ))
        ],
      ),
    );
  }
}

class TerminosCondiciones extends StatelessWidget {
  final Plan plan;
  const TerminosCondiciones({Key key, this.plan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlatButton(
        onPressed: _launchURL,
        child: Text('Terminos y condiciones',
        style: TextStyle(fontSize: 18,decoration: TextDecoration.underline),),
      ),
    );
  }
}

_launchURL() async {
  const url = 'https://weekendsantander.com/terminos_y_condiciones.pdf';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class _ModelPage with ChangeNotifier {
  int _index = 0;
  int _indexScrollTabBar = 0;
  bool _mostrarInformacionGeneral = true;
  bool _mostrarDescripcion = false;
  bool _mostrarServicios = false;
  bool _mostrarTarifa = false;
  bool _mostrarServiciosAdicional = false;
  bool _mostrarRecomendaciones = false;
  bool _mostrarObservaciones = false;
  bool _mostrarItinerario = false;
  bool _comentar = false;

  int get index => this._index;
  int get indexScrollTabBar => this._indexScrollTabBar;
  bool get mostrarInformacionGeneral => this._mostrarInformacionGeneral;
  bool get mostrarDescripcion => this._mostrarDescripcion;
  bool get mostrarServicios => this._mostrarServicios;
  bool get mostrarTarifa => this._mostrarTarifa;
  bool get mostrarServiciosAdicional => this._mostrarServiciosAdicional;
  bool get mostrarRecomendaciones => this._mostrarRecomendaciones;
  bool get mostrarObservaciones => this._mostrarObservaciones;
  bool get mostrarItinerario => this._mostrarItinerario;
  bool get mostrarCajaComentario => this._comentar;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  set mostrarInformacionGeneral(bool value) {
    this._mostrarInformacionGeneral = value;
    notifyListeners();
  }

  set indexScrollTabBar(int value) {
    _indexScrollTabBar = value;
    notifyListeners();
  }

  set mostrarDescripcion(bool value) {
    this._mostrarDescripcion = value;
    notifyListeners();
  }

  set mostrarServicios(bool value) {
    this._mostrarServicios = value;
    notifyListeners();
  }

  set mostrarTarifa(bool value) {
    this._mostrarTarifa = value;
    notifyListeners();
  }

  set mostrarServiciosAdicional(bool value) {
    this._mostrarServiciosAdicional = value;
    notifyListeners();
  }

  set mostrarRecomendaciones(bool value) {
    this._mostrarRecomendaciones = value;
    notifyListeners();
  }

  set mostrarItinerario(bool value){
    this._mostrarItinerario = value;
    notifyListeners();
  }

  set mostrarObservaciones(bool value) {
    this._mostrarObservaciones = value;
    notifyListeners();
  }

  set mostrarCajaComentario(bool value) {
    this._comentar = value;
    notifyListeners();
  }
}
