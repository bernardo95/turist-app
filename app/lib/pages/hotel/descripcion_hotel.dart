import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/pages/comentario/crear_comentario.dart';
import 'package:app/pages/comentario/listar_comentarios.dart';
import 'package:app/pages/comentario/placeholder.dart';
import 'package:app/pages/map/map_page.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:app/providers/habitacion_provider.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


class Arguments {
  List<Habitacion> habitaciones;
  Hotel hotel;
  Arguments(this.habitaciones, this.hotel);
}

class DescripcionHotel extends StatelessWidget {
  final Hotel hotel;
  const DescripcionHotel({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Provider.of<HotelesProvider>(context, listen: false).recargar = false;
        Provider.of<HotelesProvider>(context, listen: false).recargar = true;
        Navigator.of(context).pop();
      },
      child: Scaffold(
        body: ChangeNotifierProvider(
            create: (_) => _ModelPage(),
            child: SafeArea(child: _MainScroll(hotel: hotel))),
      ),
    );
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainScroll extends StatelessWidget {
  final Hotel hotel;
  const _MainScroll({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Provider.of<HotelesProvider>(context, listen: false).recargar =
                    false;
                Provider.of<HotelesProvider>(context, listen: false).recargar =
                    true;
                Navigator.of(context).pop();
              }),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          _Carousel(hotel: hotel),
          _MainView(hotel: hotel),
        ])),
      ],
    );
  }
}

class _Carousel extends StatelessWidget {
  final Hotel hotel;

  const _Carousel({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
          height: 200,
          initialPage: 0,
          scrollPhysics: BouncingScrollPhysics(),
          enlargeCenterPage: true,
          onPageChanged: (int index, CarouselPageChangedReason params) {
            Provider.of<_ModelPage>(context).index = index;
          }),
      items: hotel.imagenes.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return GestureDetector(
              onLongPress: () {
                mostrarImagenCreditos(context, imgUrl);
              },
              child: Container(
                width: size.width,
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: FadeInImage(
                    image: CachedNetworkImageProvider(imgUrl),
                    fit: BoxFit.cover,
                    placeholder: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Future<void> mostrarImagenCreditos(
      BuildContext context, String imgUrl) async {
    // final size = MediaQuery.of(context).size;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          final size = MediaQuery.of(context).size;
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                height: 320,
                width: size.width,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: size.width,
                      height: 250,
                      // margin: EdgeInsets.only(top: 5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: FadeInImage(
                          image: CachedNetworkImageProvider(imgUrl),
                          fit: BoxFit.cover,
                          // width: 350,
                          placeholder: AssetImage('assets/img/loading.gif'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Container(
                        // height: 150,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Creditos  '),
                            SizedBox(
                              width: 20,
                            ),
                            FadeInImage(
                                fit: BoxFit.contain,
                                width: size.width * 0.2,
                                placeholder:
                                    AssetImage('assets/img/loading.gif'),
                                image:
                                    AssetImage('assets/img/booking-logo.png')),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}

class _MainView extends StatefulWidget {
  final Hotel hotel;

  const _MainView({Key key, this.hotel}) : super(key: key);

  @override
  __MainViewState createState() => __MainViewState();
}

class __MainViewState extends State<_MainView>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text(
        'Información',
      ),
    ),
    Tab(
      child: Text(
        'Ubicación',
      ),
    ),
    Tab(
      child: Text(
        'Calificación',
      ),
    )
  ];
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    _tabController.addListener(() {
      Provider.of<_ModelPage>(context, listen: false).indexScrollTabBar =
          _tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    int index = Provider.of<_ModelPage>(context).indexScrollTabBar;
    return Column(children: <Widget>[
      TabBar(
        indicatorColor: Colors.green,
        indicatorWeight: 2,
        labelColor: Colors.black,
        labelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.normal, fontSize: 13),
        controller: _tabController,
        tabs: myTabs,
      ),
      ListView(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          if (index == 0) _Informacion(hotel: widget.hotel),
          if (index == 1) _Ubicacion(hotel: widget.hotel),
          if (index == 2) _Calificacion(hotel: widget.hotel)
        ],
      )
    ]);
  }
}

// ignore: must_be_immutable
class _Informacion extends StatelessWidget {
  Hotel hotel;
  _Informacion({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var provider = Provider.of<_ModelPage>(context);

    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            if (Provider.of<HotelesProvider>(context).recargar)
              FutureBuilder(
                future: HotelesProvider().getHotel(hotel.id),
                builder: (BuildContext context, AsyncSnapshot<Hotel> snapshot) {
                  if (snapshot.hasData) {
                    hotel = snapshot.data;
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 5.0),
                            child: Text(
                              '${hotel.nombre}',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              // const SizedBox(width: 16.0),
                              Container(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 8.0,
                                    horizontal: 16.0,
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[400],
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                  child: FutureBuilder(
                                    future: ComentariosProvider()
                                        .getComentariosCount(hotel.id),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<dynamic> snapshot) {
                                      if (snapshot.hasData) {
                                        return Row(
                                          children: <Widget>[
                                            Text(
                                              '${utils.rateTransform(hotel.calificacion)} ',
                                              style: TextStyle(fontSize: 16),
                                            ),
                                            Icon(
                                              Icons.star,
                                              size: 16,
                                              color: Colors.white,
                                            ),
                                            Text(
                                                ' / ${snapshot.data} Comentarios',
                                                style: TextStyle(fontSize: 16)),
                                          ],
                                        );
                                      } else {
                                        return Center(
                                          child: Container(
                                            height: 15,
                                            width: 15,
                                            child: CircularProgressIndicator(),
                                          ),
                                        );
                                      }
                                    },
                                  )),
                              Spacer(),
                              if (Provider.of<HotelesProvider>(context)
                                  .recargar)
                                FutureBuilder(
                                  future:
                                      UsuarioInstancia().isFavorito(hotel.id),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<bool> snapshot) {
                                    if (snapshot.hasData) {
                                      if (snapshot.data) {
                                        return IconButton(
                                          color: Colors.red[600],
                                          icon: Icon(Icons.favorite),
                                          onPressed: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            prefs.setBool(hotel.id, false);
                                            List<String> ids =
                                                await UsuarioInstancia()
                                                    .getListHotelesFavoritos();
                                            ids.remove(hotel.id);
                                            UsuarioInstancia()
                                                .setListFavoritosHoteles(ids);
                                            Provider.of<HotelesProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = false;
                                            Provider.of<HotelesProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = true;
                                          },
                                        );
                                      } else {
                                        return IconButton(
                                          color: Colors.grey[600],
                                          icon: Icon(Icons.favorite_border),
                                          onPressed: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            prefs.setBool(hotel.id, true);
                                            List<String> ids =
                                                await UsuarioInstancia()
                                                    .getListHotelesFavoritos();
                                            ids.add(hotel.id);
                                            UsuarioInstancia()
                                                .setListFavoritosHoteles(ids);
                                            Provider.of<HotelesProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = false;
                                            Provider.of<HotelesProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = true;
                                          },
                                        );
                                      }
                                    } else {
                                      return IconButton(
                                        color: Colors.grey[600],
                                        icon: Icon(Icons.favorite_border),
                                        onPressed: () async {
                                          SharedPreferences prefs =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs.setBool(hotel.id, true);
                                          List<String> ids =
                                              await UsuarioInstancia()
                                                  .getListHotelesFavoritos();
                                          ids.add(hotel.id);
                                          UsuarioInstancia()
                                              .setListFavoritosHoteles(ids);
                                          Provider.of<HotelesProvider>(context,
                                                  listen: false)
                                              .recargar = false;
                                          Provider.of<HotelesProvider>(context,
                                                  listen: false)
                                              .recargar = true;
                                        },
                                      );
                                    }
                                  },
                                )
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.all(25.0),
                            color: Colors.white,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text.rich(
                                            TextSpan(children: [
                                              WidgetSpan(
                                                  child: Icon(
                                                Icons.location_on,
                                                size: 16.0,
                                                color: Colors.grey,
                                              )),
                                              TextSpan(
                                                  text: '${hotel.ubicacion}',
                                                  style:
                                                      TextStyle(fontSize: 16))
                                            ]),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14.0),
                                          )
                                        ],
                                      ),
                                    ),
                                    Column(
                                      children: <Widget>[
                                        FutureBuilder(
                                          future: HabitacionesProvider()
                                              .getHabitacionesByHotel(hotel.id),
                                          builder: (BuildContext context,
                                              AsyncSnapshot<List<Habitacion>>
                                                  snapshot) {
                                            if (snapshot.hasData) {
                                              return Text(
                                                "\$ ${snapshot.data[0].precio}",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20.0),
                                              );
                                            } else {
                                              return Text('');
                                            }
                                          },
                                        ),
                                        Text(
                                          "/ por noche",
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color: Colors.grey),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                const SizedBox(height: 20.0),
                                SizedBox(
                                  width: double.infinity,
                                  child: RaisedButton(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0)),
                                    color: Colors.green,
                                    textColor: Colors.white,
                                    child: Text(
                                      "Ver habitaciones disponibles",
                                      style: TextStyle(
                                          fontWeight: FontWeight.normal,
                                          fontSize: 16),
                                    ),
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 16.0,
                                      horizontal: 32.0,
                                    ),
                                    onPressed: () async {
                                      final habitaciones =
                                          await HabitacionesProvider()
                                              .getHabitacionesByHotel(hotel.id);
                                      Navigator.pushNamed(
                                          context, 'habitaciones',
                                          arguments:
                                              Arguments(habitaciones, hotel));
                                    },
                                  ),
                                ),
                                const SizedBox(height: 30.0),
                                Column(
                                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Check-in: ${hotel.checkIn['desde']} - ${hotel.checkIn['hasta']}',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    Text(
                                        'Check-out: ${hotel.checkOut['desde']} - ${hotel.checkOut['hasta']}',
                                        style: TextStyle(fontSize: 15))
                                  ],
                                ),
                                const SizedBox(height: 30.0),
                                Text(
                                  "Descripción".toUpperCase(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.0),
                                ),
                                const SizedBox(height: 10.0),
                                Text(
                                  "${hotel.descripcion}",
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 16.0),
                                ),
                              ],
                            ),
                          ),
                          Terminos()

                        ],
                      ),
                    );
                  } else {
                    return _Loading();
                  }
                },
              ),
          ],
        ),
      ),
    );
  }
}

class _Ubicacion extends StatelessWidget {
  final Hotel hotel;
  const _Ubicacion({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height * 0.56,
      child: GoogleMapPage(
        hotel: hotel,
      ),
    );
  }
}

class _Calificacion extends StatelessWidget {
  final Hotel hotel;
  const _Calificacion({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    UsuarioInstancia usuario = Provider.of<UsuarioInstancia>(context);
    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            FutureBuilder(
              future: usuario.getId(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return GestureDetector(
                    onTap: () {
                      Provider.of<ComentariosProvider>(context, listen: false)
                          .comentando = true;
                    },
                    child:
                        (Provider.of<ComentariosProvider>(context).comentando ==
                                true)
                            ? CrearComentario(
                                ref: hotel.id,
                                idUsuario: snapshot.data,
                                tipo: "hotel",
                              )
                            : PlacehorderCommentPage(),
                  );
                } else {
                  return Container();
                }
              },
            ),
            ListarComentariosPage(
              ref: hotel.id,
              tipo: "hotel",
            )
          ],
        ),
      ),
    );
  }
}
class Terminos extends StatelessWidget {
  const Terminos({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FlatButton(
        onPressed: _launchURL,
        child: Text('Terminos y condiciones',
        style: TextStyle(fontSize: 18,decoration: TextDecoration.underline),),
      ),
        ],
      ),
    );
  }
  _launchURL() async {
  const url = 'https://www.booking.com/content/terms.es.html?label=gen173nr-1DCAEoggI46AdIM1gEaDKIAQGYAQq4ARnIAQzYAQPoAQGIAgGoAgO4Au_-1vkFwAIB0gIkY2JlNWQ5ODktM2I1YS00ZTEzLThjODItMWY3NjdmZDViZjY42AIE4AIB;sid=ced9d0a4f0588a8249564afa275a5478#tcs_s9';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
}
class _ModelPage with ChangeNotifier {
  int _index = 0;
  int _indexScrollTabBar = 0;
  bool _comentar = false;

  int get index => this._index;
  int get indexScrollTabBar => this._indexScrollTabBar;
  bool get mostrarCajaComentario => this._comentar;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  set indexScrollTabBar(int value) {
    _indexScrollTabBar = value;
    notifyListeners();
  }

  set mostrarCajaComentario(bool value) {
    this._comentar = value;
    notifyListeners();
  }
}
