import 'package:app/models/habitacion_model.dart';
import 'package:app/models/hotel_model.dart';
import 'package:app/pages/filtros/filtro_hoteles.dart';
import 'package:app/providers/filtro_provider.dart';
import 'package:app/providers/habitacion_provider.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/search/search_delegate_hoteles.dart';
import 'package:app/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;
import 'package:cached_network_image/cached_network_image.dart';


class HotelPage extends StatefulWidget {
  @override
  _HotelPageState createState() => _HotelPageState();
}

class _HotelPageState extends State<HotelPage>
    with AutomaticKeepAliveClientMixin {
  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: ChangeNotifierProvider(
    create: (_) => _ModelPage(),
    child: Scaffold(
        drawer: MenuWidget(), body: Container(child: _MainScroll())),
        ),
      );
  }

  @override
  bool get wantKeepAlive => true;

  // Future<bool> _salirApp() {
  //   return showDialog(
  //     context: context,
  //     barrierDismissible: false,
  //     builder: (BuildContext context)=>AlertDialog(
  //       shape:
  //               RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  //       title: Text('¿Desea salir de la aplicación?'),
  //       actions: <Widget>[
  //         FlatButton(onPressed: (){
  //           Navigator.pop(context,false);
  //         }, child: Text('No')),
  //         FlatButton(onPressed: (){
  //           Navigator.pop(context,true);
  //         }, child: Text('Si')),
  //       ],
  //     )
  //   );
  // }
}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainScroll extends StatefulWidget {
  const _MainScroll({Key key}) : super(key: key);

  @override
  __MainScrollState createState() => __MainScrollState();
}

class __MainScrollState extends State<_MainScroll> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          floating: true,
          backgroundColor: Colors.green,
          title: Text('Hoteles'),
          centerTitle: true,
          elevation: 0,
          actions: <Widget>[
            FutureBuilder(
              future: HotelesProvider().getHoteles(),
              builder: (BuildContext context, AsyncSnapshot<List<Hotel>> snapshot) { 
                if (snapshot.hasData) {
                  return IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  showSearch(context: context, delegate: DataSearchHoteles(hoteles:snapshot.data));
                });
                } else {
                  return Container();
                }
             },)
            
          ],

        ),
        SliverList(
          delegate: SliverChildListDelegate([
            FiltroHotelesPage(),
            if (Provider.of<HotelesProvider>(context).recargar)
              FutureBuilder(
                future: HotelesProvider().getHoteles(filtro: Provider.of<FiltroProvider>(context).filtroHotel),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Hotel>> snapshot) {
                  if (snapshot.hasData) {
                    return _ListaHoteles(
                      hoteles: snapshot.data,
                    );
                  } else {
                    return _Loading();
                  }
                },
              )
          ]),
        )
      ],
    );
  }
}

class _ListaHoteles extends StatelessWidget {
  final List<Hotel> hoteles;

  const _ListaHoteles({Key key, this.hoteles}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: hoteles.length,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return _PlantillaHotel(
          context: context,
          index: index,
          listaHoteles: hoteles,
        );
      },
    );
  }
}

class _PlantillaHotel extends StatelessWidget {
  final BuildContext context;
  final int index;
  final List<Hotel> listaHoteles;
  const _PlantillaHotel({Key key, this.index, this.listaHoteles, this.context})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                _ImagenHotel(
                  imagenes: listaHoteles[index].imagenes,
                ),
                SizedBox(
                  width: 10.0,
                ),
                _InfoHotel(hotel: listaHoteles[index]),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _ImagenHotel extends StatelessWidget {
  final List<String> imagenes;
  const _ImagenHotel({Key key, this.imagenes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: FadeInImage(
          placeholder: AssetImage('assets/img/loading.gif'),
          image: CachedNetworkImageProvider("${imagenes[0]}"),
          fit: BoxFit.fitHeight,
          width: 150.0,
          height: 280.0,
        ),
      ),
    );
  }
}

class _InfoHotel extends StatelessWidget {
  final Hotel hotel;
  const _InfoHotel({Key key, this.hotel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            "${hotel.nombre}",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.green[700],
                fontSize: 20.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: <Widget>[
              Text(
                'Calificación: ',
                style: TextStyle(fontSize: 18),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                '${utils.rateTransform(hotel.calificacion)}/5',
                style: Theme.of(context).textTheme.headline6,
                textAlign: TextAlign.start,
              ),
              Icon(
                Icons.star,
                color: Colors.green,
              )
              // EstrellasWidget(numero: hotel.calificacion,color: Colors.green,size: 15,),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.location_on,
                color: Colors.blue,
              ),
              Expanded(
                child: Text(
                  '${hotel.ubicacion}',
                  style: Theme.of(context).textTheme.subtitle1,
                  textAlign: TextAlign.start,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),

          Row(children: <Widget>[
            Text(
              'Categoría: ',
              style: TextStyle(fontWeight: FontWeight.w500, fontSize: 17),
            ),
            Expanded(
              child: Text(
                '${hotel.categoria}',
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 17),
              ),
            ),
          ]),
          // SizedBox(height: 1,),
          Row(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FutureBuilder(
                future: HabitacionesProvider().getHabitacionesByHotel(hotel.id),
                builder: (BuildContext context,
                    AsyncSnapshot<List<Habitacion>> snapshot) {
                      
                  if (snapshot.hasData && snapshot.data.length>0) {
                    List<dynamic> data = ["${hotel.id}","${snapshot.data[0].precio}"];
                    Provider.of<HotelesProvider>(context,listen: false).hotelesSortByPrecio = data;
                    return Expanded(
                        child: Text('Desde \$${snapshot.data[0]?.precio}',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                            )));
                  } else {
                    return Text('');
                  }
                },
              ),
              SizedBox(
                width: 5.0,
              ),
              RaisedButton(
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pushNamed(context, 'descripcion_hotel',
                      arguments: hotel);
                },
                child: Text(
                  'Ver oferta',
                  style: TextStyle(fontSize: 15.0),
                ),
                color: Colors.green,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
            ],
          ),
          // Text('')
        ],
      ),
    );
  }
}


class _ModelPage with ChangeNotifier {
  int _precio = 0;

  int get precio => this._precio;

  set precio(int value) {
    this._precio = value;
    notifyListeners();
  }
}

//