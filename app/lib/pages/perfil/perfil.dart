import 'dart:io';
import 'package:app/models/usuario_model.dart';
import 'package:app/providers/profile_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:app/utils/validators.dart' as utils;

class PerfilPage extends StatelessWidget {
  const PerfilPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text('Perfil'),
        centerTitle: true,
        elevation: 0,
      ),
      body: ChangeNotifierProvider(
        create: (_) => _ModelPage(),
        child: (Provider.of<ProfileProvider>(context).usuario == null)
            ? CustomAviso()
            : MainPerfil(),
      ),
    );
  }
}

class CustomAviso extends StatelessWidget {
  const CustomAviso({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Por favor inicie sesion',
        style: TextStyle(fontSize: 19),
      ),
    );
  }
}

class MainPerfil extends StatelessWidget {
  const MainPerfil({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          child: Column(
            children: <Widget>[_ImagenPerfil(), _Formulario()],
          ),
        ),
      ),
    );
  }
}

class _ImagenPerfil extends StatelessWidget {
  const _ImagenPerfil({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // bool _image = Provider.of<_ModelPage>(context).image;
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              _cambiarFoto(context);
            },
            child: Consumer<_ModelPage>(
              builder: (BuildContext context, value, Widget child) {
                if (!value.image) {
                  return Center(
                    child: FutureBuilder(
                      future: UsuarioInstancia().getPhotoUrl(),
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapshot) {
                        return CircleAvatar(
                          radius: 60,
                          backgroundImage: (snapshot.data == null)
                              ? AssetImage('assets/img/no-profile.jpg')
                              : CachedNetworkImageProvider(snapshot.data),
                        );
                      },
                    ),
                  );
                }
                return Center(
                  child: CircleAvatar(
                      radius: 60,
                      backgroundImage: AssetImage(
                          Provider.of<_ModelPage>(context).imageFile.path)),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Center(
              child: Text('click sobre la imagen para cambiarla'),
            ),
          )
        ],
      ),
    );
  }

  Future<void> _cambiarFoto(BuildContext context) async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      Provider.of<_ModelPage>(context, listen: false).imageFile = image;
      Provider.of<_ModelPage>(context, listen: false).image = true;
    }
  }
}


class _Formulario extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Usuario usuario = Provider.of<ProfileProvider>(context).usuario;
    
    final formKey = GlobalKey<FormState>();
    Usuario usuarioModel = Usuario();
    usuarioModel = usuario;
    TextEditingController _controllerName =
        TextEditingController(text: usuario.nombre);
    TextEditingController _controllerLastName =
        TextEditingController(text: usuario.apellido);
    String defecto = 'Seleccionar';
    void initStateCustom() {
      if (Provider.of<_ModelPage>(context).count == 0) {
        Provider.of<_ModelPage>(context, listen: false).fecha.text =
            usuario.fechaNacimiento;
        Provider.of<_ModelPage>(context, listen: false).count = 1;
      }
      if (Provider.of<_ModelPage>(context).countGender == 0) {
        Provider.of<_ModelPage>(context, listen: false).genero = usuario.genero;
        Provider.of<_ModelPage>(context, listen: false).countGender = 1;
      }
    }

    initStateCustom();
    
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Nombre: ',
                  style: TextStyle(fontSize: 19),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: TextFormField(
                  controller: _controllerName,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Nombre',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    usuarioModel.nombre = _controllerName.text;
                  },
                  validator: (value) {
                    if (value.length < 3) {
                      return 'Nombre no valido';
                    }
                    if (utils.isString(value)) {
                      return null;
                    } else {
                      return 'Nombre no valido';
                    }
                  },
                ))
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Apellido: ',
                  style: TextStyle(fontSize: 19),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: TextFormField(
                  controller: _controllerLastName,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Apellido',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    usuarioModel.apellido = _controllerLastName.text;
                  },
                  validator: (value) {
                    if (value.length < 3) {
                      return 'Apellido no valido';
                    }
                    if (utils.isString(value)) {
                      return null;
                    } else {
                      return 'Apellido no valido';
                    }
                  },
                ))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Correo: ',
                  style: TextStyle(fontSize: 19),
                ),
                Text(
                  usuario.email,
                  style: TextStyle(fontSize: 21),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text('Fecha de nacimiento:', style: TextStyle(fontSize: 19)),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextFormField(
                    enableInteractiveSelection: false,
                    controller: Provider.of<_ModelPage>(context).fecha,
                    style: TextStyle(
                      fontSize: 22.0,
                    ),
                    decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                      ),
                      hintText: 'aaaa/mm/dd',
                      focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                      hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                      hintStyle: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    onTap: () async {
                      // FocusScope.of(context).requestFocus(new FocusNode());
                      await selectDate(context);
                    },
                    onSaved: (value) {
                      usuarioModel.fechaNacimiento = value;
                    },
                    validator: (value) {
                      final date = DateTime.parse(value);
                      if (utils.dateVerify(date)) {
                        return null;
                      } else {
                        return 'fecha no valida';
                      }
                    },
                  ),
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Genero: ',
                  style: TextStyle(fontSize: 20),
                ),
                SizedBox(
                  width: 30,
                ),
                Expanded(
                  child: DropdownButtonFormField(

                    value: (Provider.of<_ModelPage>(context).genero == "")
                        ? defecto
                        : Provider.of<_ModelPage>(context).genero,
                    onChanged: (value) {
                      Provider.of<_ModelPage>(context, listen: false).genero =
                          value;
                    },
                    validator: (value) {
                      if (utils.validGender(value)) {
                        return null;
                      } else {
                        return 'Seleccione un genero';
                      }
                    },
                    onSaved: (value) {
                      usuarioModel.genero = value;
                    },
                    items: <String>[
                      'Seleccionar',
                      'Masculino',
                      'Femenino',
                      'Otro'
                    ].map<DropdownMenuItem>((String value) {
                      return DropdownMenuItem<String>(
                        child: Text(
                          value,
                          style: TextStyle(fontSize: 21),
                        ),
                        value: value,
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 50,
            ),
            RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 7,horizontal: 15),
                textColor: Colors.white,
                child: Text(
                  'Guardar cambios',
                  style: TextStyle(fontSize: 19),
                ),
                color: Colors.green,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                onPressed: () => {
                  _submit(formKey,usuarioModel,context),
                  // print(usuarioModel)
                })
          ],
        ),
      ),
    );
  }

  selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().year - 100),
      lastDate: DateTime(DateTime.now().year + 1),
      locale: Locale('es'),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: Colors.green,
            accentColor: Colors.green,
          ),
          child: child,
        );
      },
    );
    if (picked != null) {
      String fecha = picked.toString().split(' ')[0];
      Provider.of<_ModelPage>(context, listen: false).fecha.text = fecha;
    }
  }

  Future<void> _submit(GlobalKey<FormState> formKey,Usuario usuario,BuildContext context) async {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      final service = UsuarioProvider();
      print(usuario.nombre);
      await service.actualizarCuenta(usuario, Provider.of<_ModelPage>(context,listen: false).imageFile);
    }
  }
}

class _ModelPage with ChangeNotifier {
  bool _image = false;
  File _imageFile;
  TextEditingController _fecha = TextEditingController();
  String _genero = '';
  int _countDate = 0;
  int _countGender = 0;
  int _selectRadioTile = 0;

  bool get image => this._image;
  File get imageFile => this._imageFile;
  TextEditingController get fecha => this._fecha;
  int get count => this._countDate;
  int get countGender => this._countGender;
  String get genero => this._genero;
  int get selectedRadioTile => this._selectRadioTile;

  set image(bool value) {
    this._image = value;
    notifyListeners();
  }

  set imageFile(File value) {
    this._imageFile = value;
    notifyListeners();
  }

  set fecha(TextEditingController value) {
    this._fecha = value;
    notifyListeners();
  }

  set count(int value) {
    this._countDate = value;
    notifyListeners();
  }

  set countGender(int value) {
    this._countGender = value;
    notifyListeners();
  }

  set genero(String value) {
    this._genero = value;
    notifyListeners();
  }

  set selectedRadioTile(int value) {
    this._selectRadioTile = value;
    notifyListeners();
  }
}
