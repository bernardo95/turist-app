import 'package:app/models/restaurante_model.dart';
import 'package:app/pages/comentario/crear_comentario.dart';
import 'package:app/pages/comentario/listar_comentarios.dart';
import 'package:app/pages/comentario/placeholder.dart';
import 'package:app/pages/map/map_page.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:app/providers/restaurantes_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cached_network_image/cached_network_image.dart';


class DescripcionRestaurante extends StatelessWidget {
  final Restaurante restaurante;
  const DescripcionRestaurante({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Provider.of<RestauranteProvider>(context,listen: false).recargar = false;
            Provider.of<RestauranteProvider>(context,listen: false).recargar = true;
            Navigator.of(context).pop();
      },
          child: Scaffold(
        body: ChangeNotifierProvider(
            create: (_) => _ModelPage(),
            child: SafeArea(child: _MainScroll(restaurante: restaurante))),
      ),
    );
  }
}
class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}
class _MainScroll extends StatelessWidget {
  final Restaurante restaurante;
  const _MainScroll({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      shrinkWrap: true,
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
          leading:  IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
            Provider.of<RestauranteProvider>(context,listen: false).recargar = false;
            Provider.of<RestauranteProvider>(context,listen: false).recargar = true;
            Navigator.of(context).pop();
          }),
        ),
        SliverList(
            delegate: SliverChildListDelegate([
          _Carousel(restaurante: restaurante),
          _MainView(restaurante: restaurante),
        ])),
      ],
    );
  }
}

class _Carousel extends StatelessWidget {
  final Restaurante restaurante;

  const _Carousel({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
          height: 200,
          initialPage: 0,
          scrollPhysics: BouncingScrollPhysics(),
          enlargeCenterPage: true,
          onPageChanged: (int index, CarouselPageChangedReason params) {
            Provider.of<_ModelPage>(context).index = index;
          }),
      items: restaurante.imagenes.map((imgUrl) {
        return Builder(
          builder: (BuildContext context) {
            return GestureDetector(
              onLongPress: () {
                mostrarImagenCreditos(context, imgUrl);
              },
              child: Container(
                width: size.width,
                margin: EdgeInsets.only(top: 5.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: FadeInImage(
                    image: CachedNetworkImageProvider(imgUrl),
                    fit: BoxFit.cover,
                    placeholder: AssetImage('assets/img/loading.gif'),
                  ),
                ),
              ),
            );
          },
        );
      }).toList(),
    );
  }

  Future<void> mostrarImagenCreditos(
      BuildContext context, String imgUrl) async {
    // final size = MediaQuery.of(context).size;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          final size = MediaQuery.of(context).size;
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Container(
                height: 334,
                width: size.width,
                child: Column(
                  children: <Widget>[
                    Container(
                      width: size.width,
                      height: 250,
                      // margin: EdgeInsets.only(top: 5.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10.0),
                        child: FadeInImage(
                          image: CachedNetworkImageProvider(imgUrl),
                          fit: BoxFit.cover,
                          // width: 350,
                          placeholder: AssetImage('assets/img/loading.gif'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Container(
                        // height: 150,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Créditos  '),
                            SizedBox(
                              width: 20,
                            ),
                            FadeInImage(
                            fit: BoxFit.fitHeight,
                            width: size.width * 0.2,
                            // height: size.height * 0.2,
                            placeholder:
                                AssetImage('assets/img/loading.gif'),
                            image: AssetImage('assets/img/google.png')
                              ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));
        });
  }
}

class _MainView extends StatefulWidget {
  final Restaurante restaurante;

  const _MainView({Key key, this.restaurante}) : super(key: key);

  @override
  __MainViewState createState() => __MainViewState();
}

class __MainViewState extends State<_MainView>
    with SingleTickerProviderStateMixin {
  final List<Tab> myTabs = <Tab>[
    Tab(
      child: Text(
        'Información',
      ),
    ),
    Tab(
      child: Text(
        'Ubicación',
      ),
    ),
    Tab(
      child: Text(
        'Calificación',
      ),
    )
  ];
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    _tabController.addListener(() {
      Provider.of<_ModelPage>(context, listen: false).indexScrollTabBar =
          _tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    int index = Provider.of<_ModelPage>(context).indexScrollTabBar;
    return Column(children: <Widget>[
      TabBar(
        indicatorColor: Colors.green,
        indicatorWeight: 2,
        labelColor: Colors.black,
        labelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.normal, fontSize: 13),
        controller: _tabController,
        tabs: myTabs,
      ),
      ListView(
        padding: EdgeInsets.all(0),
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          if (index == 0) _Informacion(restaurante: widget.restaurante),
          if (index == 1) _Ubicacion(restaurante: widget.restaurante),
          if (index == 2) _Calificacion(restaurante: widget.restaurante)
        ],
      )
    ]);
  }
}

class _Informacion extends StatelessWidget {
  Restaurante restaurante;
  _Informacion({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            if (Provider.of<RestauranteProvider>(context).recargar)
              FutureBuilder(
                future: RestauranteProvider().getRestaurante(restaurante.id),
                builder: (BuildContext context,
                    AsyncSnapshot<Restaurante> snapshot) {
                  if (snapshot.hasData) {
                    restaurante = snapshot.data;
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 5.0),
                            child: Text(
                              '${restaurante.nombre}',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Row(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              // const SizedBox(width: 16.0),
                              Container(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 8.0,
                                    horizontal: 16.0,
                                  ),
                                  decoration: BoxDecoration(
                                      color: Colors.grey[400],
                                      borderRadius:
                                          BorderRadius.circular(20.0)),
                                  child: FutureBuilder(
                                    future: ComentariosProvider()
                                        .getComentariosCount(restaurante.id),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<dynamic> snapshot) {
                                      if (snapshot.hasData) {
                                      return Row(
                                        children: <Widget>[
                                           Text(
                                            "${utils.rateTransform(restaurante.calificacion)}",
                                             style: TextStyle(
                                                color: Colors.white, fontSize: 16.0),
                                           ),
                                          Icon(
                                            Icons.star,
                                            size: 16,
                                            color: Colors.white,
                                          ),
                                          Text('  / ${snapshot.data} Comentarios',
                                              style: TextStyle(fontSize: 16)),
                                        ],
                                      );
                                      } else {
                                        return Center(
                                          child: Container(
                                            height: 15,
                                            width: 15,
                                            child: CircularProgressIndicator(),
                                          ),
                                        );
                                      }
                                    },
                                  )),
                              Spacer(),
                              if (Provider.of<RestauranteProvider>(context)
                                  .recargar)
                                FutureBuilder(
                                  future:
                                      UsuarioInstancia().isFavorito(restaurante.id),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<bool> snapshot) {
                                    if (snapshot.hasData) {
                                      if (snapshot.data) {
                                        return IconButton(
                                          color: Colors.red[600],
                                          icon: Icon(Icons.favorite),
                                          onPressed: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            prefs.setBool(restaurante.id, false);
                                            List<String> ids = await UsuarioInstancia().getListRestaurantesFavoritos();
                                            ids.remove(restaurante.id);
                                            UsuarioInstancia().setListFavoritosRestaurantes(ids);
                                            Provider.of<RestauranteProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = false;
                                            Provider.of<RestauranteProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = true;
                                          },
                                        );
                                      } else {
                                        return IconButton(
                                          color: Colors.grey[600],
                                          icon: Icon(Icons.favorite_border),
                                          onPressed: () async {
                                            SharedPreferences prefs =
                                                await SharedPreferences
                                                    .getInstance();
                                            prefs.setBool(restaurante.id, true);
                                            List<String> ids = await UsuarioInstancia().getListRestaurantesFavoritos();
                                            ids.add(restaurante.id);
                                            UsuarioInstancia().setListFavoritosRestaurantes(ids);
                                            Provider.of<RestauranteProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = false;
                                            Provider.of<RestauranteProvider>(
                                                    context,
                                                    listen: false)
                                                .recargar = true;
                                          },
                                        );
                                      }
                                    } else {
                                      return IconButton(
                                        color: Colors.grey[600],
                                        icon: Icon(Icons.favorite_border),
                                        onPressed: () async {
                                          SharedPreferences prefs =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs.setBool(restaurante.id, true);
                                          List<String> ids = await UsuarioInstancia().getListRestaurantesFavoritos();
                                            ids.add(restaurante.id);
                                            UsuarioInstancia().setListFavoritosRestaurantes(ids);
                                          Provider.of<RestauranteProvider>(context,
                                                  listen: false)
                                              .recargar = false;
                                          Provider.of<RestauranteProvider>(context,
                                                  listen: false)
                                              .recargar = true;
                                        },
                                      );
                                    }
                                  },
                                )
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.all(25.0),
                            color: Colors.white,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text.rich(
                                            TextSpan(children: [
                                              WidgetSpan(
                                                  child: Icon(
                                                Icons.location_on,
                                                size: 16.0,
                                                color: Colors.grey,
                                              )),
                                              TextSpan(
                                                  text:
                                                      '${restaurante.ubicacion}',
                                                  style:
                                                      TextStyle(fontSize: 16))
                                            ]),
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: 14.0),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                //const SizedBox(height: 20.0),
                                //const SizedBox(height: 30.0),
                                const SizedBox(height: 20.0),
                                if (restaurante.informacion != '')
                                Text(
                                  "Información".toUpperCase(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.0),
                                ),
                                if (restaurante.informacion == '')
                                Text(
                                  "No hay información de este restaruante",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16.0),
                                ),
                                const SizedBox(height: 10.0),
                                Text(
                                  "${restaurante.informacion}",
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 16.0),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  } else {
                    return _Loading();
                  }
                },
              )
          ],
        ),
      ),
    );
  }
}

class _Ubicacion extends StatelessWidget {
  final Restaurante restaurante;
  const _Ubicacion({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height * 0.56,
      child: GoogleMapPage(
        restaurante: restaurante,
      ),
    );
  }
}

class _Calificacion extends StatelessWidget {
  final Restaurante restaurante;
  const _Calificacion({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    UsuarioInstancia usuario = Provider.of<UsuarioInstancia>(context);
    return Container(
      width: size.width,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(
          children: <Widget>[
            FutureBuilder(
              future: usuario.getId(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.hasData) {
                  return GestureDetector(
                    onTap: () {
                      Provider.of<ComentariosProvider>(context, listen: false)
                          .comentando = true;
                    },
                    child:
                        (Provider.of<ComentariosProvider>(context).comentando ==
                                true)
                            ? CrearComentario(
                                ref: restaurante.id,
                                idUsuario: snapshot.data,
                                tipo: "restaurante",
                              )
                            : PlacehorderCommentPage(),
                  );
                } else {
                  return Container();
                }
              },
            ),
            ListarComentariosPage(
              ref: restaurante.id,
              tipo: "restaurante",
            )
          ],
        ),
      ),
    );
  }
}

class _ModelPage with ChangeNotifier {
  int _index = 0;
  int _indexScrollTabBar = 0;
  bool _comentar = false;

  int get index => this._index;
  int get indexScrollTabBar => this._indexScrollTabBar;
  bool get mostrarCajaComentario => this._comentar;

  set index(int value) {
    _index = value;
    notifyListeners();
  }

  set indexScrollTabBar(int value) {
    _indexScrollTabBar = value;
    notifyListeners();
  }

  set mostrarCajaComentario(bool value) {
    this._comentar = value;
    notifyListeners();
  }
}
