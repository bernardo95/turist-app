import 'package:app/models/restaurante_model.dart';
import 'package:app/pages/filtros/filtro_restaurante.dart';
import 'package:app/providers/filtro_provider.dart';
import 'package:app/providers/restaurantes_provider.dart';
import 'package:app/search/search_delegate_resturantes.dart';
import 'package:app/widgets/menu_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;
import 'package:cached_network_image/cached_network_image.dart';

class RestaurantPage extends StatefulWidget{

  @override
  _RestaurantPageState createState() => _RestaurantPageState();
}

class _RestaurantPageState extends State<RestaurantPage> with AutomaticKeepAliveClientMixin{
  @override
// ignore: must_call_super
Widget build(BuildContext context) {

    return SafeArea(
        top: true,
      child: ChangeNotifierProvider(
    create: (_) => _ModelPage(),
        child: Scaffold(
      drawer: MenuWidget(),
      body: Container(
          child:  _MainScroll()
        )),
        ),
      );
  }

  @override
  bool get wantKeepAlive => true;

  Future<bool> _salirApp() {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context)=>AlertDialog(
        shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        title: Text('¿Desea salir de la aplicación?'),
        actions: <Widget>[
          FlatButton(onPressed: (){
            Navigator.pop(context,false);
          }, child: Text('No')),
          FlatButton(onPressed: (){
            Navigator.pop(context,true);
          }, child: Text('Si')),
        ],
      )
    );
  }
}
class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainScroll extends StatefulWidget {

  const _MainScroll({Key key}) : super(key: key);

  @override
  __MainScrollState createState() => __MainScrollState();
}

class __MainScrollState extends State<_MainScroll> {

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          floating: true,
          backgroundColor: Colors.green,
          title: Text('Restaurantes'),
          centerTitle: true,
          elevation: 0,
          actions: <Widget>[
            FutureBuilder(
              future: RestauranteProvider().getRestaurantes(),
              builder: (BuildContext context, AsyncSnapshot<List<Restaurante>> snapshot) { 
                if (snapshot.hasData) {
                  return IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  showSearch(context: context, delegate: DataSearchRestaurante(restaurantes:snapshot.data));
                });
                } else {
                  return Container();
                }
             },)
          ],
          
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            FiltroRestaurantesPage(),
            if(Provider.of<RestauranteProvider>(context).recargar)
            FutureBuilder(
              future: RestauranteProvider().getRestaurantes(filtro: Provider.of<FiltroProvider>(context).filtroRestaurante),
              builder: (BuildContext context, AsyncSnapshot<List<Restaurante>> snapshot) { 
                if (snapshot.hasData) {
                  return _ListaRestaurantes(
              restaurantes: snapshot.data,
            );
                } else {
                  return _Loading();
                }
             },)
            
          ]),
        )
      ],
    );
  }
}

class _ListaRestaurantes extends StatelessWidget {
  final List<Restaurante> restaurantes;

  const _ListaRestaurantes({Key key, this.restaurantes,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: restaurantes.length,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return _PlantillaRestaurante(
          context: context,  index: index, listaRestaurantes: restaurantes,
        );
      },
    );
  }
}

class _PlantillaRestaurante extends StatelessWidget {
  final BuildContext context;
  final int index;
  final List<Restaurante> listaRestaurantes;
  const _PlantillaRestaurante({Key key, this.index, this.listaRestaurantes, this.context}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                _ImagenRestaurante(imagenes: listaRestaurantes[index].imagenes,),
                SizedBox(
                  width: 10.0,
                ),
                _InfoRestaurante(restaurante: listaRestaurantes[index]),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _ImagenRestaurante extends StatelessWidget {
  final List<String> imagenes;
  const _ImagenRestaurante({Key key, this.imagenes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: FadeInImage(
            placeholder: AssetImage('assets/img/loading.gif'),
            image: CachedNetworkImageProvider("${imagenes[0]}"),
            fit: BoxFit.fitHeight,
            width: 150.0,
            height: 250.0,
          ),
        ),
      );
  }
}

class _InfoRestaurante extends StatelessWidget {

  final Restaurante restaurante;
  const _InfoRestaurante({Key key, this.restaurante}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
              "${restaurante.nombre}",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: Colors.green[700],
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10,),
          Row(
            children: <Widget>[
              Text('Calificación: ',style: TextStyle(fontSize: 18),),
              SizedBox(width: 5,),
              Text(
                '${utils.rateTransform(restaurante.calificacion)}/5',
                style: Theme.of(context).textTheme.headline6,
                textAlign: TextAlign.start,
              ),
              Icon(Icons.star,color: Colors.green,)
              // EstrellasWidget(numero: hotel.calificacion,color: Colors.green,size: 15,),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            children: <Widget>[
          Icon(
            Icons.location_on,
            color: Colors.blue,
          ),
          Expanded(
              child: Text(
              '${restaurante.ubicacion}',
              style: Theme.of(context).textTheme.subtitle1,
              textAlign: TextAlign.start,
            ),
          ),
            ],
          ),
          SizedBox(height: 25,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                width: 5.0,
              ),
              RaisedButton(
                textColor: Colors.white,
                onPressed: () {
                  Navigator.pushNamed(context, 'descripcion_restaurante',arguments: restaurante);
                },
                child: Text(
                  'Ver más',
                  style: TextStyle(fontSize: 15.0),
                ),
                color: Colors.green,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
              ),
            ],
          ),
          // Text('')
        ],
      ),
    );
  }
}

class _ModelPage with ChangeNotifier {
 int _precio = 0;

 int get precio => this._precio;

  set precio(int value){
    this._precio = value;
    notifyListeners();
  }
}