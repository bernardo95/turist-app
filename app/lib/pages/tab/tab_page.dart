import 'package:app/pages/hotel/hotel_page.dart';
import 'package:app/pages/restaurante/restaurant_page.dart';
import 'package:app/pages/sitios/feed_page.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyBottomNavigationBar extends StatefulWidget {
  @override
  _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
}

class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_)=>_NavegacionModel(),
      child: Scaffold(
            body: DoubleBackToCloseApp(
              snackBar: SnackBar(content: Text('Presiona dos veces para salir')),
              child: _Paginas()),
            bottomNavigationBar: _Navegacion(),
          ), 
    );
  }
}

class _Navegacion extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final _navegacionModel = Provider.of<_NavegacionModel>(context);

    return CurvedNavigationBar(
      index: _navegacionModel.paginaActual,
      onTap: (i)=>_navegacionModel.paginaActual = i,
      items: <Widget>[
        Icon(
          Icons.mms,
        ),
        Icon(
          Icons.hotel,
        ),
        Icon(
          Icons.restaurant,
        ),

      ],
      color: Colors.green,
      buttonBackgroundColor: Colors.white,
      backgroundColor: Colors.white,
      height: 60,
      animationDuration: Duration(milliseconds: 300),
      animationCurve: Curves.fastOutSlowIn,
    );
  }
}

class _Paginas extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final _navegacionModel = Provider.of<_NavegacionModel>(context);
    return PageView(

      controller: _navegacionModel.pageController,
      // physics: BouncingScrollPhysics(),
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        FeedPage(),
        HotelPage(),
        RestaurantPage()
      ],
    );
  }
}

class _NavegacionModel with ChangeNotifier{
  int _paginaActual = 0;
  PageController _pageController = new PageController();

  int get paginaActual => this._paginaActual;

  set paginaActual(int valor){
    _paginaActual = valor;
    _pageController.animateToPage(valor, duration: Duration(milliseconds: 250), curve: Curves.easeOut);
    notifyListeners();
  }

  PageController get pageController => this._pageController;
}