
import 'package:flutter/material.dart';

class PlacehorderCommentPage extends StatelessWidget {
  final String ref;
  final String idUsuario;
  const PlacehorderCommentPage({Key key, this.ref, this.idUsuario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      // height: 50,
      child: Column(
        children: <Widget>[
          Container(
            width: size.width*0.5,
            height: 40,
            child: Text('Comenta este sitio',style: TextStyle(fontSize: 19),),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: 1
                )
              )
            )
          )
        ],
      ),
    );
  }
}
