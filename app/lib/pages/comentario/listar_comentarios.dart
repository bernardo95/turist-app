import 'package:app/models/calificacion_model.dart';
import 'package:app/models/usuario_model.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:app/providers/profile_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'eliminar_comentario.dart';

class ListarComentariosPage extends StatelessWidget {
  final String ref;
  final String tipo;
  const ListarComentariosPage({Key key, this.ref, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return ChangeNotifierProvider(
      create: (_)=>_ModelPage(),
          child: Container(
          child: FutureBuilder(
        future: Provider.of<ComentariosProvider>(context).getComentarios(ref),
        builder:
            (BuildContext context, AsyncSnapshot<List<Calificacion>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.length==0) {
              return Container(
              child: Text('Aun no hay comentarios para este sitio'),
            );
            } else {
              var data = snapshot.data.reversed.toList();
            return ListaComentarios(data: data,tipo:tipo);
            }
            
          }else{
            return Container(
              child: Text('Aun no hay comentarios para este sitio'),
            );
          }
        },
      )),
    );
  }
}

class ListaComentarios extends StatelessWidget {
  final List<Calificacion> data;
  final String tipo;
  const ListaComentarios({Key key, this.data, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        physics: BouncingScrollPhysics(),
        itemCount: data.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 15,horizontal: 8),
            child: VistaComentario(
              comentario: data[index],tipo:tipo
            ),
          );
        },
      ),
    );
  }
}

class VistaComentario extends StatelessWidget {
  final Calificacion comentario;
  final String tipo;
  const VistaComentario({Key key, this.comentario, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
   
    return FutureBuilder(
      future: ProfileProvider().getUsuario(comentario.idUsuario),
      builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) { 
        if (snapshot.hasData) {
          return Column(
            children: <Widget>[
              Encabezado(comentario:comentario,usuario:snapshot.data,tipo: tipo,),
              Cuerpo(comentario:comentario.comentario),
              Fecha(fecha:comentario.created),
            ],
          );
        } else {
          return Center(
            widthFactor: 50,
            child: CircularProgressIndicator());
        }
     },
     
    );
  }
}

class Encabezado extends StatelessWidget {
  final Calificacion comentario;
  final Usuario usuario;
  final String tipo;
  const Encabezado({Key key, this.comentario, this.usuario, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var instanciaId;
    int contador = Provider.of<_ModelPage>(context).contador;
    init() async {
      if (contador==0) {
      instanciaId = await Provider.of<UsuarioInstancia>(context).getId();
      Provider.of<_ModelPage>(context,listen:false).id = instanciaId;
      contador++;
      }
    }
    init();
    return Row(
      children: <Widget>[
        CircleAvatar(
          child: FadeInImage(placeholder: AssetImage('assets/img/loading.gif'), image: CachedNetworkImageProvider(usuario.imgProfile)),
          radius: 20,
          // backgroundImage: (usuario.imgProfile == null)
          //     ? AssetImage('assets/img/no-profile.jpg')
          //     : CachedNetworkImageProvider(usuario.imgProfile),
        ),
        SizedBox(width: 15,),
        Text(usuario.nombre +' ' + usuario.apellido),
        Spacer(),
        Estrellas(rate: comentario.rate),
        if (usuario.id == Provider.of<_ModelPage>(context).id)
        EliminarComentarioPage(idComentario: comentario.id,idRef: comentario.idRef,tipo: tipo,)
      ],
    );
  }
}

class Estrellas extends StatelessWidget {
  final int rate;
  const Estrellas({Key key, this.rate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Expanded(
          child: Container(
        width: size.width,
        height: 20,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
            itemCount: rate,
            itemBuilder: (BuildContext context, int index) { 
              return Container(
                child: Icon(Icons.star,size: 15,));
           },),
      ),
    );
  }
}

class Cuerpo extends StatelessWidget {
  final String comentario;
  const Cuerpo({Key key, this.comentario}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(width: 15,),
        Expanded(child: Text(comentario,style: TextStyle(fontSize: 19),))
      ],
      
    );
  }
}

class Fecha extends StatelessWidget {
  final String fecha;
  const Fecha({Key key, this.fecha}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(),
        Text(fecha.substring(0,10))
      ],
    );
  }
}

class _ModelPage with ChangeNotifier {
  int _contador = 0;
  String _id;
  int get contador => this._contador;
  String get id => this._id;

  set contador(int value){
    this._contador = value;
    notifyListeners();
  }

  set id(String value){
    this._id = value;
    notifyListeners();
  }
}