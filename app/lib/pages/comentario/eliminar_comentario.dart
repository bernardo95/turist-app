import 'package:app/models/calificacion_model.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EliminarComentarioPage extends StatelessWidget {
  final String idComentario;
  final String idRef;
  final String tipo;
  const EliminarComentarioPage(
      {Key key, this.idComentario, this.idRef, this.tipo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.close),
      onPressed: () async {
        bool eliminar = await mensajeAdvertencia(context);
        Calificacion resp = Calificacion();
        if (eliminar) {
          Provider.of<ComentariosProvider>(context, listen: false).comentando =
              true;
          resp = await ComentariosProvider()
              .deleteComentario(idComentario, idRef, tipo);
          Provider.of<ComentariosProvider>(context, listen: false).comentando =
              false;
          if (resp.id == null) {
            mensajeError(
              context,
            );
          }
        }
      },
    );
  }

  Future<void> mensajeError(BuildContext context) async {
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('Ocurrio un error'),
            content: Text('No se pudo eliminar la calificación'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'))
            ],
          );
        });
  }

  Future<bool> mensajeAdvertencia(BuildContext context) async {
    bool confirmar = false;
    await showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('¿Desea eliminar la calificación?'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('No')),
              FlatButton(
                  onPressed: () {
                    confirmar = true;
                    Navigator.of(context).pop();
                  },
                  child: Text('Si'))
            ],
          );
        });
    return confirmar;
  }
}
