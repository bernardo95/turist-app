import 'package:app/models/calificacion_model.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CrearComentario extends StatelessWidget {
  final String ref;
  final String idUsuario;
  final String tipo;
  const CrearComentario({Key key, this.ref, this.idUsuario, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();
    Calificacion calificacionModel = Calificacion();
    calificacionModel.idRef = ref;
    calificacionModel.idUsuario = idUsuario;

    final size = MediaQuery.of(context).size;
    return ChangeNotifierProvider(
      create: (_) => _ModelPage(),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Center(
          child: Container(
            width: double.infinity,
            height: size.height * 0.29,
            decoration: BoxDecoration(
                // color: Colors.green[50],
                borderRadius: BorderRadius.circular(20)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Estrella(value: 1,model: calificacionModel,),
                    Estrella(value: 2,model: calificacionModel),
                    Estrella(value: 3,model: calificacionModel),
                    Estrella(value: 4,model: calificacionModel),
                    Estrella(value: 5,model: calificacionModel),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                CajaTexto(formKey: _formKey,model: calificacionModel),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    BotonEnviar(
                      formkey: _formKey,
                      model: calificacionModel,
                      tipo: tipo
                    ),
                    SizedBox(
                      width: 25,
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Estrella extends StatelessWidget {
  final int value;
  final Calificacion model;
  const Estrella({Key key, this.value, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int provider = Provider.of<_ModelPage>(context).numero;
    return GestureDetector(
      onTap: () => {
        Provider.of<_ModelPage>(context, listen: false).numero = value,
        model.rate = value
      },
      child: (value <= provider) ? Icon(Icons.star) : Icon(Icons.star_border),
    );
  }
}

class CajaTexto extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final Calificacion model;
  const CajaTexto({Key key, this.formKey, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    TextEditingController controller = TextEditingController();
    return Container(
      width: size.width * 0.8,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(width: 1, color: Colors.green)),
      child: Form(
        key: formKey,
        child: TextFormField(
            controller: controller,
            textCapitalization: TextCapitalization.sentences,
            style: TextStyle(
              fontSize: 22.0,
            ),
            keyboardType: TextInputType.text,
            decoration: InputDecoration.collapsed(
              hintText: 'Haz tu comentario',
              focusColor: Colors.green,
              hoverColor: Colors.green,
              hintStyle: TextStyle(fontWeight: FontWeight.w500),
            ),
            maxLength: 200,
            textAlign: TextAlign.center,
            onSaved: (value) {
              controller.text = value;
              model.comentario = value;
            }),
      ),
    );
  }
}

class BotonEnviar extends StatelessWidget {
  final GlobalKey<FormState> formkey;
  final Calificacion model;
  final String tipo;


  const BotonEnviar({Key key, this.formkey, this.model, this.tipo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        onPressed: () => {submit(formkey,context)},
        child: Text('Comentar'),
        color: Colors.green,
        textColor: Colors.white);
  }

  submit(GlobalKey<FormState> formkey,BuildContext context) async {
    ComentariosProvider comentarioProvider = Provider.of<ComentariosProvider>(context,listen: false);
     if (!formkey.currentState.validate()) return;
    if (formkey.currentState.validate()) {
      formkey.currentState.save();
      if (model.rate==null) {
        mensaje(context);
      }else{
       await comentarioProvider.postComentario(model,tipo);
       Provider.of<ComentariosProvider>(context,listen:false).comentando = false;

      }
    }
  }

  Future<void> mensaje(BuildContext context) async{
    await showDialog(
     barrierDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Text('No se pudo enviar la calificación'),
            content: Text('Debe seleccionar una calificación'),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'))
            ],
          );
        }
    );
  }
}

class _ModelPage with ChangeNotifier {
  int _numero = 0;

  int get numero => this._numero;

  set numero(int value) {
    this._numero = value;
    notifyListeners();
  }
}
