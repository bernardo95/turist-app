import 'package:app/models/plan_model.dart';
import 'package:app/pages/filtros/filtro_sitios.dart';
import 'package:app/providers/filtro_provider.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/search/search_delegate_planes.dart';
import 'package:app/widgets/menu_drawer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app/utils/calificacion.dart' as utils;

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child:
          Scaffold(
          drawer: MenuWidget(), 
          body: Container(child: _MainScroll()
          )
            ),
    );
  }

}

class _Loading extends StatelessWidget {
  const _Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.6,
        child: Center(child: CircularProgressIndicator()));
  }
}

class _MainScroll extends StatefulWidget {
  const _MainScroll({Key key}) : super(key: key);

  @override
  __MainScrollState createState() => __MainScrollState();
}

class __MainScrollState extends State<_MainScroll> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        SliverAppBar(
          floating: true,
          backgroundColor: Colors.green,
          title: Text('Turist App'),
          centerTitle: true,
          elevation: 0,
          actions: <Widget>[
            FutureBuilder(
              future: PlanesProvider().getPlanes(),
              builder: (BuildContext context, AsyncSnapshot<List<Plan>> snapshot) { 
                if (snapshot.hasData) {
                  return IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  showSearch(context: context, delegate: DataSearchPlanes(planes :snapshot.data));
                });
                } else {
                  return Container();
                }
             },)
          ],
        ),
        SliverList(
          delegate: SliverChildListDelegate([
            FiltroSitiosPage(),
            if (Provider.of<PlanesProvider>(context).recargar)
              FutureBuilder(
                future: PlanesProvider().getPlanes(filtro: Provider.of<FiltroProvider>(context).filtroPlan),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Plan>> snapshot) {
                  if (snapshot.hasData) {
                    return _ListaSitios(
                      planes: snapshot.data,
                    );
                  } else {
                    return _Loading();
                  }
                },
              )
          ]),
        )
      ],
    );
  }
}

class _ListaSitios extends StatelessWidget {
  final List<Plan> planes;

  const _ListaSitios({Key key, this.planes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: planes.length,
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return _Card(context: context, index: index, listaPlanes: planes);
      },
    );
  }
}

class _Card extends StatelessWidget {
  final BuildContext context;
  final int index;
  final List<Plan> listaPlanes;

  const _Card({Key key, this.context, this.index, this.listaPlanes})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Colors.green[100],
                  blurRadius: 1,
                  spreadRadius: 1,
                  offset: Offset(0, 0))
            ],
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          children: <Widget>[
            GestureDetector(
              child: Container(
                  width: size.width * 0.9,
                  height: size.height * 0.3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: FadeInImage(
                      placeholder: AssetImage('assets/img/loading.gif'),
                      image: CachedNetworkImageProvider("${listaPlanes[index].imagenes[0]}"),
                      fit: BoxFit.cover,
                    ),
                  )),
              onTap: () {
                //  Provider.of<PlanesProvider>(context,listen: false).getPlan(listaPlanes[index].id);
                Navigator.pushNamed(context, 'planTuristico',
                    arguments: listaPlanes[index]);
              },
            ),
            SizedBox(height: 12),
            Row(
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.05,
                ),
                Expanded(
                  flex: 5,
                  child: Text(
                    listaPlanes[index].nombre,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
                  ),
                ),
                Spacer(),
                if (listaPlanes[index].calificacion != null && listaPlanes[index].calificacion!=0)
                  Container(
                    width: size.width * 0.2,
                    height: size.height * 0.05,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        SizedBox(),
                        Icon(
                          Icons.star,
                          color: Colors.green,
                        ),
                        if (listaPlanes[index].calificacion != null && listaPlanes[index].calificacion!=0)
                          Text(
                            '${utils.rateTransform(listaPlanes[index].calificacion)}/5',
                            style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w500),
                          ),
                        SizedBox(),
                      ],
                    ),
                  ),
                SizedBox(
                  width: size.width * 0.05,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
