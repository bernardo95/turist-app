import 'dart:io';
import 'package:app/models/usuario_model.dart';
import 'package:app/providers/usuario_provider.dart';
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';


class RegistroPage6 extends StatelessWidget {
  final Usuario data;
  const RegistroPage6({this.data});

  @override
  Widget build(BuildContext context) {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldkey = GlobalKey<ScaffoldState>();
    return WillPopScope(
      onWillPop: (){
        Navigator.pushNamed(context, 'registroPage5',arguments: data);
      },
          child: Scaffold(
            key: _scaffoldkey,
          resizeToAvoidBottomInset: false,
          body: ChangeNotifierProvider(
            create: (_)=>_ModelPage(),
              child: Stack(
              children: <Widget>[
                _fondo(context),
                _botonAtras(context),
                _RegistroFormulario(formKey: formKey,data: data,scaffoldkey: _scaffoldkey,),
                
              ],
            ),
          )),
    );
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      child: Padding(
        padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
        child: IconButton(
           
            alignment: Alignment.topLeft,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30.0,
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'registroPage5',
                  arguments: data);
            }),
      ),
    );
  }
  
}
class _RegistroFormulario extends StatelessWidget {
  final GlobalKey<FormState> formKey;
  final GlobalKey<ScaffoldState> scaffoldkey;
  final Usuario data;
  const _RegistroFormulario({Key key, this.formKey, this.data, this.scaffoldkey}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    File _image = Provider.of<_ModelPage>(context).image;
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                'Selecciona una imagen',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: GestureDetector(
                  child: _widgetDynamic(context,_image,scaffoldkey),
                  onTap: () {
                    _selecionarFoto(context,_image,scaffoldkey);
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit(context,formKey,_image);
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Finalizar',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  void _submit(BuildContext context,GlobalKey<FormState> formKey,File _image) {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      _enviarFormulario(context,_image);
    }
  }
  Future<void> _enviarFormulario(BuildContext context,File _image) async {

    UsuarioProvider usuarioProvider = UsuarioProvider();
    final size = MediaQuery.of(context).size;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            content: FutureBuilder(
              future: usuarioProvider.crearCuenta(data, _image),
              builder: (BuildContext context, AsyncSnapshot<Usuario> snapshot) {
                if (snapshot.hasData) {

                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'Cuenta creada, ya puedes iniciar sesion',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        RaisedButton(
                          onPressed: ()  {
                            Navigator.pushNamedAndRemoveUntil(context, 'app',ModalRoute.withName('/app'));
                          },
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          textColor: Colors.white,
                          child: Text(
                            'Terminar',
                            style: TextStyle(fontSize: 19),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0)),
                        ),
                      ],
                    ),
                  );
                }
                if (snapshot.hasError) {
                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          'No se pudo crear la cuenta',
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 15.0,
                        ),
                        RaisedButton(
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(context, 'app',ModalRoute.withName('/app'));
                          },
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          textColor: Colors.white,
                          child: Text(
                            'Intentar después',
                            style: TextStyle(fontSize: 19),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0)),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Container(
                      height: size.height * 0.6,
                      child: Center(child: CircularProgressIndicator()));
                }
              },
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
          );
        });
  }
}
Widget _widgetDynamic(BuildContext context,File _image,GlobalKey<ScaffoldState> _scaffoldkey) {
    if (_image == null) {
      return IconButton(
        iconSize: 100.0,
        color: Colors.grey[800],
        icon: Icon(Icons.image),
        onPressed: () {
          _selecionarFoto(context,_image,_scaffoldkey);
          // setState(() {});
        },
      );
    } else {
      return _mostrarFoto(context,_image,_scaffoldkey);
    }
  }
  Widget _mostrarFoto(BuildContext context,File _image,GlobalKey<ScaffoldState> _scaffoldkey) {
    return GestureDetector(
      onTap: () {
        _mostrarAlerta(context,_image,_scaffoldkey);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image(
          image:(_image?.path!=null)? AssetImage(_image.path) : AssetImage('assets/img/no-profile.jpg'),
          height: 150,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
  void _mostrarAlerta(BuildContext context,File _image,GlobalKey<ScaffoldState> _scaffoldkey) {
    // final size = MediaQuery.of(context).size;
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext contex) {
          return AlertDialog(
              title: Text('¿Deseas cambiar la imagen?',
                  style: TextStyle(fontSize: 20.0)),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                    Provider.of<_ModelPage>(context,listen: false).image = null;
                      
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Eliminar imagen',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
                // SizedBox(width: size.height*0.05,),
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'No',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
                FlatButton(
                    onPressed: () {
                      Provider.of<_ModelPage>(context,listen: false).image = null;
                       _selecionarFoto(contex,_image,_scaffoldkey);
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Si',
                      style: TextStyle(
                          color: Color.fromRGBO(0, 140, 80, 0.75),
                          fontSize: 16.0),
                    )),
              ],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)));
        });
  }
_selecionarFoto(BuildContext context,File _image,GlobalKey<ScaffoldState> _scaffoldkey) async {
    File image;
    image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (_image != null) {
      //limpieza
      _scaffoldkey.currentState.showSnackBar(new SnackBar(content: new Text("Unable to select photo, Try again..")));
      
    }
    Provider.of<_ModelPage>(context,listen: false).image = image;
    
  }


class _ModelPage with ChangeNotifier {
  File _image;
  File get image => this._image;
  set image(File value){
    this._image = value;
    notifyListeners();
  }
}