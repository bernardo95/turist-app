import 'package:app/models/usuario_model.dart';
import 'package:app/utils/validators.dart' as utils;
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';

class RegistroPage extends StatefulWidget {
  final Usuario data;

  RegistroPage({
    Key key,
    @required this.data,
  }) : super(key: key);
  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends State<RegistroPage> {
  final formKey = GlobalKey<FormState>();
  Usuario usuarioModel = Usuario();
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();

@override
  void initState() {
    super.initState();
    if (widget.data?.nombre!=null) {
      _controllerName.text = widget.data.nombre;
      
    }
    if (widget.data?.apellido!=null) {
      _controllerLastName.text = widget.data.apellido;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pushNamed(context, 'loginPage');
      },
          child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: <Widget>[
              _fondo(context),
              _botonAtras(context),
              _registroFormulario(context)
            ],
          )),
    );
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
        child: IconButton(
            
            alignment: Alignment.topLeft,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30.0,
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'loginPage');
            }),
      ),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                '¿Cuál es su nombre?',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: TextFormField(
                  controller: _controllerName,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Nombre',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    usuarioModel.nombre = _controllerName.text;
                  },
                  validator: (value) {
                    if (value.length < 3) {
                      return 'Nombre no valido';
                    }
                    if (utils.isString(value)) {
                      return null;
                    } else {
                      return 'Nombre no valido';
                    }
                  },
                ),
              ),
              SizedBox(
                width: size.width * 0.4,
                child: TextFormField(
                  controller: _controllerLastName,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Apellido',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    usuarioModel.apellido = _controllerLastName.text;
                  },
                  validator: (value) {
                    if (value.length < 3) {
                      return 'Apellido no valido';
                    }
                    if (utils.isString(value)) {
                      return null;
                    } else {
                      return 'Apellido no valido';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      usuarioModel.facebook = false;
      usuarioModel.google = false;
      usuarioModel.role = 'USER_ROLE';
      usuarioModel.estado = true;
      Navigator.pushNamed(context, 'registroPage2', arguments: usuarioModel);
    }
  }
}
