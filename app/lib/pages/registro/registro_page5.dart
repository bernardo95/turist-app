import 'package:app/models/usuario_model.dart';
import 'package:app/utils/validators.dart';
import 'package:app/widgets/customShapeClipper.dart';
import 'package:flutter/material.dart';

class RegistroPage5 extends StatefulWidget {
  final Usuario data;

  RegistroPage5({
    Key key,
    @required this.data,
  }) : super(key: key);
  @override
  _RegistroPage5State createState() => _RegistroPage5State();
}

class _RegistroPage5State extends State<RegistroPage5> {
  final formKey = GlobalKey<FormState>();
  TextEditingController _controllerPassword1 = TextEditingController();
  TextEditingController _controllerPassword2 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.pushNamed(context, 'registroPage4',arguments: widget.data);
      },
          child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: <Widget>[
              _fondo(context),
              _botonAtras(context),
              _registroFormulario(context)
            ],
          )),
    );
  }

  Widget _fondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            height: size.height * 0.4,
            color: Color.fromRGBO(0, 140, 80, 0.75),
          ),
        )
      ],
    );
  }

  Widget _botonAtras(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      // alignment: Alignment.topCenter,
      child: Padding(
        padding: EdgeInsets.only(left: 10.0, top: size.height * 0.07),
        child: IconButton(
            
            alignment: Alignment.topLeft,
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30.0,
            ),
            onPressed: () {
              Navigator.pushNamed(context, 'registroPage4',
                  arguments: widget.data);
            }),
      ),
    );
  }

  _registroFormulario(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SafeArea(
                  child: Container(
                height: size.height * 0.17,
              )),
              Text(
                'Ingrese una contraseña',
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              SizedBox(
                height: size.height * 0.1,
              ),
              SizedBox(
                width: size.width * 0.5,
                child: TextFormField(
                  controller: _controllerPassword1,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    hintText: 'Contraseña',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  validator: (value) {
                    if (value.length < 5) {
                      return 'Contraseña muy corta';
                    } else {
                      return null;
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.5,
                child: TextFormField(
                  controller: _controllerPassword2,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: 22.0,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: UnderlineInputBorder(
                      borderSide:
                          BorderSide(color: Color.fromRGBO(0, 140, 80, 0.75)),
                    ),
                    labelStyle: TextStyle(color: Colors.teal),
                    hintText: 'Contraseña',
                    focusColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hoverColor: Color.fromRGBO(0, 140, 80, 0.75),
                    hintStyle: TextStyle(fontWeight: FontWeight.w500),
                  ),
                  onSaved: (value) {
                    widget.data.password = value;
                  },
                  validator: (value) {
                    if (value.length < 5) {
                      return 'Contaseña muy corta';
                    } else if (passwordMatch(
                        _controllerPassword1.text, _controllerPassword2.text)) {
                      return null;
                    } else {
                      return 'La contraseña no coincide';
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                width: size.width * 0.4,
                child: RaisedButton(
                  onPressed: () {
                    _submit();
                  },
                  color: Color.fromRGBO(0, 140, 80, 0.75),
                  textColor: Colors.white,
                  child: Text(
                    'Siguiente',
                    style: TextStyle(fontSize: 19),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _submit() {
    if (!formKey.currentState.validate()) return;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();

      print(widget.data.toMap());
      Navigator.pushNamed(context, 'registroPage6', arguments: widget.data);
    }
  }
}
