import 'package:app/main.dart';
import 'package:app/pages/favoritos/favoritos_page.dart';
import 'package:app/pages/habitacion/habitacion_page.dart';
import 'package:app/pages/hotel/descripcion_hotel.dart';

import 'package:app/pages/login/login.dart';
import 'package:app/pages/map/map_page.dart';
import 'package:app/pages/menu/botton_navigation_menu.dart';
import 'package:app/pages/registro/registro.dart';
import 'package:app/pages/registro/registro_page2.dart';
import 'package:app/pages/registro/registro_page3.dart';
import 'package:app/pages/registro/registro_page4.dart';
import 'package:app/pages/registro/registro_page5.dart';
import 'package:app/pages/registro/registro_page6.dart';
import 'package:app/pages/perfil/perfil.dart';
import 'package:app/pages/reservas/historial_reservas_page.dart';
import 'package:app/pages/reservas/reserva_habitacion.dart';
import 'package:app/pages/reservas/reserva_plan.dart';
import 'package:app/pages/restaurante/descripcion_restaurante.dart';
import 'package:app/pages/sitios/feed_page.dart';
import 'package:app/pages/terminos_condiciones/terminos_condiciones_page.dart';
import 'package:app/test/test.dart';
import 'package:flutter/material.dart';

import 'pages/plan_turistico/plan_turistico.dart';


class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case 'feedPage':
        return MaterialPageRoute(builder: (_) => FeedPage());
      case 'planTuristico':
        return MaterialPageRoute(builder: (_)=> PlanTuristicoPage(plan:args));
      case 'reservaPlan':
        return MaterialPageRoute(builder: (_)=> ReservaPlanPage(plan:args));
      case 'loginPage':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case 'menuPage':
        return MaterialPageRoute(builder: (_)=>MenuBottonPage());
      case 'registroPage':
        return MaterialPageRoute(builder: (_) => RegistroPage(data: args));
      case 'registroPage2':
        return MaterialPageRoute(builder: (_) => RegistroPage2(data: args,));
      case 'registroPage3':
        return MaterialPageRoute(builder: (_) => RegistroPage3(data: args,));
      case 'registroPage4':
        return MaterialPageRoute(builder: (_) => RegistroPage4(data: args,));
      case 'registroPage5':
        return MaterialPageRoute(builder: (_) => RegistroPage5(data: args,));
      case 'registroPage6':
        return MaterialPageRoute(builder: (_) => RegistroPage6(data: args,));
      case 'app':
        return MaterialPageRoute(builder: (_)=> MyApp());
      case 'perfil':
        return MaterialPageRoute(builder: (_)=> PerfilPage());
      case 'mapa':
        return MaterialPageRoute(builder: (_)=>GoogleMapPage());
      case 'test':
        return MaterialPageRoute(builder: (_)=> TestPage());
      case 'descripcion_hotel':
        return MaterialPageRoute(builder: (_)=> DescripcionHotel(hotel:args));
      case 'descripcion_restaurante':
        return MaterialPageRoute(builder: (_)=> DescripcionRestaurante(restaurante: args,));
      case 'habitaciones':
        return MaterialPageRoute(builder: (_)=> HabitacionPage(args: args,));
      case 'reservaHabitacion':
        return MaterialPageRoute(builder: (_)=> ReservaRestaurantePage(args:args));
      case 'historialReservas':
        return MaterialPageRoute(builder: (_)=> HistorialReservasPage(idUsuario: args,));
      case 'favoritos':
        return MaterialPageRoute(builder: (_)=> FavoritosPage());
      case 'terminos_condiciones':
        return MaterialPageRoute(builder: (_)=> TerminosCondicionesPage());
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
