import 'package:app/pages/tab/tab_page.dart';
import 'package:app/providers/comentarios_provider.dart';
import 'package:app/providers/filtro_provider.dart';
import 'package:app/providers/habitacion_provider.dart';
import 'package:app/providers/hoteles_provider.dart';
import 'package:app/providers/login_state.dart';
import 'package:app/providers/planes_provider.dart';
import 'package:app/providers/profile_provider.dart';
import 'package:app/providers/restaurantes_provider.dart';
import 'package:app/providers/usuario_instancia_provider.dart';
import 'package:app/route_generator.dart';
import 'package:app/theme/tema.dart';
import 'package:flutter/material.dart'; 
import 'package:app/providers/analytics_provider.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(

      providers: [
        ChangeNotifierProvider(create: (_)=> PlanesProvider()),
        ChangeNotifierProvider(create: (_)=> HotelesProvider()),
        ChangeNotifierProvider(create: (_)=> HabitacionesProvider(),),
        ChangeNotifierProvider(create: (_)=> RestauranteProvider()),
        ChangeNotifierProvider(create: (_)=> ComentariosProvider()),
        ChangeNotifierProvider(create: (_) => UsuarioInstancia()),
        ChangeNotifierProvider(create: (_) => LoginState()),
        ChangeNotifierProvider(create: (_)=> ProfileProvider(),),
        ChangeNotifierProvider(create: (_)=> FiltroProvider(),),
        ChangeNotifierProvider(create: (_)=> AnalyticsProvider(),),
      ],
      child: MaterialApp(
        navigatorObservers: [
          AnalyticsProvider().getAnalyticsObserver(),
        ],
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('es'), //Spanish
        ],
        // initialRoute: 'test',
        debugShowCheckedModeBanner: false,
        home: MyBottomNavigationBar(),
        onGenerateRoute: RouteGenerator.generateRoute,
        theme: miTema,
        // initialRoute: 'registroPage',
      ),
    );
  }
}
