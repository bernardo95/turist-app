import 'package:app/models/hotel_model.dart';
import 'package:flutter/material.dart';

class DataSearchHoteles extends SearchDelegate {
  final List<Hotel> hoteles;
  DataSearchHoteles({this.hoteles});

  @override
  List<Widget> buildActions(BuildContext context) {
    // acciones de nuestro appbar
    return [
      IconButton(icon: Icon(Icons.clear), onPressed:(){
        query = '';
      })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // icono a la izquierda del appbar
    return IconButton(icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation), onPressed: (){
      close(context, null);
    });
  }

  @override
  Widget buildResults(BuildContext context) {
    // crea los resultados que se muestran
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // son las sugerencias que aparecen cuando la persona escribe
    final listaSugeridad = (query.isEmpty) ? hoteles.toList()
                                            : hoteles.where((hotel) => hotel.nombre.toLowerCase().contains(query.toLowerCase())).toList();
      return ListView.builder(
        itemCount: listaSugeridad.length,
        itemBuilder: (BuildContext context, int index) { 
          return ListTile(
            title: Text(listaSugeridad[index].nombre),
            onTap: (){
              Navigator.pushNamed(context, 'descripcion_hotel',arguments: listaSugeridad[index]);
            },
          );
       },);
    
  }
  
}