

import 'dart:convert';
class ReservasHabitaciones{
  List<ReservaHabitacion> items = new List();
  ReservasHabitaciones();

  ReservasHabitaciones.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final reserva = new ReservaHabitacion.fromJson(item);
      items.add(reserva);
    }
  }
}


ReservaHabitacion reservaHabitacionFromJson(String str) => ReservaHabitacion.fromJson(json.decode(str));

String reservaHabitacionToJson(ReservaHabitacion data) => json.encode(data.toJson());

class ReservaHabitacion {
    ReservaHabitacion({
        this.id,
        this.idUsuario,
        this.idHabitacion,
        this.idHotel,
        this.estado,
        this.precio,
        this.personas,
        this.habitaciones,
        this.fechaInicio,
        this.fechaFin,
    });

    String id;
    String idUsuario;
    String idHabitacion;
    String idHotel;
    String estado;
    int precio;
    int personas;
    int habitaciones;
    String fechaInicio;
    String fechaFin;

    factory ReservaHabitacion.fromJson(Map<String, dynamic> json) => ReservaHabitacion(
        id: json["_id"],
        idUsuario: json["id_usuario"],
        idHabitacion: json["id_habitacion"],
        idHotel: json["id_hotel"],
        estado: json["estado"],
        precio: json["precio"],
        personas: json["personas"],
        habitaciones: json["habitaciones"],
        fechaInicio: json["fechaInicio"],
        fechaFin: json["fechaFin"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "id_usuario": idUsuario,
        "id_habitacion": idHabitacion,
        "id_hotel": idHotel,
        "estado": estado,
        "precio": precio,
        "personas": personas,
        "habitaciones": habitaciones,
        "fechaInicio": fechaInicio,
        "fechaFin": fechaFin,
    };
}
