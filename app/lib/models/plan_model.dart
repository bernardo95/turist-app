import 'dart:convert';

class Planes {
  List<Plan> items = new List();
  Planes();

  Planes.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final sitio = new Plan.fromJson(item);
      items.add(sitio);
    }
  }
}
Plan planFromJson(String str) => Plan.fromJson(json.decode(str));

String planToJson(Plan data) => json.encode(data.toJson());

class Plan {
    Plan({
        this.id,
        this.nombre,
        this.imagenes,
        this.descripcion,
        this.ubicacion,
        this.duracion,
        this.precio,
        this.personas,
        this.puntoEncuentro,
        this.coordenadas,
        this.observaciones,
        this.categoria,
        this.servicios,
        this.serviciosExtra,
        this.recomendaciones,
        this.itinerario,
        this.horaInicio,
        this.horaFin,
        this.sitio,
        this.calificacion,
        this.estado,
    });

    String id;
    String nombre;
    List<String> imagenes;
    String descripcion;
    String ubicacion;
    String duracion;
    int precio;
    int personas;
    String puntoEncuentro;
    Map coordenadas;
    List<String> observaciones;
    String categoria;
    List<String> servicios;
    List<String> serviciosExtra;
    List<String> recomendaciones;
    List<String> itinerario;
    String horaInicio;
    String horaFin;
    String sitio;
    num calificacion;
    bool estado;

    factory Plan.fromJson(Map<String, dynamic> json) => Plan(
        id: json["_id"],
        nombre: json["nombre"],
        imagenes: List<String>.from(json["imagenes"].map((x) => x)),
        descripcion: json["descripcion"],
        ubicacion: json["ubicacion"],
        duracion: json["duracion"],
        precio: json["precio"],
        personas: json["personas"],
        puntoEncuentro: json["punto_encuentro"],
        coordenadas: json["coordenadas"],
        observaciones: List<String>.from(json["observaciones"].map((x) => x)),
        categoria: json["categoria"],
        servicios: List<String>.from(json["servicios"].map((x) => x)),
        serviciosExtra: List<String>.from(json["servicios_extra"].map((x) => x)),
        recomendaciones: List<String>.from(json["recomendaciones"].map((x) => x)),
        itinerario: List<String>.from(json["itinerario"].map((x) => x)),
        horaInicio: json["hora_inicio"],
        horaFin: json["hora_fin"],
        sitio: json["sitio"],
        calificacion: json["calificacion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre": nombre,
        "imagenes": List<dynamic>.from(imagenes.map((x) => x)),
        "descripcion": descripcion,
        "ubicacion": ubicacion,
        "duracion": duracion,
        "precio": precio,
        "personas": personas,
        "punto_encuentro": puntoEncuentro,
        "coordenadas": coordenadas,
        "observaciones": List<dynamic>.from(observaciones.map((x) => x)),
        "categoria": categoria,
        "servicios": List<dynamic>.from(servicios.map((x) => x)),
        "servicios_extra": List<dynamic>.from(serviciosExtra.map((x) => x)),
        "recomendaciones": List<dynamic>.from(recomendaciones.map((x) => x)),
        "itinerario": List<dynamic>.from(itinerario.map((x) => x)),
        "hora_inicio": horaInicio,
        "hora_fin": horaFin,
        "sitio": sitio,
        "calificacion": calificacion,
        "estado": estado,
    };
}
