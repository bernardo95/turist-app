

import 'dart:convert';

class Restaurantes {
  List<Restaurante> items = new List();
  Restaurantes();

  Restaurantes.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final restaurante = new Restaurante.fromJson(item);
      items.add(restaurante);
    }
  }
}

Restaurante restauranteFromJson(String str) => Restaurante.fromJson(json.decode(str));

String restauranteToJson(Restaurante data) => json.encode(data.toJson());

class Restaurante {
    Restaurante({
        this.id,
        this.nombre,
        this.imagenes,
        this.informacion,
        this.coordenadas,
        this.horario,
        this.ubicacion,
        this.calificacion,
        this.estado,
    });

    String id;
    String nombre;
    List<String> imagenes;
    String informacion;
    Map coordenadas;
    String horario;
    String ubicacion;
    num calificacion;
    bool estado;

    factory Restaurante.fromJson(Map<String, dynamic> json) => Restaurante(
        id: json["_id"],
        nombre: json["nombre"],
        imagenes: List<String>.from(json["imagenes"].map((x) => x)),
        informacion: json["informacion"],
        coordenadas: json["coordenadas"],
        horario: json["horario"],
        ubicacion: json["ubicacion"],
        calificacion: json["calificacion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre": nombre,
        "imagenes": List<dynamic>.from(imagenes.map((x) => x)),
        "informacion": informacion,
        "coordenadas": coordenadas,
        "horario": horario,
        "ubicacion": ubicacion,
        "calificacion": calificacion,
        "estado": estado,
    };
}
