
import 'dart:convert';
class Habitaciones {
  List<Habitacion> items = new List();
  Habitaciones();

  Habitaciones.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final sitio = new Habitacion.fromJson(item);
      items.add(sitio);
    }
  }
}

Habitacion habitacionFromJson(String str) => Habitacion.fromJson(json.decode(str));

String habitacionToJson(Habitacion data) => json.encode(data.toJson());

class Habitacion {
    Habitacion({
        this.id,
        this.idHotel,
        this.tipo,
        this.imagenes,
        this.precio,
        this.personas,
        this.descripcion,
        this.estado,
    });

    String id;
    String idHotel;
    String tipo;
    List<String> imagenes;
    int precio;
    int personas;
    String descripcion;
    bool estado;

    factory Habitacion.fromJson(Map<String, dynamic> json) => Habitacion(
        id: json["_id"],
        idHotel: json["id_hotel"],
        tipo: json["tipo"],
        imagenes: List<String>.from(json["imagenes"].map((x) => x)),
        precio: json["precio"],
        personas: json["personas"],
        descripcion: json["descripcion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "id_hotel": idHotel,
        "tipo": tipo,
        "imagenes": List<dynamic>.from(imagenes.map((x) => x)),
        "precio": precio,
        "personas": personas,
        "descripcion": descripcion,
        "estado": estado,
    };
}
