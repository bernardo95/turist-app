
import 'dart:convert';

class ReservasPlanes{
  List<ReservaPlan> items = new List();
  ReservasPlanes();

  ReservasPlanes.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final reserva = new ReservaPlan.fromJson(item);
      items.add(reserva);
    }
  }
}

ReservaPlan reservaPlanFromJson(String str) => ReservaPlan.fromJson(json.decode(str));

String reservaPlanToJson(ReservaPlan data) => json.encode(data.toJson());

class ReservaPlan {
    ReservaPlan({
        this.id,
        this.idUsuario,
        this.idPlan,
        this.estado,
        this.precio,
        this.personas,
        this.fecha,
    });

    String id;
    String idUsuario;
    String idPlan;
    String estado;
    int precio;
    int personas;
    String fecha;

    factory ReservaPlan.fromJson(Map<String, dynamic> json) => ReservaPlan(
        id: json["_id"],
        idUsuario: json["id_usuario"],
        idPlan: json["id_plan"],
        estado: json["estado"],
        precio: json["precio"],
        personas: json["personas"],
        fecha: json["fecha"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "id_usuario": idUsuario,
        "id_plan": idPlan,
        "estado": estado,
        "precio": precio,
        "personas": personas,
        "fecha": fecha,
    };
}
