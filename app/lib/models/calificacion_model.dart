import 'dart:convert';


class Calificaciones {
  List<Calificacion> items = new List();
  Calificaciones();

  Calificaciones.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final calificacion = new Calificacion.fromJson(item);
      items.add(calificacion);
    }
  }
}


Calificacion calificacionFromJson(String str) => Calificacion.fromJson(json.decode(str));

String calificacionToJson(Calificacion data) => json.encode(data.toJson());

class Calificacion {
    Calificacion({
        this.id,
        this.idUsuario,
        this.idRef,
        this.rate,
        this.comentario,
        this.created,
        this.updated
    });
    String id;
    String idUsuario;
    String idRef;
    int rate;
    String comentario;
    String created;
    String updated;

    factory Calificacion.fromJson(Map<String, dynamic> json) => Calificacion(
        id: json["_id"],
        idUsuario: json["id_usuario"],
        idRef: json["id_ref"],
        rate: json["rate"],
        comentario: json["comentario"],
        created: json["createdAt"],
        updated: json["updatedAt"]
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "id_usuario": idUsuario,
        "id_ref": idRef,
        "rate": rate,
        "comentario": comentario,
        "createdAt": created,
        "updatedAt": updated
    };
}
