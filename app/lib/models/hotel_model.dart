import 'dart:convert';

class Hoteles {
  List<Hotel> items = new List();
  Hoteles();

  Hoteles.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final sitio = new Hotel.fromJson(item);
      items.add(sitio);
    }
  }
}

Hotel hotelFromJson(String str) => Hotel.fromJson(json.decode(str));

String hotelToJson(Hotel data) => json.encode(data.toJson());

class Hotel {
    Hotel({
        this.id,
        this.nombre,
        this.imagenes,
        this.ubicacion,
        this.coordenadas,
        this.checkIn,
        this.checkOut,
        this.calificacion,
        this.categoria,
        this.descripcion,
        this.estado,
    });

    String id;
    String nombre;
    List<String> imagenes;
    String ubicacion;
    Map coordenadas;
    Map checkIn;
    Map checkOut;
    num calificacion;
    String categoria;
    String descripcion;
    bool estado;

    factory Hotel.fromJson(Map<String, dynamic> json) => Hotel(
        id: json["_id"],
        nombre: json["nombre"],
        imagenes: List<String>.from(json["imagenes"].map((x) => x)),
        ubicacion: json["ubicacion"],
        coordenadas: json["coordenadas"],
        checkIn: json["checkIn"],
        checkOut: json["checkOut"],
        calificacion: json["calificacion"],
        categoria: json["categoria"],
        descripcion: json["descripcion"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre": nombre,
        "imagenes": List<dynamic>.from(imagenes.map((x) => x)),
        "ubicacion": ubicacion,
        "coordenadas": coordenadas,
        "checkIn": checkIn,
        "checkOut": checkOut,
        "calificacion": calificacion,
        "categoria": categoria,
        "descripcion": descripcion,
        "estado": estado,
    };
}
